package com.fkppiapp;

import com.BV.LinearGradient.LinearGradientPackage;
import com.facebook.react.ReactPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativenavigation.NavigationApplication;
import com.rssignaturecapture.RSSignatureCapturePackage;
import com.horcrux.svg.SvgPackage;
import com.imagepicker.ImagePickerPackage;
import com.wix.RNCameraKit.RNCameraKitPackage;
import com.RNFetchBlob.RNFetchBlobPackage; 
//import com.magus.fblogin.FacebookLoginPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;
//import com.rumax.reactnative.pdfviewer.PDFViewPackage;
//import com.reactlibrary.PDFViewPackage;
//import org.wonday.pdf.RCTPdfView;
// import com.keyee.pdfview.PDFView; 
//import com.rnfs.RNFSPackage;
import java.util.Arrays;
import java.util.List;



// public class MainApplication extends Application implements ReactApplication {

//   private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
//     @Override
//     public boolean getUseDeveloperSupport() {
//       return BuildConfig.DEBUG;
//     }

//     @Override
//     protected List<ReactPackage> getPackages() {
//       return Arrays.<ReactPackage>asList(
//           new MainReactPackage(),
            // new PDFViewPackage(),
            // new RNFSPackage(),
            // new PDFView(),
           // new RCTPdfView(),
            // new RNCameraPackage(),
            //new RNCameraPackage(),
            //new RSSignatureCapturePackage(),
            //new RNCloudinaryPackage(),
//             new VectorIconsPackage(),
//             new SvgPackage(),
//             new NavigationReactPackage(),
//             new LinearGradientPackage(),
//             new ImagePickerPackage(),
//             new RNCloudinaryPackage()
//       );
//     }

//     @Override
//     protected String getJSMainModuleName() {
//       return "index";
//     }
//   };

//   @Override
//   public ReactNativeHost getReactNativeHost() {
//     return mReactNativeHost;
//   }

//   @Override
//   public void onCreate() {
//     super.onCreate();
//     SoLoader.init(this, /* native exopackage */ false);
//   }
// }

///////////////

public class MainApplication extends NavigationApplication {
  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    // Add the packages you require here.
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList(

      new VectorIconsPackage(),
      new SvgPackage(),
      new LinearGradientPackage(),
      new ImagePickerPackage(),
      new RNCameraKitPackage(),
      new RNFetchBlobPackage(),
      new RSSignatureCapturePackage(),
       //new FacebookLoginPackage(),
        new RNVersionNumberPackage()
        //new RCTPdfView()
         //new PDFViewPackage()
        //  new RCTPdfView(),
        //  new PDFView()
        //  new RNFSPackage()

    );
  }
  
  // @Override
  //   protected String getJSMainModuleName() {
  //     return "index";
  //   }
}
