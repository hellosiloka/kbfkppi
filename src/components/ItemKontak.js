import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import AutoHeightImage from 'react-native-auto-height-image';
import CommonStyles from '../styles/CommonStyles';

export default class ItemKontak extends Component {

  constructor(props) {
    super(props);
  }

  renderAvatar(){

   if(this.props.itemImg == ''){

      return(
          
            <Image
                  source={require('../../img/fkppi/avatars.png')}
                  style={{width: 100, height: 100,borderRadius: 100,borderColor:'#ccc',borderWidth:1}}
                />
        )
   }else{

     return(
          
             <Image
                source={{uri: this.props.itemImg}}
                style={{width: 90, height: 90,borderRadius: 90,borderColor:'#ccc',borderWidth:1}}
              />

               

        )

   }

  }


  render() {
    return (
      <View style={CommonStyles.itemWhiteBox}>
        <TouchableHighlight
          underlayColor='transparent'
          style={styles.highlightBox}
          onPress={this.props.onPressItem}
        >
          <View style={styles.detailItemBox}>
            <View style={styles.leftCol}>
              <View style={[styles.icon, {backgroundColor: this.props.itemColor}]}>
                {this.renderAvatar()}
              </View>
              <View style={{
                width: 0.7,
                height: 150,
                backgroundColor: 'rgb(229,229,229)'}}
              />
            </View>
            <View style={styles.centerInfo}>
            <Text style={{fontFamily: 'Roboto-Bold', color:'#2d2a26', fontSize:14}}>{this.props.itemName} {this.props.itemNameLast}</Text>
            <Text style={{fontFamily: 'Roboto-Medium', color:'#2d2a26', fontSize:13, lineHeight: 20}}>{this.props.itemJabatan}</Text>
            
            <Text style={{fontFamily: 'Roboto-Regular', color:'#2d2a26', fontSize:13, lineHeight: 20}}>PD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;{this.props.itemDpd}</Text>
            <Text style={{fontFamily: 'Roboto-Regular', color:'#2d2a26', fontSize:13, lineHeight: 20}}>PC &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;{this.props.itemDpw}</Text>
            <Text style={{fontFamily: 'Roboto-Regular', color:'#2d2a26', fontSize:13, lineHeight: 20}}>Rayon &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;{this.props.itemRayon}</Text>
            </View>
           
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

 const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 0,
    paddingHorizontal: 0
  },
  highlightBox: {
    borderRadius: 2,
  },
  centerInfo: {
    flex: 1, 
    margin: 0,
    alignItems: 'flex-start',
  },
  leftCol: {
    flexDirection: 'row',
    width: 105,
    marginRight: 15
  },
  icon: {
    width: 100,
    justifyContent: 'center',
     alignItems: 'center',
    margin:2,
  },
  iconText:{
    alignSelf: 'center',
  },
  rightCol: {
    width: 20,
    alignItems: 'center',
    marginRight: 10
  },
});

//const PropTypes = React.PropTypes;

ItemKontak.propTypes = {
  itemImg: PropTypes.number,
  itemName:PropTypes.string,
  itemNameLast:PropTypes.string,
  itemJabatan:PropTypes.string,
  itemTelepon:PropTypes.string,
  itemHp:PropTypes.string,
  itemStatus:PropTypes.string,
  itemOrganisasi:PropTypes.string,
  itemDpd:PropTypes.string,
  itemDpw:PropTypes.string,
  itemStatus:PropTypes.string,
  itemRayon:PropTypes.string,
  itemColor:PropTypes.string,
  itemHeaderText: PropTypes.string,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number,
  onPressItem: PropTypes.func,
};
