import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Iconx from 'react-native-vector-icons/FontAwesome';
import CommonStyles from '../styles/CommonStyles';

import moment from 'moment';
import 'moment/locale/id';
moment.locale('id');

export default class ItemNews extends Component {

  constructor(props) {
    super(props);
  }

  render() {
      const todayDate = moment(this.props.itemCreatedAt).format('LL');
      const timeAgo = moment(this.props.itemCreatedAt).startOf('hour').fromNow(); 
    return (
      <View style={CommonStyles.itemWhiteBox}>
        <TouchableHighlight
          underlayColor='transparent'
          style={styles.highlightBox}
          onPress={this.props.onPressItem}
        >
          <View style={styles.detailItemBox}>
            <View style={styles.leftCol}>
              <View style={[styles.icon, {backgroundColor: this.props.itemColor}]}>
                
                <Image
                    style={{width: 100, height: 100}}
                    source={{uri: this.props.itemImg}}
                  />
              </View>
              <View style={{
                width: 0.7,
                minHeight:125,
                backgroundColor: 'rgb(229,229,229)'}}
              />
            </View>
            <View style={styles.centerInfo}>
            <Text style={{color:'#ffbf20', fontSize: 11, lineHeight:13}}>{this.props.itemPublisher}, {todayDate} </Text>
            <Text style={{fontFamily: 'Roboto-Regular', color:'#000', fontSize:15, lineHeight:20, marginTop:5, marginBottom:5, marginRight:20}}>{this.props.itemTitle}</Text>
            <Text style={{color:'#333',fontSize: 11 , fontFamily: 'Roboto-Light'}}>{timeAgo}</Text>
          
             </View>

            <View style={{marginRight:10, marginTop:60, flexDirection:'row'}}><Iconx name="thumbs-up" size={15} color="#bbb" /><Text style={{marginLeft:5, fontSize:12}}>{this.props.itemLike}</Text></View>
           
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

 const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 0,
    paddingHorizontal: 0
  },
  highlightBox: {
    borderRadius: 2,
  },
  centerInfo: {
    flex: 1, 
    margin: 0,
    alignItems: 'flex-start',
  },
  leftCol: {
    flexDirection: 'row',
    width: 120,
    marginRight: 5
  },
  icon: {
    width: 110,
    justifyContent: 'center',
     alignItems: 'center',
    margin:2,
  },
  iconText:{
    alignSelf: 'center',
  },
  rightCol: {
    width: 20,
    alignItems: 'center',
    marginRight: 10
  },
});

//const PropTypes = React.PropTypes;

ItemNews.propTypes = {
  itemImg: PropTypes.string,
  itemTitle:PropTypes.string,
  itemTime:PropTypes.string,
  itemContent:PropTypes.string,
  itemLike:PropTypes.number,
  itemPublisher:PropTypes.string,
  itemCreatedAt:PropTypes.string,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number,
  onPressItem: PropTypes.func,
};
