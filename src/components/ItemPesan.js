import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  ToastAndroid,
  AsyncStorage,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import {DevApi, MainApi} from '../screens/fkppi/Api';
import Icon from 'react-native-vector-icons/Entypo';
import Iconx from 'react-native-vector-icons/Ionicons'
import CommonStyles from '../styles/CommonStyles';
import Swipeable from 'react-native-swipeable';
import moment from 'moment';
import 'moment/locale/id';
moment.locale('id');

export default class ItemPesan extends Component {

  constructor(props) {
    super(props);
  }



  async getDelete(id) {

   this.getKey(id);
   console.log('delete' + ' ' + id );

  }

 //////
 async getKey(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getRemove(value, id );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

////


getRemove(value, id){


       const FETCH_TIMEOUT = 2000;
       let didTimeOut = false;

        var url =  DevApi + 'message/delete/' + id;
        var that = this;
        new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'DELETE',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 

              }
        })

        .then((response) => {

           clearTimeout(timeout);

           if(!didTimeOut) {
                  
                // if (response.status == '403') {

                //     ToastAndroid.show('Login Gagal', ToastAndroid.SHORT)
                //     that.setState({
                //     loading: false,
                //   });
                // }
                if (response.status == '200') {
                    
                    ToastAndroid.show('Pesan sukses dihapus', ToastAndroid.SHORT)
                    //AsyncStorage.setItem('@Username:key', username);
                    //that.saveKey(response.headers.map.authorization[0]);
                  //   that.setState({
                  //   loading: false,
                  // });

                }else{


                   ToastAndroid.show('Pesan gagal dihapus', ToastAndroid.SHORT)

                }


           }


     //    });
     // .then((result) => {
     //     clearTimeout(timeout);
     //        if(!didTimeOut) {
     //             that.setState({ data : result,
     //                      loading: false

     //                    });

     //            console.log('fetch good! ', result);
     //            resolve(result);
     //        }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });


}

  render() {
const rightButtons = [
  <TouchableOpacity onPress={() => this.getDelete(this.props.itemId)} style={{marginTop:15, marginLeft:20}}>
  
<Iconx name="ios-trash" size={40} color="#404f3d"/>

  </TouchableOpacity>
];

    var str = this.props.itemHeaderText;
    const res = str.charAt(0);
    //console.log(res);

    const colorx = (Math.random()*0xFFFFFF<<0).toString(16);

    const todayDate = moment(this.props.itemCreatedAt).format('LL');
    const today = moment(this.props.itemCreatedAt).format('dddd'); 
    return (
      <View style={CommonStyles.itemWhiteBox}>
      <Swipeable  rightButtons={rightButtons}>
        <TouchableHighlight
          underlayColor='transparent'
          style={styles.highlightBox}
          onPress={this.props.onPressItem}
        >

         
          
       
          <View style={styles.detailItemBox}>
            <View style={styles.leftCol}>
              <View style={[styles.icon, {backgroundColor: '#333', borderWidth:2}]}>
                 <Text style={{color:'white', fontSize: 33, fontFamily: 'Roboto-Bold',}}>{res}</Text>
              </View>
              <View style={{
                width: 0.7,
                height: 75,
                backgroundColor: 'rgb(229,229,229)'}}
              />
            </View>
            <View style={styles.centerInfo}>
            <Text style={{fontFamily: 'Roboto-Bold', color:'black', fontSize:13}}>Dari: {this.props.itemFrom}</Text>
            <Text style={{fontFamily: 'Roboto-Regular', color:'#959595', fontSize:11, lineHeight: 20}}>{today}, {todayDate}</Text>
              <Text style={[
                CommonStyles.pesanText,
                CommonStyles.yellowColor,
                CommonStyles.mediumBold
              ]}>
                {this.props.itemHeaderText}
              </Text>
            </View>
            <View style={styles.rightCol}>
              <Icon
                style={{fontSize: 24, textAlign: 'center'}}
                name="chevron-thin-right"
                color="rgb(105,105,105)"
              />
            </View>
          </View>
        </TouchableHighlight>
         </Swipeable>
      </View>
    );
  }
}

 const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 0,
    paddingHorizontal: 0
  },
  highlightBox: {
    borderRadius: 2,
  },
  centerInfo: {
    flex: 1, 
    margin: 0,
    alignItems: 'flex-start',
  },
  leftCol: {
    flexDirection: 'row',
    width: 75,
    marginRight: 5
  },
  icon: {
    width: 70,
    justifyContent: 'center',
     alignItems: 'center',
    margin:2,
  },
  iconText:{
    alignSelf: 'center',
  },
  rightCol: {
    width: 20,
    alignItems: 'center',
    marginRight: 10
  },
});

//const PropTypes = React.PropTypes;

ItemPesan.propTypes = {
  itemImg: PropTypes.number,
  itemId: PropTypes.number,
  itemName:PropTypes.string,
  itemFrom:PropTypes.string,
  itemName:PropTypes.string,
  itemColor:PropTypes.string,
  itemCreatedAt:PropTypes.string,
  itemHeaderText: PropTypes.string,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number,
  onPressItem: PropTypes.func,
};
