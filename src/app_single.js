import {
  Platform
} from 'react-native';
import {Navigation} from 'react-native-navigation';

////Mobx
import Provider       from './utils/MobxRnnProvider';
import Stores         from './stores';

//////////


import {registerScreens} from './screens';

registerScreens(Stores, Provider);

// this will start our app
Navigation.startSingleScreenApp({
  screen: {
    screen: 'Fkppi.StartUpScreen',
  },
   animationType: 'slide-up',
   portraitOnlyMode: true,
   appStyle: {
    orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
  },
});


