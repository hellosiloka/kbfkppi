import React, { Component } from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  Platform,
  AsyncStorage,
  ToastAndroid,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-simple-modal";
import {DevApi, MainApi} from './fkppi/Api';
import Loader from './fkppi/Loader';

import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { appSingleNavigation } from '../styles/navigatorStyle';

export default class DaftarLoginScreen extends Component {
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.state = {
      data: {},
      myToken: '',
      //member_id:0,
      loaderx: true,
      animating: true,
      loading: false,
      password: "",
      hidePassword: true,
      
    }
  }

  componentDidMount() {
       StatusBar.setHidden(true);
        var that = this;
       that.getKey();
    }

    ////////////////

managePasswordVisibility = () =>
  {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  async getDaftar() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    setTimeout(() => {
     this._goToDaftarScreen(this.state.firstname, this.state.lastname, this.state.avatar, this.state.member_id)
      this.setState({
        loading: false,
      });
    }, 1500);
  }


 
  async getEdit() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    setTimeout(() => {
     this._goToDaftarEditScreen(this.state.firstname, this.state.lastname, this.state.avatar, this.state.member_id)
      this.setState({
        loading: false,
      });
    }, 1500);
  }

    async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

//////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loaderx: true
      });
      

      //console.log(this.state.myToken)
      
      //var ID = this.props.newsId

      var url =  DevApi + 'user/profile' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            firstname: result.firstname,
            lastname: result.lastname,
            avatar: result.avatar,
            member_id: result.member_id,
            loaderx: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }


  state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });
///////


async getLogin() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    this.getKeyx();
    //this._goToMainScreen();

    // setTimeout(() => {
    //  this._goToMainScreen();
    //  //AsyncStorage.setItem('@Token:key', 'contohloginkali');
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }




  async getKeyx() {
    try {
      const username = await AsyncStorage.getItem('@Username:key');
      //this.setState({myToken: value});
      this.onLoginPress(username);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
/////////


onLoginPress(username) {

   
     if(this.state.password ==''){
       ToastAndroid.show('Password tidak boleh kosong', ToastAndroid.SHORT);
       this.setState({
                    loading: false,
                  });
     }
     else {
        const FETCH_TIMEOUT = 5000;
       let didTimeOut = false;
        //ToastAndroid.show('OK', ToastAndroid.SHORT);
        //let username = this.state.username;
        let password = this.state.password;
        let userDetails = {
          //username:username,
          password:password
        }

        var url =  DevApi + 'user/login';
        var that = this;
      //////////////////

       new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',

              },
              body: JSON.stringify({

                'username' : username,
                'password' : password


              }),
        })

        .then((response) => {

           clearTimeout(timeout);

           if(!didTimeOut) {
                  
                if (response.status == '403') {

                    ToastAndroid.show('Login Gagal', ToastAndroid.SHORT)
                    that.setState({
                    loading: false,
                  });
                }
                if (response.status == '200') {
                    
                    //AsyncStorage.setItem('@Username:key', username);
                    that.saveKey(response.headers.map.authorization[0]);
                    that.setState({
                    loading: false,
                  });

                }


           }


     //    });
     // .then((result) => {
     //     clearTimeout(timeout);
     //        if(!didTimeOut) {
     //             that.setState({ data : result,
     //                      loading: false

     //                    });

     //            console.log('fetch good! ', result);
     //            resolve(result);
     //        }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });




     }
  }
////

async saveKey(value) {
    try {
      await AsyncStorage.setItem('@Token:key', value);
      this.getEdit();
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }

///////

renderButton(){
if (this.state.loaderx) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:50}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
  if(this.state.member_id == 0){


    return(

        <View>
        
        
                 <TouchableOpacity onPress={() => this.getDaftar()}>
                  <View style={[CommonStyles.buttonFieldLogin, styles.buttonBox, { marginTop: spaceHeight * 0.15, }]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Daftar
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>


      )


  }else{


    return(
    <View>
        

           <View style={{marginTop:30}}>
        
        
                 <TouchableOpacity onPress={() => this.getLogin()}>
                  <View style={[CommonStyles.buttonFieldLogin, styles.buttonBox, {marginBottom: spaceHeight * 0.1}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Masukan password
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
 </View>

      )


  }


}

  render() {
    return (
       
       <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
   <Loader
          loading={this.state.loading} />
      <View style={styles.container}>

       <View style={[{flex:2, marginTop: deviceHeight * 0.08}, styles.elementsContainer]}>

         <View style={{flex: 1}}>
             <View style={[styles.logoBox,{height: 130, width: 90}]}>
              <Image
                source={require('../../img/fkppi/logo2.png')}
                style={{width: 90, height: 130}}
              />
            </View>
         </View>
          <View style={{flex: 5}}>

               <View style={styles.formBox}>
                  <View style={CommonStyles.textInputField}>
                    <Image
                      source={require('../../img/fkppi/ic_password.png')}
                      style={{position:'absolute', bottom: 22,left: 20, width: 15, height: 22}}
                    />
                    <TextInput
                    secureTextEntry={this.state.hidePassword}
                    placeholder='Password'
                    style={CommonStyles.textInput}
                    underlineColorAndroid='transparent'
                    onChangeText={(password) => this.setState({password})}
                    keyboardType="default"
                    ref="loginPassword"
                    returnKeyType="done"
                   
                    value={this.state.password}
                  />
                  <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                    <Image source = { ( this.state.hidePassword ) ? require('../../img/fkppi/hide.png') : require('../../img/fkppi/view.png') } style = { styles.btnImage } />
                  </TouchableOpacity>
                  </View>
                 
                  {this.renderButton()}

             

              

            

               </View>
           
          </View>
          
       </View>

       <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Terkonfirmasi!
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Nomor Anggota Terdaftar Selamat Datang !
          </Text>

          <View style={{height: 40, marginTop: 20,borderColor:'#dce1e4',
    borderWidth: 0.5,
    borderStyle: 'solid',
    borderRadius: 0,
    backgroundColor:'#fbfbfb', justifyContent: 'center', alignItems: 'center' }}>
           
           <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#000', textAlign:  'center' }]}>Anna R. Legawati</Text>

          </View>

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>Lanjut</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>



      </View>
    </ImageBackground>
      
    );
  }

  _goToSignUpScreen() {
    this.props.navigator.push({
      screen: "Fkppi.SignUpScreen"
    });
  }



  _goToDaftarEditScreen(firstName, lastName, avatar, memberID) {
    this.props.navigator.push({
      screen: "Fkppi.DaftarEditScreen",
       passProps: {
       firstName: firstName,
       lastName: lastName,
       avatar: avatar,
       memberID: memberID
      },
    });
  }


  _goToDaftarScreen(firstName, lastName, avatar, memberID) {
    this.props.navigator.push({
      screen: "Fkppi.DaftarMemberScreen",
      passProps: {
       firstName: firstName,
       lastName: lastName,
       avatar: avatar,
       memberID: memberID
      },
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

    // Go to FindDoctorScreen
  _goToMainScreen() {
    this.props.navigator.push({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
    });
  }


  // _goToUpdatePassScreen() {
  //   this.props.navigator.push({
  //     screen: "Fkppi.UpdatePassScreen",
  //     animationType: 'slide-up'
  //   });
  // }

  // _handleClickFortgotPass() {
  //   this.props.navigator.push({
  //     screen: "Fkppi.ForgotPasswordScreen"
  //   });
  // }
}

const ELEMENT_HEIGHT = 377;
const spaceHeight = deviceHeight - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      },
    elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      flexDirection: 'row',
      marginTop: 20

    },
    textBox:{
      marginLeft: 24,
      marginRight: 24,

    },
    textItem:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Medium',fontSize: 16,lineHeight: 23
    },
    footerBox:{
      flex: 1,
      width: 40,
      height: 40,
      alignSelf:'flex-start'
    },
    textFooter:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Bold',fontSize: 16,lineHeight: 23
    },
    formBox: {
    marginTop: deviceHeight * 0.20,
    alignItems: 'center',
    marginBottom: spaceHeight * 0.05,
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
 titleBox: {
   justifyContent: 'flex-start'
  },
  visibilityBtn:
  {
    position: 'absolute',
    right: 5,
    height: 30,
    width: 35,
    padding: 5,
    top:10,
    opacity:0.5
  },

  btnImage:
  {
    resizeMode: 'contain',
    height: '100%',
    width: '100%'
  },
});
