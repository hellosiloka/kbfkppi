import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
    Alert,
  ImageBackground,
  ScrollView,
   Dimensions,
  TouchableHighlight,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { Navigation } from 'react-native-navigation';

import RNFetchBlob from 'rn-fetch-blob'
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';
import GridView from 'react-native-super-grid';
import AutoHeightImage from 'react-native-auto-height-image';

import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';
import HTML from 'react-native-render-html';
import ItemPesan from '../components/ItemPesan';

const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:true};

export default class MabesDetailScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: {},
      myToken: '',
      loading: true,
      animating: true,
      
    }
    
   
  }
  
  componentDidMount() {
       StatusBar.setHidden(true);
       //this.closeActivityIndicator();
       var that = this;
       that.getKey();
        
    }

 

 async requestStoragePermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        'title': 'Storage Permission',
        'message': 'FKPPI App needs access to your Storage '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the storage")
    } else {
      console.log("Storage permission denied")
    }
  } catch (err) {
    console.warn(err)
  }
}


  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.requestStoragePermission();
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  ///////////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.mId
      var url =  DevApi + 'mabes/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            body: result.body,
            attachment: result.attachment,
            attachment_name: result.attachment_name,
            loading: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }


 onPress = () => {
    this.getDownload();
  }
////////////

getDownload(){

    
   var date      = new Date();
    var url       = this.state.attachment;
    var ext       = this.extention(url);
    ext = "."+ext[0];
    const { config, fs } = RNFetchBlob
    let PictureDir = fs.dirs.PictureDir
    let options = {
      fileCache: true,
      addAndroidDownloads : {
        useDownloadManager : true,
        notification : true,
        path:  PictureDir + "/PDF_"+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
        description : 'Pdf'
      }
    }
    config(options).fetch('GET', url).then((res) => {
      Alert.alert("PDF Sukses didownload");
    });
  }
extention(filename){
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;


//////////end
  }
  ////////


  renderBody(){

     if (this.state.loading) {

        return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)

     }else{
   

       return(

          <View>

             <HTML style={{color:'#2d2a26', fontSize: 15, lineHeight:28, fontFamily: 'Roboto-Light'}} html={this.state.body} imagesMaxWidth={Dimensions.get('window').width} />
           

          </View>


       )


     }


  }
 /////


 renderPdf(){

  if(this.state.attachment == null){
   
   return
 
  }else{

   return(

 
    <AutoHeightImage
          width={100}
          source={require('../../img/fkppi/pdf.png')}
        />
           

   )

  }


 }
  //////////
 

  renderDownload(){

    if(this.state.attachment == null){
      
      return(
        
         <TouchableOpacity>
                  <View style={{justifyContent:'center', alignSelf:'center',  marginBottom: spaceHeight * 0.1}}>
                    <Text style={{color:'#e89e01', textAlign:'center' , fontSize:14, fontFamily:'Roboto-Medium'}}>
                      {this.props.mName}
                    </Text>
                  </View>
                </TouchableOpacity>

      )
    }else{
    
      return(

        <TouchableOpacity onPress={this.onPress}>
                  <View style={{justifyContent:'center', alignSelf:'center',  marginBottom: spaceHeight * 0.1}}>
                    <Text style={{color:'#e89e01', textAlign:'center' , fontSize:14, fontFamily:'Roboto-Medium'}}>
                      DOWNLOAD PDF
                    </Text>
                  </View>
                </TouchableOpacity>

      )

    }


  }

  ///////
   
  render() {

  // const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:false};
  // console.log(source)
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.01}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>KB FKPPI</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
               
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
         <ScrollView style={CommonStyles.noTabScroll}>
          <View style={{margin:20}}>
 
           <Text style={{color:'#333', fontFamily:'Roboto-Medium', fontSize:16}}> {this.props.mName}</Text>
           <View style={{width:'100%', height:2, backgroundColor:'#e89e01', marginTop:10, marginBottom:10}}></View>
             
           <HTML style={{color:'#2d2a26', fontSize: 15, lineHeight:28, fontFamily: 'Roboto-Light'}} html={this.state.body} imagesMaxWidth={Dimensions.get('window').width} />
            

           
             <View style={{width:100,}}>
             
             {this.renderPdf()}
             
             </View>

           <Text style={{marginTop:10}}> {this.state.attachment_name == 'undefined' ? '' : this.state.attachment_name} </Text>
          </View>
           
        </ScrollView>

        <View style={{width:'100%', height:60, backgroundColor:'#232020', justifyContent:'flex-end'}}>

             {this.renderDownload()}
            </View>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//

_goToBack() {
    this.props.navigator.pop({
      animated: true, // does the pop have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
  }


  
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  pdf: {
        width:'100%',flex:8,
        backgroundColor:'#fbfbfb'
    },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
