import React, { Component } from 'react';
import {Dimensions, Text, View, Image } from 'react-native';
import CommonStyles from '../../styles/CommonStyles';
const deviceHeight = Dimensions.get('window').height;


export default class Kosong extends Component {

    render(){

        return(

          <View style={{flex:1}}>
          <View style={{alignItems:'center', justifyContent:'center', marginTop: deviceHeight / 7.5,}}>
          <Image
            source={require('../../../img/fkppi/ic_ava_not_found.png')}
            style={{width: 70, height: 100}}
          />
          <Text style={[
            CommonStyles.normalText,
            CommonStyles.lightgreyColor,
            CommonStyles.mediumBold,
            {lineHeight: 40, marginBottom: 7}
          ]}>DATA BELUM ADA!</Text>
          </View>
          </View>
        );
        
    }

}
