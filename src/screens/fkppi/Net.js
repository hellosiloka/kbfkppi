import React, { Component } from 'react';
import { Dimensions, Text, View, StyleSheet, ScrollView, Image, RefreshControl } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const deviceHeight = Dimensions.get('window').height;

export default class Net extends Component {
  
    constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }



    render(){

        return(

    

          <View style={{flex:1}} >

          	<View style={{alignItems:'center', justifyContent:'center', marginTop: deviceHeight / 4,}}>
          	  <Icon name="signal-wifi-off"  size={100} color="#eee"/>
	          <Text style={{color:'#888'}}>
	            Internet Anda Terputus !!!
	          </Text>
          	</View>
          </View>
      
        );

    }

}


