import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   Alert,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  AsyncStorage,
  ToastAndroid,
  DatePickerAndroid,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import {DevApi, MainApi} from './fkppi/Api';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import Select from 'react-native-select-plus';
import GridView from 'react-native-super-grid';
import RNFetchBlob from 'rn-fetch-blob';
import {toJS } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import Loader from './fkppi/Loader';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Modal from "react-native-simple-modal";
import { Container, Header, Button, Body, Content, Card, CardItem, Form, TabHeading, Item, Picker, Label, Input, Textarea, Tab, Tabs, ScrollableTab } from 'native-base';

import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemNews from '../components/ItemNews';


const Loading = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih Provinsi Terlebih  Dahulu</Text>
    </View>
)

const Loadingx = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih PD Terlebih Dahulu</Text>
    </View>
)



@inject('Dropdown', 'Provinsi', 'Rayon')
@observer
export default class DaftarUlangScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;



  constructor(props) {
    super(props);
    this.state = {
      data: [],
      myToken: '',
      loading: false,
      avatar:'',
      avatarx:'',
      secureTextEntry: true,
      avatarSource: null,
      ktaBack: null,
      ktaFront: null,
      foto:null,
      ktp:null,
      simpleDate: new Date(),
      skep_pensiun:null,
      kta_tnipolri:null,
      dok_tambahan:null,
      aktalahir:null,
      kk:null,
      videoSource: null,
      selected2: undefined,
      title1:'',
      firstname:'',
      lastname:'',
      email:null,
      title2:'',
      alias:'',
      nik:'',
      birthplace:'',
      simpleText: '',
      birthday: '',
      sex:'',
      religions_id:'',
      degree:'',
      occupations:'',
      maritalstatus:'',
      spousename:'',
      mothername:'',
      h_province_id:'',
      h_cities_id:'',
      h_districts_id:'',
      h_villages_id:'',
      h_address_1:'',
      h_address_2:'',
      h_rtrw:'',
      h_postcodes_id:'',
      h_phone:'',
      mobile:'',
      o_province_id:'',
      o_cities_id:'',
      o_districts_id:'',
      o_villages_id:'',
      o_address_1:'',
      o_address_2:'',
      o_rtrw:'',
      o_postcodes_id:'',
      o_phone:'',
      p_nrp:'',
      p_name:'',
      p_military_status_id:'',
      p_military_uo_id:'',
      p_military_ranks_id:'',
      p_military_position_id:'',
      p_childrens:'',
      p_religions_id:'',
      p_kinship_id:'',
      p_info:'',
      g_members_memberstatus_id:'',
      g_registered_at:'',
      g_dpd_id:'',
      g_dpw_id:'',
      g_dpra_id:'',
      g_jabatan_id:'',
      g_jabatan_start:'',
      g_jabatan_end:'',
      d_kta_front:'',
      d_kta_back:'',
      d_foto:'',
      d_ktp:'',
      d_skep_pensiun:'',
      d_kta_tnipolri:'',
      d_dok_tambahan:'',
      d_aktalahir:'',
      d_kk:'',
      kosong:'',
      kota:[],
      date:""
      



    }
   
  }
 /////////

 state = { open: false };

 

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });


 async getSubmit() {
    this.setState({
      loading: true
    });

   
    //console.log('coords', coords)
    this.getKey();


    // setTimeout(() => {
    //  //this.onSubmit();
    //  this.getKey();
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }
  
   async getLoad() {
    this.setState({
      loading: true
    });

    const filex = {
          // `uri` can also be a file system path (i.e. file://)
          uri: 'file:///storage/emulated/0/Pictures/image-19084565-ff90-4e73-b33b-a5d24ccbacc5.jpg',
          name: 'image-19084565-ff90-4e73-b33b-a5d24ccbacc5.jpg',
          type: 'image/jpeg'
        }

    setTimeout(() => {
       this.getToken();
       //that.getLoad();
       this.getTokenProv();
     //this._goToMainScreen();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
      this.setState({
        loading: false,
        defaulFile: filex
      });
    }, 2500);
  }



   async getMain() {
    this.setState({
      loading: true
    });

    setTimeout(() => {
     this._goToMainScreen();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
      this.setState({
        loading: false,
      });
    }, 2000);
  }




showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'day'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'day'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

////

showPickerAnggota = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'day'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'day'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

showPickerRegis = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_at'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_at'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

  showPickerStart = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_start'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_start'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };



   showPickerEnd = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_end'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_end'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };



  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       //that.getToken();
       that.getLoad();
       //that.getTokenProv();

        AsyncStorage.removeItem('@Card:key', (err, result) => {
                  console.log("Hapus card");
                });

        AsyncStorage.removeItem('@CardDepan:key', (err, result) => {
                  console.log("Hapus card");
                });


        AsyncStorage.removeItem('@CardBelakang:key', (err, result) => {
                  console.log("Hapus card");
                });
        
    }


  componentWillUnmount() {

      this.props.Provinsi.clearItems();
      this.props.Dropdown.clearItems();
      this.props.Rayon.clearItems();
        
    }
   

////////


  async getToken() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Dropdown.fetchReligion(value); 
      this.props.Dropdown.fetchTitle1(value);
      this.props.Dropdown.fetchTitle2(value);
      this.props.Dropdown.fetchGender(value);
      this.props.Dropdown.fetchOccupations(value);
      this.props.Dropdown.fetchMartial(value);
      this.props.Dropdown.fetchDegree(value);
      this.props.Dropdown.fetchMilitary(value);
      this.props.Dropdown.fetchKesatuan(value);
      this.props.Dropdown.fetchKinship(value);
      this.props.Dropdown.fetchMembership(value);
      this.props.Dropdown.fetchJabatan(value);
      this.props.Rayon.fetchPd(value);
      //this.props.Provinsi.fetchProvinsi(value); 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getTokenProv() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchProvinsi(value); 
      this.props.Provinsi.fetchProvinsi(value); 
      //this.props.Provinsi.fetchProvinsi(value); 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getKota(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKota(value, id);  
      this.getCity(value, id) 
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

getCity(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/regency/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            city: result,
         });

          console.log(that.state.city)

        })
        .catch((error) => { console.error(error); });
  }

  async getPc(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchPc(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getRayon(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchRayon(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getKecamatan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKecamatan(value, id); 
      this.getCamat(value, id)  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

async getCamatan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchKecamatan(value, id);  
      //console.log(this.state.myToken)
      //this.getCamat(value, id) 
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getKelurahan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKelurahan(value, id);  
      //console.log(this.state.myToken)
           this.getKel(value, id) 
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
  async getLurah(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchKelurahan(value, id); 
      //this.getKel(value, id)  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
  async getKodeposx(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getKodepos2(value, id );
      //this.props.Provinsi.fetchKodepos(value, id);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

   async getKodepos(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getPos(value, id );
      //this.props.Provinsi.fetchKodepos(value, id);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getKode(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getKodepos(value, id );
      //this.props.Provinsi.fetchKodepos(value, id);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  getCamat(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/subdistrict/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            district: result,
         });

          console.log(that.state.district)

        })
        .catch((error) => { console.error(error); });
  }
/////////////////////////
   getKel(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/village/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            village: result,
         });

          console.log(that.state.village)

        })
        .catch((error) => { console.error(error); });
  }
  getKodepos(value, id) {
      // Set loading to true to display a Spinner
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'list/postcode/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            o_postcodes_id: result.postcode,
         });

          //console.log(result.postcode)

        })
        .catch((error) => { console.error(error); });
  }
  getKodepos2(value, id) {
      // Set loading to true to display a Spinner
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'list/postcode/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            h_postcodes_id: result.postcode,
         });

          console.log(result.postcode)

        })
        .catch((error) => { console.error(error); });
  }



 /////////

 onSelectedReligion = (key, value) => {
    this.setState({ religion_id: key });

    //console.log(this.state.religion_id)
  };


  // onInputChange(e) {
  //   this.setState({ input: e.target.value }, () => {
  //       console.log(`state: ${this.state}, value: ${e.target.value}`);
  //     });
  // }

  onSelectedTitle1 = (titlex1) => {

    const Title1 = titlex1.toString();
    this.setState({title1: Title1, titlex1 });
    console.log(Title1)
  }

  onSelectedTitle2 = (titlex2) => {
    const Title2 = titlex2.toString();
    this.setState({ title2: Title2, titlex2 });

    //console.log(this.state.religion_id)
  };


  onSelectedGender = (sexx) => {
   const Sex = sexx.toString();
    this.setState({ sex: Sex, sexx });

    console.log(Sex)
  };

  onSelectedReligion = (religionsx_id) => {

    const religi = religionsx_id.toString();
    this.setState({ religions_id: religi, religionsx_id });

    //console.log(religi)
  };

  onSelectedReligion2 = (p_religionsx_id) => {
    const p_religi = p_religionsx_id.toString();
    this.setState({ p_religions_id: p_religi, p_religionsx_id });

    //console.log(this.state.religion_id)
  };

   onSelectedDegree = (degreex) => {
     const Degree = degreex.toString();
    this.setState({ degree: Degree, degreex });

    //console.log(this.state.religion_id)
  };

  onSelectedOccu = (occupationsx) => {
    const Occu = occupationsx.toString();
    this.setState({ occupations: Occu, occupationsx });

    //console.log(this.state.religion_id)
  };

   onSelectedMartial = (maritalstatusx) => {
    const Mart = maritalstatusx.toString();
    this.setState({ maritalstatus: Mart, maritalstatusx });

    console.log(Mart)
  };

onSelectedMilitary = (p_military_statusx_id) => {
    const MilStat = p_military_statusx_id.toString();
    this.setState({ p_military_status_id: MilStat, p_military_statusx_id });

    //console.log(this.state.religion_id)
  };


  onSelectedKesatuan = (p_military_uox_id) => {
     const Kesat = p_military_uox_id.toString();
    this.setState({ p_military_uo_id: Kesat, p_military_uox_id });

    //console.log(this.state.religion_id)
  };

  onSelectedKinship = (p_kinshipx_id) => {
     const Kinsh = p_kinshipx_id.toString();
    this.setState({ p_kinship_id: Kinsh, p_kinshipx_id });

    //console.log(this.state.religion_id)
  };

  onSelectedMembership = (g_members_memberstatusx_id) => {

    const Mem = g_members_memberstatusx_id.toString();
    this.setState({ g_members_memberstatus_id: Mem, g_members_memberstatusx_id });

    //console.log(this.state.religion_id)
  };

  onSelectedJab = (g_jabatanx_id) => {
     const Jab = g_jabatanx_id.toString();
    this.setState({ g_jabatan_id: Jab, g_jabatanx_id });

    //console.log(this.state.religion_id)
  };



onSelectedProv = (provinsi) => {
    const Prov = provinsi.toString();
    this.setState({h_province_id: Prov, provinsi });
    this.getKota(provinsi);
    //console.log(Prov)
  }

  onSelectedPd = (g_dpdx_id) => {

    const GPD = g_dpdx_id.toString();
    this.setState({ g_dpd_id: GPD, g_dpdx_id });

    this.getPc(g_dpdx_id);

    console.log(GPD)
  };

  onSelectedPc = (g_dpwx_id) => {
    const GPC= g_dpwx_id.toString();
    this.setState({ g_dpw_id: GPC, g_dpwx_id });

    this.getRayon(g_dpwx_id);

    //console.log(this.state.g_dpw_id)
  };

  onSelectedRayon = (g_dprax_id) => {
    const GRAYON= g_dprax_id.toString();
    this.setState({ g_dpra_id: GRAYON, g_dprax_id });
  };

 onSelectedProv2 = (o_provincex_id) => {
    const ProvOf = o_provincex_id.toString();
    this.setState({ o_province_id: ProvOf, o_provincex_id });

    this.getKota(o_provincex_id);

    //console.log(this.state.religion_id)
  };
   onSelectedKota = (key, value) => {
    this.setState({ h_cities_id: key });

    this.getKecamatan(key);

    //console.log(this.state.religion_id)
  };

  onSelectedKota2 = (o_citiesx_id) => {
     const CityOf = o_citiesx_id.toString();

    this.setState({ o_cities_id: CityOf, o_citiesx_id });

    this.getCamatan(o_citiesx_id);

    //console.log(this.state.religion_id)
  };

   onSelectedKec = (key, value) => {
    this.setState({ h_districts_id: key });

    this.getKelurahan(key);

    //console.log(this.state.religion_id)
  };

  onSelectedKec2 = (o_districtsx_id) => {
      const KecOf = o_districtsx_id.toString();
    this.setState({ o_districts_id: KecOf, o_districtsx_id });

    this.getLurah(o_districtsx_id);

    //console.log(this.state.religion_id)
  };


 onSelectedKel = (h_villagesx_id) => {

      const Kell = h_villagesx_id.toString();
    this.setState({ h_villages_id: Kell, h_villagesx_id });

    this.getKodeposx(h_villagesx_id);

    //console.log(this.state.religion_id)
  };

  onSelectedKel2 = (o_villagesx_id) => {
     const Kell2 = o_villagesx_id.toString();
    this.setState({ o_villages_id: Kell2, o_villagesx_id });

    this.getKode(o_villagesx_id);

    //console.log(this.state.religion_id)
  };
 ////////////

onSelectedProvinsi = (provinsi) => {
    const Prov = provinsi.toString();
    this.setState({h_province_id: Prov, provinsi });
    this.getKota(provinsi);
    //console.log(Prov)
  }


onSelectedKotax = (kotax) => {

   const Cit = kotax.toString();
    this.setState({h_cities_id: Cit, kotax });
    this.getKecamatan(kotax);
    //console.log(this.state.kotax)
  }

onSelectedKecx = (kec) => {

    const Dist = kec.toString();
    this.setState({h_districts_id: Dist, kec });
    this.getKelurahan(kec);
    //console.log(selectedItems)
  }


onSelectedKelx = (h_villagesx_id) => {

   const Vil = h_villagesx_id.toString();
    this.setState({h_villages_id: Vil, h_villagesx_id});
    //this.getKodepos(h_villages_id);
    //console.log(h_villages_id)
  }


selectPhotoKtaFront() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktaFront: file
        });

        console.log(this.state.ktaFront)
      }
    });
  }
 /////////////////
 selectPhotoKtaBack() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktaBack: file
        });

        console.log(this.state.ktaBack)
      }
    });
  }

  /////////

  // getPos(value, id) {
  //     // Set loading to true to display a Spinner
  

  //     //console.log(this.state.myToken)
      
      
  //     var url =  DevApi + 'list/postcode/' + id;
  //     var that = this;
  //     return fetch(url, {
  //             method: 'GET',
  //             headers: {
  //               'Content-Type': 'application/json',
  //               'Authorization': value 
  //             }
  //       })
  //       .then(function(response) {
  //         return response.json();
  //       }).then(function(result) {
  //         that.setState({
  //           h_postcodes_id: result,
  //        });

  //         console.log(that.state.h_postcodes_id)

  //       })
  //       .catch((error) => { console.error(error); });
  // }

  /////////////////
 selectPhotoFoto() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          foto: file
        });

        console.log(this.state.foto)
      }
    });
  }

  /////////////////
 selectPhotoKtp() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktp: file
        });

        console.log(this.state.ktp)
      }
    });
  }

   /////////////////
 selectPhotoSkep() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          skep_pensiun: file
        });

        console.log(this.state.skep_pensiun)
      }
    });
  }

 /////////////////
 selectPhotoKtaPolri() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          kta_tnipolri: file
        });

        console.log(this.state.kta_tnipolri)
      }
    });
  }

/////////////////
 selectPhotoDok() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          dok_tambahan: file
        });

        console.log(this.state.dok_tambahan)
      }
    });
  }
  ///

   onCancel = () => {
    this.SectionedMultiSelect._removeAllItems()

    this.setState({
      selectedItems: '',
    })
    console.log(this.state.selectedItems);
  }

   onCancel2 = () => {
    this.SectionedMultiSelect2._removeAllItems()

    this.setState({
      selectedItems: '',
    })
    console.log(this.state.selectedItems);
  }

  /////////////////
 selectPhotoAkta() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          aktalahir: file
        });

        console.log(this.state.aktalahir)
      }
    });
  }
  ///

   /////////////////
 selectPhotoKK() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          kk: file
        });

        console.log(this.state.kk)
      }
    });
  }
  ///

  onPress = () => {
    this.getKey();
  }


  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getNews(value ); 
      this.onSubmit(value );     
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 

  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });

    console.log(this.state.selected2)
  }


  onSubmit(value){


    if(this.state.firstname == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nama Depan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }

    else if(this.state.lastname  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nama Belakang Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.nik  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nomer NIK / KTP Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.birthplace  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Tempat Lahir Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }

    else if(this.state.birthday  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Tanggal Lahir Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.mothername  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nama Ibu Kandung Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.sex  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Gender Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.religions_id  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Agama Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.occupations  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Pekerjaan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.maritalstatus == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Status Perkawinan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else  if(this.state.h_province_id  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data Provinsi  Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.h_cities_id  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data Kota Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.h_districts_id  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data Kecamatan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.h_villages_id  == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data Kelurahan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.h_address_1  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Data Alamat  Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    
    else if(this.state.mobile  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nomer HP Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.p_nrp  == '') {

       console.log('wajib diisi')

       ToastAndroid.show('NRP Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.p_name == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Nama Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.p_military_status_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Status Kemiliteran Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.p_military_uo_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Kesatuan Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.p_military_ranks_id == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Pangkat Terakhir Orang Tua  Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.p_military_position_id == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Jabatan Terakhir Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.p_childrens == '') {

       console.log('wajib diisi')

       ToastAndroid.show('Jumlah Anak Orang Tua  Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.p_religions_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Agama Orang Tua Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.p_kinship_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Hubungan Orang Tua dengan anggota Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
     else if(this.state.g_dpd_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data PD Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.g_dpw_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data PC Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.g_dpra_id == 0) {

       console.log('wajib diisi')

       ToastAndroid.show('Data Rayon Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.ktaFront == null) {

       console.log('wajib diisi')

      ToastAndroid.show('Foto KTA Depan Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.ktaBack == null) {

       console.log('wajib diisi')

      ToastAndroid.show('Foto KTA belakang Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    else if(this.state.foto == null) {

       console.log('wajib diisi')

      ToastAndroid.show('Foto Anggota Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }
    
    else if(this.state.ktp == null) {

       console.log('wajib diisi')

       ToastAndroid.show('Foto KTP Wajib diisi', ToastAndroid.SHORT);
       //this.openModal();
       this.setState({
            loading: false,
          });


    }

    // else if(this.state.skep_pensiun == null) {

    //    console.log('wajib diisi')

    //   ToastAndroid.show('Data * Wajib diisi', ToastAndroid.SHORT);
    //    //this.openModal();
    //    this.setState({
    //         loading: false,
    //       });


    // }
    //  else if(this.state.kta_tnipolri == null) {

    //    console.log('wajib diisi')

    //    ToastAndroid.show('Data * Wajib diisi', ToastAndroid.SHORT);
    //    //this.openModal();
    //    this.setState({
    //         loading: false,
    //       });


    // }
    // else if(this.state.aktalahir == null) {

    //    console.log('wajib diisi')

    //    ToastAndroid.show('Data * Wajib diisi', ToastAndroid.SHORT);
    //    //this.openModal();
    //    this.setState({
    //         loading: false,
    //       });


    // }
    // else if(this.state.kk == null) {

    //    console.log('wajib diisi')

    //    ToastAndroid.show('Data * Wajib diisi', ToastAndroid.SHORT);
    //    //this.openModal();
    //    this.setState({
    //         loading: false,
    //       });


    // }





    else{
      this.setState({
            loading: false,
          });



      console.log('ok')
      this._handleClick(
       this.state.title1,
       this.state.title2,
       this.state.firstname, 
       this.state.lastname,
       this.state.email,
       this.state.alias,
       this.state.nik,
       this.state.birthplace,
       this.state.birthday,
       this.state.sex,
       this.state.religions_id,
       this.state.degree,
       this.state.occupations,
       this.state.maritalstatus,
       this.state.spousename,
       this.state.mothername,
       this.state.h_province_id,
       this.state.h_cities_id,
       this.state.h_districts_id,
       this.state.h_villages_id,
       this.state.h_address_1, 
       this.state.h_address_2,
       this.state.h_rtrw,
       this.state.h_postcodes_id,
       this.state.h_phone,
       this.state.mobile,
       this.state.o_province_id,
       this.state.o_cities_id,
       this.state.o_districts_id,
       this.state.o_villages_id,
       this.state.o_address_1,
       this.state.o_address_2,
       this.state.o_rtrw,
       this.state.o_postcodes_id,
       this.state.o_phone,
       this.state.p_nrp,
       this.state.p_name,
       this.state.p_military_status_id,
       this.state.p_military_uo_id,
       this.state.p_military_ranks_id,
       this.state.p_military_position_id,
       this.state.p_childrens,
       this.state.p_religions_id,
       this.state.p_kinship_id,
       this.state.p_info,
       this.state.g_members_memberstatus_id,
       this.state.g_registered_at,
       this.state.g_dpd_id,
       this.state.g_dpw_id,
       this.state.g_dpra_id,
       this.state.g_jabatan_id,
       this.state.g_jabatan_start,
       this.state.g_jabatan_end,
       this.state.ktaFront,
       this.state.ktaBack,
       this.state.foto,
       this.state.ktp,
       this.state.kosong,
       this.state.kosong,
       this.state.kosong,
       this.state.kosong,
       this.state.kosong,
       this.state.kosong,
       this.state.kosong







      );
    // var url =  DevApi + 'member/register';
    // var _this = this;
    // var formData = new FormData();

    // formData.append('title1', this.state.title1);
    // formData.append('title2', this.state.title2);
    // formData.append('firstname', this.state.firstname);
    // formData.append('lastname', this.state.lastname);
    // formData.append('alias', this.state.alias);
    // formData.append('nik', this.state.nik);
    // formData.append('birthplace', this.state.birthplace);
    // formData.append('birthday', this.state.birthday);
    // formData.append('sex', this.state.sex);
    // formData.append('religions_id', this.state.religions_id);
    // formData.append('degree', this.state.degree);
    // formData.append('occupations', this.state.occupations);
    // formData.append('maritalstatus', this.state.maritalstatus);
    // formData.append('spousename', this.state.spousename);
    // formData.append('h_province_id', this.state.h_province_id);
    // formData.append('h_cities_id', this.state.h_cities_id);
    // formData.append('h_districts_id', this.state.h_districts_id);
    // formData.append('h_villages_id', this.state.h_villages_id);
    // formData.append('h_address_1', this.state.h_address_1);
    // formData.append('h_address_2', this.state.h_address_2);
    // formData.append('h_rtrw', this.state.h_rtrw);
    // formData.append('h_postcodes_id', this.state.h_postcodes_id);
    // formData.append('h_phone', this.state.h_phone);
    // formData.append('mobile', this.state.mobile);
    // formData.append('o_province_id', this.state.o_province_id);
    // formData.append('o_cities_id', this.state.o_cities_id);
    // formData.append('o_districts_id', this.state.o_districts_id);
    // formData.append('o_villages_id', this.state.o_villages_id);
    // formData.append('o_address_1', this.state.o_address_1);
    // formData.append('o_address_2', this.state.o_address_2);
    // formData.append('o_rtrw', this.state.o_rtrw);
    // formData.append('o_postcodes_id', this.state.o_postcodes_id);
    // formData.append('o_phone', this.state.o_phone);
    // formData.append('p_nrp', this.state.p_nrp);
    // formData.append('p_name', this.state.p_name);
    // formData.append('p_military_status_id', this.state.p_military_status_id);
    // formData.append('p_military_uo_id', this.state.p_military_uo_id);
    // formData.append('p_military_ranks', this.state.p_military_ranks_id);
    // formData.append('p_military_position', this.state.p_military_position_id);
    // formData.append('p_childrens', this.state.p_childrens);
    // formData.append('p_religions_id', this.state.p_religions_id);
    // formData.append('p_kinship_id', this.state.p_kinship_id);
    // formData.append('p_info', this.state.p_info);
    // formData.append('g_members_memberstatus_id', this.state.g_members_memberstatus_id);
    // formData.append('g_organizations_id', this.state.g_organizations_id);
    // formData.append('g_registered_at', this.state.g_registered_at);
    // formData.append('g_dpd_id', this.state.g_dpd_id);
    // formData.append('g_dpw_id', this.state.g_dpw_id);
    // formData.append('g_dpra_id', this.state.g_dpra_id);
    // formData.append('g_jabatan_id', this.state.g_jabatan_id);
    // formData.append('g_jabatan_start', this.state.g_jabatan_start);
    // formData.append('g_jabatan_end', this.state.g_jabatan_end);
    // formData.append('d_kta_front', this.state.ktaFront);
    // formData.append('d_kta_back', this.state.ktaBack);
    // formData.append('d_foto', this.state.foto);
    // formData.append('d_ktp', this.state.ktp);
    // formData.append('d_skep_pensiun', this.state.skep_pensiun);
    // formData.append('d_kta_tnipolri', this.state.kta_tnipolri);
    // formData.append('d_dok_tambahan', this.state.dok_tambahan);
    // formData.append('d_aktalahir', this.state.aktalahir);
    // formData.append('d_kk', this.state.kk);

    // console.log(formData)

    // //  this.openModal();
 
    // var xhr = new XMLHttpRequest();
    // xhr.open('POST', url);
    // xhr.responseType = 'json';
    // console.log('OPENED', xhr.status);

    // //xhr.timeout = 2000; // time in milliseconds

    // xhr.onprogress = function () {
    //     console.log('LOADING', xhr.status);
    //     //Alert.alert("Sedang Mengajukan");
    //     //ToastAndroid.show('Sedang Mengajukan', ToastAndroid.SHORT);
    // };


    // xhr.onload = function(e) {
    //   if (this.status == 200) {
  
    //      setTimeout(() => {
    //       _this.openModal();
    //       _this.setState({
    //         loading: false,
    //       });
    //     }, 1500);
    //     /////////////
    //     //Alert.alert("Berhasil Terdaftar");
    //     //console.log('response', this.response.printedcard); // JSON response  
    //   }
    // };
    
    // xhr.setRequestHeader('Authorization', value);
    // xhr.setRequestHeader('Content-Type', 'multipart/form-data');
    // xhr.setRequestHeader('Accept', 'application/json');
    // xhr.send(formData);


    
}



    

     //////


 // AsyncStorage.setItem('@FormData:key', JSON.stringify(formData) (err, result) => {
 //                  console.log("Token Remove");
     
 //                });

 // AsyncStorage.setItem('@FormData:Key', JSON.stringify(formData), (err, result) => {

 //   setTimeout(() => {
 //     this._handleClick(this.props.firstName, this.props.lastName, this.props.avatar);
 //      this.setState({
 //        loading: false,
 //      });
 //    }, 1500);


 // });

  }


//   getDownload(value){


//   var date      = new Date();
//     var url       = this.props.print;
//     var ext       = this.extention(url);
//     ext = "."+ext[0];
//     const { config, fs } = RNFetchBlob
//     let PictureDir = fs.dirs.PictureDir
//     let options = {
//       fileCache: true,
//       addAndroidDownloads : {
//         useDownloadManager : true,
//         notification : true,
//         path:  PictureDir + "/image_"+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
//         description : 'Image'
//       }
//     }
//     config(options).fetch('GET', url).then((res) => {
//       Alert.alert("Success Downloaded");
//     });
//   }
// extention(filename){
//     return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;


// //////////end
//   }

////////////////


// renderButton(){

// if (this.state.firstname == '' && )


// }

 
  render() {
    const { h_province_id, o_province_id} = this.state;
    const {Dropdown, Provinsi, Rayon} = this.props;
    const store = this.props.Dropdown;
    const prov = this.props.Provinsi;
    const ray = this.props.Rayon;

    const listCity = this.state.city || []
    const listCamat = this.state.district || []
    const listKel = this.state.village || []

    const listTitle1 = toJS(store.title1)
    const listTitle2 = toJS(store.title2)
    const listGender = toJS(store.gender)
    const listReligi = toJS(store.religions)
    const listDegree = toJS(store.degree)
    const listOccu = toJS(store.occupations)
    const listMartial = toJS(store.maritalstatus)
    const listMilitary = toJS(store.militarystatus)
    const listKesatuan = toJS(store.kesatuan)
    const listKinship = toJS(store.kinship)
    const listMembership = toJS(store.membership)
    const listJabatan = toJS(store.jabatan)

    const listProvinsi = toJS(prov.provinsi)
    const listKota = toJS(prov.kota)
    const listKecamatan = toJS(prov.kecamatan)
    const listKelurahan = toJS(prov.kelurahan)

    const listPd = toJS(ray.pd)
    const listPc = toJS(ray.pc)
    const listRayon = toJS(ray.rayon)

    const KODEPOS = this.state.h_postcodes_id;
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       <Loader
          loading={this.state.loading} />
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>
         
         <View style={{flex:1, flexDirection:'row'}}>
          
          <View style={{marginTop:40, marginLeft:10}}>
             <Image
              source={{uri: this.props.avatar}}
              style={{width: 40, height: 40,borderRadius: 40,borderColor:'#ccc',borderWidth:4}} />
          </View>

          <View style={{marginTop:40, marginLeft:10}}>
            <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.itemHeaderText,
              CommonStyles.whiteColor]}>{this.props.firstName} {this.props.lastName}</Text>
             <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                Pendaftaran Ulang Anggota
              </Text>
          </View>
 

         </View>

         
        </View>
       
        <View style={{flex: 5, justifyContent: 'center', backgroundColor:'#000' }}>
          <Container>
        <Tabs 
         renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#404f3d" }}/>}>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Personal</Text>
              </TabHeading>}>

              <ScrollView keyboardShouldPersistTaps="always">

                  <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form> 
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Gelar Depan</Label>
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listTitle1.map((category) => (
                           
                           {name: category.title, id: category.id,}


                          ))}
                        
                          uniqueKey='id'
                          single={true}
                      
                          ref={SectionedMultiSelect => this.SectionedMultiSelect = SectionedMultiSelect}
                           onCancel={this.onCancel}
                          showCancelButton
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Gelar Depan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedTitle1.bind(this)}
                          selectedItems={this.state.titlex1}
                        />
                        </View>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Depan <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.firstname} 
                          onChangeText={(firstname) => this.setState({firstname})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Belakang <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.lastname} 
                          onChangeText={(lastname) => this.setState({lastname})}
                          />
                        </Item>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Gelar Belakang</Label>
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listTitle2.map((category) => (
                           
                           {name: category.title, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          ref={SectionedMultiSelect2 => this.SectionedMultiSelect2 = SectionedMultiSelect2}
                           onCancel={this.onCancel2}
                          showCancelButton
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Gelar Belakang"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedTitle2.bind(this)}
                          selectedItems={this.state.titlex2}
                        />
                        </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Alias</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.alias} 
                          onChangeText={(alias) => this.setState({alias})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor KTP / Kartu Pelajar <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.nik} 
                          onChangeText={(nik) => this.setState({nik})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Tempat Lahir <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.birthplace} 
                          onChangeText={(birthplace) => this.setState({birthplace})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Tanggal Lahir <Text style={{color:'red'}}>* </Text></Label>
                       

                        <Item regular>
                   <DatePicker
                            style={{width: '100%'}}
                            date={this.state.birthday}
                            mode="date"
                            placeholder="Isi Tanggal Lahir"
                            format="YYYY-MM-DD"
                            minDate="1945-12-31"
                            maxDate="2018-12-31"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateIcon: {
                                position: 'absolute',
                                right: 0,
                                top: 4,
                                marginLeft: 0
                              },
                             dateText:{
                              textAlign:'left',
                              marginLeft: 5
                             },
                             placeholderText:{
                                marginLeft: 5
                             },
                              dateInput: {
                                marginLeft: 0,
                                borderWidth: 0,
                                alignItems: 'flex-start',
                                height:50 
                              }
                              // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(birthday) => {this.setState({birthday: birthday})}}
                          />
                        </Item>

                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jenis Kelamin <Text style={{color:'red'}}>* </Text></Label>
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listGender.map((category) => (
                           
                           {name: category.gender, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Jenis Kelamin"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedGender.bind(this)}
                          selectedItems={this.state.sexx}
                        />
                        </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Agama <Text style={{color:'red'}}>* </Text></Label>
                         <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listReligi.map((category) => (
                           
                           {name: category.religion, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Agama"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedReligion.bind(this)}
                          selectedItems={this.state.religionsx_id}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pendidikan Terakhir <Text style={{color:'red'}}>* </Text></Label>
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listDegree.map((category) => (
                           
                           {name: category.degree, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                           searchPlaceholderText="Cari"
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Pendidikan Terakhir"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedDegree.bind(this)}
                          selectedItems={this.state.degreex}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pekerjaan <Text style={{color:'red'}}>* </Text></Label>
                    <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listOccu.map((category) => (
                           
                           {name: category.occupation, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Pekerjaan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedOccu.bind(this)}
                          selectedItems={this.state.occupationsx}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Status Perkawinan <Text style={{color:'red'}}>* </Text></Label>
                      <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listMartial.map((category) => (
                           
                           {name: category.status, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Status Perkawinan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedMartial.bind(this)}
                          selectedItems={this.state.maritalstatusx}
                        />
                       </View>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Pasangan</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.spousename} 
                          onChangeText={(spousename) => this.setState({spousename})}
                          />
                        </Item>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Gadis Ibu Kandung <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.mothername} 
                          onChangeText={(mothername) => this.setState({mothername})}
                          />
                        </Item>

                     </Form>

                 </Content>

              </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Alamat Rumah</Text>
              </TabHeading>}>

              <ScrollView>

                 <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Provinsi <Text style={{color:'red'}}>* </Text></Label>
                    <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={this.props.Provinsi.provinsi.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Provinsi"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedProvinsi}
                          selectedItems={this.state.provinsi}
                        />
                    </View>
                 <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kota / Kabupaten <Text style={{color:'red'}}>* </Text></Label>
                   <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                      <SectionedMultiSelect
                          items={listCity.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kota"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKotax}
                          selectedItems={this.state.kotax}
                        />
                    </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kecamatan <Text style={{color:'red'}}>* </Text></Label>
           
                 <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={listCamat.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                           colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kecamatan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKecx}
                          selectedItems={this.state.kec}
                        />
                    </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kelurahan <Text style={{color:'red'}}>* </Text></Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={listKel.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                           colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          style={{fontSize:8}}
                          selectText="Pilih Kelurahan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKel}
                          selectedItems={this.state.h_villagesx_id}
                        />
                  </View>
                   <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Alamat Sesuai KTP <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                           placeholder='contoh: Jl. Pahlawan Revolusi No 45'
                          placeholderTextColor={'#ccc'}
                          maxLength={50}
                
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.h_address_1} 
                          onChangeText={(h_address_1) => this.setState({h_address_1})}
                          />
                        </Item>

                 
                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>RT / RW Sesuai KTP</Label>
                        <Item regular>
                          <Input 
                          placeholder="contoh: 001/002"
                          keyboardType="default"
                          placeholderTextColor={'#ccc'}
                          returnKeyType="next"
                          value={this.state.h_rtrw} 
                          onChangeText={(h_rtrw) => this.setState({h_rtrw})}
                          />
                        </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>KodePos </Label>
                        <Item regular>
                       

                          <TextInput
                          placeholder=''
                          style={CommonStyles.textInputLeft}
                          underlineColorAndroid='transparent'
                          keyboardType="numeric"
                          returnKeyType="next"
                          onChangeText={(h_postcodes_id) => this.setState({h_postcodes_id})}
                          value={this.state.h_postcodes_id}
                        />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Telepon</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.h_phone} 
                          onChangeText={(h_phone) => this.setState({h_phone})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Handphone <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.mobile} 
                          onChangeText={(mobile) => this.setState({mobile})}
                          />
                        </Item>

                </Form>

                </Content> 
             </ScrollView>
             
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Alamat Kantor</Text>
              </TabHeading>}>

              <ScrollView>

                 <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Provinsi</Label>
                   <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={this.props.Provinsi.provinsi.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Provinsi"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedProv2.bind(this)}
                          selectedItems={this.state.o_provincex_id}
                        />
                    </View>
                 <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kota / Kabupaten</Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                 
                      <SectionedMultiSelect
                          items={listCity.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kota"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKota2.bind(this)}
                          selectedItems={this.state.o_citiesx_id}
                        />
                   </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kecamatan</Label>
           
                   <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listKecamatan.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kecamatan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKec2.bind(this)}
                          selectedItems={this.state.o_districtsx_id}
                        />
                    </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kelurahan</Label>
           
                 <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listKelurahan.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kelurahan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKel2.bind(this)}
                          selectedItems={this.state.o_villagesx_id}
                        />
                    </View>
                   <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Alamat Kantor Lengkap</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          maxLength={28}
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.o_address_1} 
                          onChangeText={(o_address_1) => this.setState({o_address_1})}
                          />
                        </Item>

                 
                    
                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>RT / RW Kantor</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.o_rtrw} 
                          onChangeText={(o_rtrw) => this.setState({o_rtrw})}
                          />
                        </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>KodePos</Label>
                        <Item regular>
                      
                          <TextInput
                          placeholder=''
                          style={CommonStyles.textInputLeft}
                          underlineColorAndroid='transparent'
                          keyboardType="default"
                          returnKeyType="next"
                          onChangeText={(o_postcodes_id) => this.setState({o_postcodes_id})}
                          value={this.state.o_postcodes_id}
                        />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Telepon Kantor</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.o_phone} 
                          onChangeText={(o_phone) => this.setState({o_phone})}
                          />
                        </Item>
                       
                      

                </Form>


                </Content> 
             </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Orang Tua</Text>
              </TabHeading>}>
              <ScrollView>
                <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                  <Form>
                    <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>N.R.P/N.I.P Orang Tua<Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_nrp} 
                          onChangeText={(p_nrp) => this.setState({p_nrp})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Orang Tua <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_name} 
                          onChangeText={(p_name) => this.setState({p_name})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Status Kemiliteran Orang Tua <Text style={{color:'red'}}>* </Text></Label>
                   
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>  
                        <SectionedMultiSelect
                          items={listMilitary.map((category) => (
                           
                           {name: category.status, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Status Kemiliteran"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedMilitary.bind(this)}
                          selectedItems={this.state.p_military_statusx_id}
                        />
                       </View>
                          <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kesatuan Dinas Orang Tua<Text style={{color:'red'}}>* </Text></Label>
                   
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>  
                        <SectionedMultiSelect
                          items={listKesatuan.map((category) => (
                           
                           {name: category.uo, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Kesatuan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKesatuan.bind(this)}
                          selectedItems={this.state.p_military_uox_id}
                        />
                       </View>
                          <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pangkat Terakhir Orang Tua<Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_military_ranks_id} 
                          onChangeText={(p_military_ranks_id) => this.setState({p_military_ranks_id})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jabatan Terakhir Orang Tua<Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_military_position_id} 
                          onChangeText={(p_military_position_id) => this.setState({p_military_position_id})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jumlah Anak Orang Tua<Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.p_childrens} 
                          onChangeText={(p_childrens) => this.setState({p_childrens})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Agama Orang Tua<Text style={{color:'red'}}>* </Text></Label>
           
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listReligi.map((category) => (
                           
                           {name: category.religion, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Agama"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedReligion2.bind(this)}
                          selectedItems={this.state.p_religionsx_id}
                        />
                     </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Hubungan Orang Tua dengan anggota <Text style={{color:'red'}}>* </Text></Label>
                  <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                     <SectionedMultiSelect
                          items={listKinship.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Hubungan dengan Anggota"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKinship.bind(this)}
                          selectedItems={this.state.p_kinshipx_id}
                        />
                    </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Keterangan Lainnya</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_info} 
                          onChangeText={(p_info) => this.setState({p_info})}
                          />
                        </Item>

                  </Form>

                  </Content> 
               </ScrollView>
            
          </Tab>

          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Organisasi</Text>
              </TabHeading>}>

              <ScrollView>
               <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                  <Form>
                    
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Keanggotaan Sejak</Label>
                        <Item regular>
                          <Input 
            
                          placeholder="contoh: 2018"
                          placeholderTextColor={'#ccc'}
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.g_registered_at} 
                          onChangeText={(g_registered_at) => this.setState({g_registered_at})}
                          />
                          
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PD saat ini<Text style={{color:'red'}}>* </Text></Label>
                     <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listPd.map((categoryz, index) => (
                           
                           {name: categoryz.pd, idz: categoryz.id,}


                          ))}
                          uniqueKey='idz'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih PD"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPd.bind(this)}
                          selectedItems={this.state.g_dpdx_id}
                        />
                      </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PC saat ini<Text style={{color:'red'}}>* </Text></Label>
           
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listPc.map((category) => (
                           
                           {name: category.pc, idx: category.id,}


                          ))}
                          uniqueKey='idx'
                          single={true}
                          confirmText="Pilih"
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih PC"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPc.bind(this)}
                          selectedItems={this.state.g_dpwx_id}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Rayon saat ini<Text style={{color:'red'}}>* </Text></Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listRayon.map((category) => (
                           
                           {name: category.rayon, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Rayon"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedRayon.bind(this)}
                          selectedItems={this.state.g_dprax_id}
                        />
                    </View>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jabatan <Text style={{color:'red'}}>* </Text></Label>
           
                        
                      <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listJabatan.map((category) => (
                           
                           {name: category.position, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Jabatan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedJab.bind(this)}
                          selectedItems={this.state.g_jabatanx_id}
                        />
                     </View>

                       
                  </Form>
               </Content>
              </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Dokumen</Text>
              </TabHeading>}>

               <ScrollView>
                  <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>

                  

                <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Foto KTA <Text style={{color:'red'}}>* </Text></Label>
                        <Item regular>
                          <Input disabled
                          placeholder='Tampak Depan'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.ktaFront === null ? <Button onPress={this.selectPhotoKtaFront.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtaFront.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktaFront} />
                             </TouchableWithoutFeedback>
                            }
                      </Item>

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          placeholder='Tampak Belakang'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.ktaBack === null ? <Button onPress={this.selectPhotoKtaBack.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                              <TouchableWithoutFeedback onPress={this.selectPhotoKtaBack.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktaBack} />
                              </TouchableWithoutFeedback>
                            }
                          
                      </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Dokumen Anggota <Text style={{color:'red'}}>* </Text></Label>

                      <Item regular>
                          <Input disabled
                          placeholder='Foto Anggota *'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.foto === null ? <Button onPress={this.selectPhotoFoto.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                          <TouchableWithoutFeedback onPress={this.selectPhotoFoto.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.foto} />
                          </TouchableWithoutFeedback>
                            }
                      </Item>

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          placeholder='KTP / Kartu Pelajar *'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.ktp === null ? <Button onPress={this.selectPhotoKtp.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                          <TouchableWithoutFeedback onPress={this.selectPhotoKtp.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktp} />
                           </TouchableWithoutFeedback>
                            }
                          
                      </Item>

                      
          
             <View>
                 <TouchableWithoutFeedback onPress={() => this.getSubmit()}>
                  <View style={[CommonStyles.buttonFieldYellow2x, styles.buttonBox, {marginBottom: spaceHeight * 0.1}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      AJUKAN
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            
          
              

                  </Content>
               </ScrollView>
            
          </Tab>
        </Tabs>
      </Container>

       </View>

      

        
      </View>
      <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Sukses
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Terima Kasih sudah mendaftar, data anda akan di verifikasi terlebih dahulu dan akan menerima email lanjutan dari kami

          </Text>

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
           
            <TouchableOpacity onPress={() => this.getMain()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>OK</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </ImageBackground>
    )
  }

//

_goToMainScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }


  _handleClick(
      // firstName, 
      // lastName, 
      // avatar,
      title1,
      title2,
      namaDepan,
      namaBelakang,
      email,
      alias,
      nik,
      birthplace,
      birthday,
      sex,
      religions_id,
      degree,
      occupations,
      maritalstatus,
      spousename,
      mothername,
      h_province_id,
      h_cities_id,
      h_districts_id,
      h_villages_id,
      h_address_1,
      h_address_2,
      h_rtrw,
      h_postcodes_id,
      h_phone,
      mobile,
      o_province_id,
      o_cities_id,
      o_districts_id,
      o_villages_id,
      o_address_1,
      o_address_2,
      o_rtrw,
      o_postcodes_id,
      o_phone,
      p_nrp,
      p_name,
      p_military_status_id,
      p_military_uo_id,
      p_military_ranks_id,
      p_military_position_id,
      p_childrens,
      p_religions_id,
      p_kinship_id,
      p_info,
      g_members_memberstatus_id,
      g_registered_at,
      g_dpd_id,
      g_dpw_id,
      g_dpra_id,
      g_jabatan_id,
      g_jabatan_start,
      g_jabatan_end,
      ktaFront,
      ktaBack,
      foto,
      ktp,
      skep_pensiun,
      skep_pensiun_back,
      kta_tnipolri,
      kta_tnipolri_back,
      dok_tambahan,
      aktalahir,
      kk
      ) {
    this.props.navigator.push({
      screen: "Fkppi.KonfirmasiMemberScreen",
      animated: true,
      passProps: {
       // firstName: firstName,
       // lastName: lastName,
       // avatar: avatar,
       title1:title1,
       title2:title2,
       namaDepan: namaDepan,
       namaBelakang: namaBelakang,
       email: email,
       alias:alias,
       nik:nik,
       birthplace: birthplace,
       birthday:birthday,
       sex:sex,
       religions_id:religions_id,
       degree:degree,
       occupations:occupations,
       maritalstatus:maritalstatus,
       spousename:spousename,
       mothername:mothername,
       h_province_id: h_province_id,
       h_cities_id: h_cities_id,
       h_districts_id: h_districts_id,
       h_villages_id:h_villages_id,
       h_address_1: h_address_1,
       h_address_2: h_address_2,
       h_rtrw: h_rtrw,
       h_postcodes_id: h_postcodes_id,
       h_phone: h_phone,
       mobile:mobile,
       o_province_id: o_province_id,
       o_cities_id: o_cities_id,
       o_districts_id: o_districts_id,
       o_villages_id:o_villages_id,
       o_address_1: o_address_1,
       o_address_2: o_address_2,
       o_rtrw: o_rtrw,
       o_postcodes_id: o_postcodes_id,
       o_phone: o_phone,
       p_nrp:p_nrp,
       p_name:p_name,
       p_military_status_id: p_military_status_id,
       p_military_uo_id: p_military_uo_id,
       p_military_ranks: p_military_ranks_id,
       p_military_position:p_military_position_id,
       p_childrens: p_childrens,
       p_religions_id: p_religions_id,
       p_kinship_id: p_kinship_id,
       p_info:p_info,
       g_members_memberstatus_id:g_members_memberstatus_id,
       g_registered_at: g_registered_at,
        g_dpd_id:g_dpd_id,
      g_dpw_id:g_dpw_id,
      g_dpra_id:g_dpra_id,
      g_jabatan_id:g_jabatan_id,
      g_jabatan_start:g_jabatan_start,
      g_jabatan_end:g_jabatan_end,
      ktaFront:ktaFront,
      ktaBack:ktaBack,
      foto:foto,
      ktp:ktp,
      skep_pensiun:skep_pensiun,
      skep_pensiun_back:skep_pensiun_back,
      kta_tnipolri:kta_tnipolri,
      kta_tnipolri_back:kta_tnipolri_back,
      dok_tambahan:dok_tambahan,
      aktalahir:aktalahir,
      kk:kk
      },
    });
  }


}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  bg:{
    backgroundColor:'#404f3d',
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
 titleBox: {
   justifyContent: 'flex-start'
  },
});
