import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
   Dimensions,
  TouchableWithoutFeedback,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ConnectivityRenderer } from 'react-native-offline';
import GridView from 'react-native-super-grid';
import {DevApi, MainApi} from './fkppi/Api';
import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import Net from './fkppi/Net' ;
import HTML from 'react-native-render-html';

import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemPesan from '../components/ItemPesan';

export default class FaqDetailScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);

    this. state = {
      data: {},
      myToken: '',
      loading: true,
      animating: true
    }
    
   
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);
        var that = this;
       that.getKey();
    }

  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.faqId
      var url =  DevApi + 'faq/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            body: result.body,
            loading: false
         });

          console.log(that.state.body)

        })
        .catch((error) => { console.error(error); });
  }


  renderBody(){
      
    if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:50}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }else{


      return(
    
        <View style={CommonStyles.wrapperBoxNews}>
           <Text style={{fontFamily: 'Roboto-Bold', color:'#ffbf20', fontSize:18, lineHeight:24, marginTop:10, marginBottom:10}}>{this.props.faqName}</Text>
          
            <View style={{height:1, backgroundColor:'#e2e2e1', marginTop:15, marginBottom:5}}></View>
             <HTML style={{color:'#2d2a26', fontSize: 15, lineHeight:28, fontFamily: 'Roboto-Light'}} html={this.state.body} imagesMaxWidth={Dimensions.get('window').width} />
          

          
           <View >
        
        
                  <TouchableOpacity onPress={() => this._goToBack()} >
                  <View style={[CommonStyles.buttonFieldYellow2, styles.buttonBox, {marginBottom: spaceHeight * 0.1}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      Mengerti
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
          </View>


      )
    }


  }
///

renderMain(){

     return(

      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <View style={{flex:1,justifyContent: 'center'}}>{this.renderBody()}</View>
        ) : (
          <Net />
          
        )
      )}
    </ConnectivityRenderer>


     )

  }

   
  render() {

  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>FAQ</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
         <ScrollView style={CommonStyles.noTabScroll}>
          {this.renderMain()}
        </ScrollView>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//
_goToBack() {
    this.props.navigator.pop({
      animated: true, // does the pop have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
  }

  
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
