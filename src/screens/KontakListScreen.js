import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
  AsyncStorage,
  ScrollView,
   ToastAndroid,
   ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';

import GridView from 'react-native-super-grid';

import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import Kosong from './fkppi/Kosong';

import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemKontak from '../components/ItemKontak';

export default class KontakListScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: [],
      myToken: '',
      loading: true,
      animating: true
    }
   
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);

       this.getKey();
    }





  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getAnggota(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  ////////

  getAnggota(value){
   
    const FETCH_TIMEOUT = 5000;
    let didTimeOut = false;
    
      this.setState({
          loading: true
      });

     var url =  DevApi + 'search/member';
     var that = this;
   
   new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 

              },
              body: JSON.stringify({

                'name' : that.props.anggota,
                'idfkppi' : that.props.no_anggota,
                'position_id' : that.props.jabatan_id,
                'pd_id' : that.props.pd_id,
                'pc_id' : that.props.pc_id,
                'rayon_id' : that.props.rayon_id,


              }),
        })

        // .then((response) => {

        //    clearTimeout(timeout);

        //    if(!didTimeOut) {
                  
        //         if (response.status == '403') {

        //             ToastAndroid.show('Data Kosong', ToastAndroid.SHORT)
        //             that.setState({
        //             loading: false,
        //           });
        //         }
        //         if (response.status == '200') {
                   
        //             console.log(response)
        //             //that.saveKey(response.headers.map.authorization[0]);
        //             that.setState({
        //             loading: false,
        //           });

        //         }


        //    }


       // });
     .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                                 total: result.length,
                              loading: false

                        });

                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });



  }
  

  renderList(){

   if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
    if (this.state.data == '') {
     
     return(

        <Kosong />

     )

    }
    else{

      return(

          <ScrollView style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.data.map((item, index) => (
                <ItemKontak
                  key={item.id}
                  itemName={item.firstname}
                  itemNameLast={item.lastname}
                  itemJabatan={item.position}
                  itemImg={item.avatar}
                  itemTelepon={item.telepon}
                  itemHp={item.mobile}
                  itemStatus={item.position}
                  itemOrganisasi="FKPPI"
                  itemDpd={item.pd}
                  itemDpw={item.pc}
                  itemRayon={item.rayon}
                  itemHeaderText={item.title}
                  
                />
              ))
            }
          </View>
        </ScrollView>


        )

    }
    


  }




  render() {

  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Kontak</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.whiteColor]}>
                Hasil Pencarian : {this.state.total}
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
         {this.renderList()}

       </View>

        
      </View>
      </ImageBackground>
    )
  }

////
  _handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }

  
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
