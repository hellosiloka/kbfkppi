import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
  Alert,
  StatusBar,
  Linking,
  ImageBackground,
  RefreshControl,
  ScrollView,
  AsyncStorage,
  BackHandler,
  TouchableOpacity,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import VersionNumber from 'react-native-version-number';
import Icon from 'react-native-vector-icons/FontAwesome';
import GridView from 'react-native-super-grid';
import Slider from './Slider';
import Menu from './MainMenu';
import { inject, observer } from 'mobx-react/native';
import { ConnectivityRenderer } from 'react-native-offline';
import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import Modal from "react-native-simple-modal";
import MenuItemBox from '../components/MenuItemBox';
import Net from './fkppi/Net' ;
//import CustomTabBar from '../components/CommonTabBar';
import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import GradientNavigationBar from '../elements/GradientNavigationBar';
import {DevApi, MainApi} from './fkppi/Api';
import { appSingleNavigation } from '../styles/navigatorStyle';

export default class MainServiceScreen extends Component {
     static navigatorStyle = appSingleNavigation;

     
  constructor(props) {
    super(props);
    this.state = {
        myKey: null,
        refreshing: false,
        loading:true,
        animating: true,
        
    }

    this._handleClickKomunitas = this._handleClickKomunitas.bind(this)
     this._handleClickMabes = this._handleClickMabes.bind(this)
     this._handleClickPesan = this._handleClickPesan.bind(this)
     this._handleClickKontakList = this._handleClickKontakList.bind(this)
     this._handleClickNews = this._handleClickNews.bind(this)
     this._handleClickDaftar = this._handleClickDaftar.bind(this)
  }
  

   _onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  }


//setInterval(function(){ alert("Hello"); }, 3000);


 


  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       that.getKey();
       //that.getVersion();
         this.closeActivityIndicator()

    }


  
   closeActivityIndicator = () => setTimeout(() => this.setState({ 
      animating: false }), 1500)




  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );
      this.getRead(value );      
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      //var ID = this.props.newsId

      var url =  DevApi + 'user/profile' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            no_anggota:result.idfkppi,
            member_id: result.member_id,
            loading: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }
  /////



  getRead(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      //var ID = this.props.newsId

      var url =  DevApi + 'message/checknew' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            unRead:result,
            loading: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }




// renderActive(){


  state = { open: false, buka:false };

  modalDidOpen = () => console.log("Modal did open.");

   modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

   openModal = () => this.setState({ open: true });

    closeModal = () => this.setState({ open: false });




// }



renderMenu(){

   const linkDaftar =  this._handleClickDaftar.bind(this) ;
    const linkPesan =  this._handleClickPesan.bind(this) ;
    const linkMabes =  this._handleClickMabes.bind(this) ;
    const linkNews =  this._handleClickNews.bind(this) ;
    const linkKontak =  this._handleClickKontak.bind(this) ;
    const linkKomunitas =  this._handleClickKomunitas.bind(this) ;

    const linkKosong = this.openModal;

    const items = [
      { name: 'Daftar', width: 96, height:75, unRead:0, link: linkDaftar, url: require('../../img/fkppi/ic_menu_daftarx.png') },
      { name: 'Pesan', width: 43, height:75, unRead:this.state.unRead,link: linkPesan, url:require('../../img/fkppi/ic_menu_pesan.png')  },
      { name: 'KB FKPPI', width: 78, height:57, unRead:0,link: linkMabes, url:require('../../img/fkppi/ic_menu_mabes.png')  },
      { name: 'Berita', width: 59, height:60, unRead:0, link: linkNews, url:require('../../img/fkppi/ic_menu_berita.png')  },
      { name: 'Kontak', width: 63, height:63, unRead:0, link: linkKontak, url:require('../../img/fkppi/ic_menu_kontak.png')  },
      { name: 'Komunitas', width: 59, height:66, unRead:0, link: linkKomunitas, url:require('../../img/fkppi/ic_menu_komunitas.png')  },

    ];


     const items_grey = [
      { name: 'Daftar', width: 96, height:75,unRead:0,link: linkDaftar, url:require('../../img/fkppi/ic_menu_daftarx.png') },
      { name: 'Pesan', width: 43, height:75, unRead:0,link: linkKosong , url:require('../../img/fkppi/ic_menu_pesan_grey.png') },
      { name: 'KB FKPPI', width: 78, height:57, unRead:0,link: linkKosong , url:require('../../img/fkppi/ic_menu_mabes_grey.png') },
      { name: 'Berita', width: 59, height:60, unRead:0,link: linkKosong , url:require('../../img/fkppi/ic_menu_berita_grey.png')},
      { name: 'Kontak', width: 63, height:63, unRead:0,link: linkKosong , url:require('../../img/fkppi/ic_menu_kontak_grey.png') },
      { name: 'Komunitas', width: 59, height:66, unRead:0,link: linkKosong , url:require('../../img/fkppi/ic_menu_komunitas_grey.png') },

    ];



    const animating = this.state.animating



     if (this.state.loading){

       return(
         <View style={{flex:1}}>
           <ActivityIndicator
               animating = {animating}
               color="#404f3d"
               size = "large"
               style = {styles.activityIndicator}/>
         </View>

         )

     }else{
        
      if(this.state.no_anggota == null){


        return(

          <GridView
        itemDimension={140}
        items={items_grey}
        style={styles.gridView}
        renderItem={item => (
          <MenuItemBox
              boxTitle={item.name}
              boxIcon={item.url}
              boxIconWidth={item.width} 
              boxIconHeight={item.height}
              unRead={item.unRead} 
              onPressBoxItem={item.link}
            >
           
          </MenuItemBox>

        )}
      />


        )


      }else{
  
       return(


 <GridView
        itemDimension={140}
        items={items}
        style={styles.gridView}
        renderItem={item => (
          <MenuItemBox
              boxTitle={item.name}
              boxIcon={item.url}
              boxIconWidth={item.width}
              unRead={item.unRead} 
              boxIconHeight={item.height}
              onPressBoxItem={item.link}
            >
           
          </MenuItemBox>

        )}
      />


  )

      }

}

}


renderMain(){



   const titleImgStyle = {
      width: 35,
      height: 50,
    };

    const navbarStyle = {
      height: 80,
    };



    

  return(

     <View style={CommonStyles.normalPage}>

       <StatusBar
         backgroundColor="#857d60"
         barStyle="light-content"
         hidden={true}

       />
      <GradientNavigationBar
          navigator={this.props.navigator}
          screenTitle='image'
          titleImg={require('../../img/fkppi/logo2.png')}
          titleImgStyle={titleImgStyle}
          isBack={false} 
          rightButtons={
            [
            
              {
                key: 2,
                buttonIcon: require('../../img/fkppi/ic_faq.png'),
                buttonAction: this._handleClickFaq.bind(this),
                buttonWidth: 20,
                buttonHeight: 20,
              }
            ]
          }
        />
        
        <View style={[{flex:1, marginTop: deviceHeight / 13.5}, styles.elementsContainer]}>

         <View style={{flex: 1}}>

             <View>
               <Slider />

             </View>
            
         </View>
      </View>
    
       <View style={{flex: 4, backgroundColor: '#fbfbfb',justifyContent: 'center', flexDirection: 'row', }}>
    <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        >
      
      {this.renderMenu()}
</ScrollView>
       </View>

        <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 6,
            padding: 0,
            height: 150,
            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
        <View style={{height:50, backgroundColor:'#404f3d', borderTopLeftRadius:6, borderTopRightRadius:6, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color:'#fff', fontSize:18}}>Informasi :</Text>
        </View>

        <View style={{marginLeft:50, marginRight:50}}> 
         <Text style={{textAlign: 'center',marginTop:15, fontSize: 15, color:'#2d2a26'}}>
         Silahkan melakukan pendaftaran terlebih dahulu, agar menu ini tersedia !
        </Text>
        </View>

        
       
        </Modal>
      </View>


    )


}



   
  render() {


    /////

   
    
    return (

   
      
      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <View style={{flex:1,justifyContent: 'center'}}>{this.renderMain()}</View>
        ) : (
           <View style={{flex:1,justifyContent: 'center'}}>{this.renderMain()}</View>
          
        )
      )}
    </ConnectivityRenderer>

    )
  }
//

 _handleClickFaq() {
    this.props.navigator.push({
      screen: "Fkppi.FaqScreen",
      animated: true,
       // animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

_handleClickKosong(){

}
  
_handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
      // animationType: 'slide-horizontal',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

   _handleClickKomunitas() {
    this.props.navigator.push({
      screen: "Fkppi.KomunitasScreen",
      animated: true,
       // animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  _handleClickMabes() {
    this.props.navigator.push({
      screen: "Fkppi.MabesScreen",
      animated: true,
       // animationType: 'slide-horizontal'
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  _handleClickKontakList() {
    this.props.navigator.push({
      screen: "Fkppi.KontakListScreen",
      animated: true,
       //animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

   _handleClickKontak() {
    this.props.navigator.push({
      screen: "Fkppi.KontakScreen",
      animated: true,
       //animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  _handleClickNews() {
    this.props.navigator.push({
      screen: "Fkppi.NewsScreen",
      animated: true,
       //animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

 

  _handleClickPesan() {
    this.props.navigator.push({
      screen: "Fkppi.PesanScreen",
      animated: true,
       //animationType: 'slide-horizontal',
       portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }


  
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1.7,
      backgroundColor:'#857d60',

    
    },
    logoBox:{
      alignSelf:'center',
      flexDirection: 'row',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10
  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
   activityIndicator: {
      alignItems: 'center',
      justifyContent:'center',
      marginTop:50,

   }
});
