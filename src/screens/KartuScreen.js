import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  AsyncStorage,
  Image,
  Alert,
  ActivityIndicator,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
    PermissionsAndroid,
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import RNFetchBlob from 'rn-fetch-blob'

import CommonStyles, {shadowOpt, spaceHeight, deviceWidth,} from '../styles/CommonStyles';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import GradientNavigationBar from '../elements/GradientNavigationBar';
// import SelectBox from '../elements/SelectBox';
import {DevApi, MainApi} from './fkppi/Api';

import { RaisedTextButton } from 'react-native-material-buttons';
import FitImage from 'react-native-fit-image';
 

export default class KartuScreen extends Component {
  static navigatorStyle = noNavTabbarNavigation;

  constructor(props) {
    super(props);
   this. state = {
    frontCard:'',
    backCard:'',
    printCard:'',
    loading: true,

   }
  }

  ////////////////////////
 componentDidMount() {
       //StatusBar.setHidden(true);
       var that = this;
      
       setTimeout(() => {
     that.getKey();
      this.setState({
        loading: false,
      });
    }, 1500);
        
    }

  //////////////////////


// async getKey() {
//     try {
//       const depan = await AsyncStorage.getItem('@CardDepan:key');
//       const back = await AsyncStorage.getItem('@CardBelakang:key');
//       this.setState({cardDepan: depan, cardBack: back});
//       //this.getDownload(value);    
//      //console.log("filw" + value)
//     } catch (error) {
//       console.log("Error retrieving data" + error);
//     }
//   }

  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.requestStoragePermission();
      this.getCard(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

async requestStoragePermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            'title': 'Storage Permission',
            'message': 'FKPPI App needs access to your Storage '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage")
        } else {
          console.log("Storage permission denied")
        }
      } catch (err) {
        console.warn(err)
      }
    }

//////////////////////////////////////


   getCard(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      
      const FETCH_TIMEOUT = 5000;
     let didTimeOut = false;

      //console.log(this.state.myToken)
      
      //var ID = this.props.newsId

      var url =  DevApi + 'member/profile' ;
      var that = this;
      // return fetch(url, {
      //         method: 'GET',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Authorization': value 
      //         }
      //   })
      //   .then(function(response) {
      //     return response.json();
      //   }).then(function(result) {
      //     that.setState({
      //       frontCard: result.cards.frontcard,
      //       backCard:  result.cards.backcard,
      //       printCard:  result.cards.printedcard,
      //       loading: false
      //    });

      //     console.log(result.cards)

      //   })
      //   .catch((error) => { console.error(error); });


     new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({
                  frontCard: result.cards.frontcard,
                  backCard:  result.cards.backcard,
                  printCard:  result.cards.printedcard,
                  loading: false
               });
                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });

  }


   onPress = () => {
    this.getKey2();
  }


async getKey2() {
    try {
      //const value = await AsyncStorage.getItem('@Card:key');
      //const back = await AsyncStorage.getItem('@CardBelakang:key');
      //this.setState({cardDepan: depan, cardBack: back});
      this.getDownload();    
     //console.log("filw" + value)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 
  getDownload(){

    
// RNFetchBlob
//   .config({
//     fileCache : true,
//     // by adding this option, the temp files will have a file extension
//     appendExt : 'png'
//   })
//   .fetch('GET', 'https://res.cloudinary.com/catcha/image/upload/v1535528819/kartu.png', {
//     //some headers ..
//   })
//   .then((res) => {
//     // the temp file path with file extension `png`
//     console.log('The file saved to ', res.path())
//     ToastAndroid.show('Download Sukses', ToastAndroid.SHORT)
//     // Beware that when using a file path as Image source on Android,
//     // you must prepend "file://"" before the file path
//     imageView = <Image source={{ uri : Platform.OS === 'android' ? 'file://' + res.path() : '' + res.path() }}/>
//   })

   var date      = new Date();
    var url       = this.state.printCard;
    var ext       = this.extention(url);
    ext = "."+ext[0];
    const { config, fs } = RNFetchBlob
    let PictureDir = fs.dirs.PictureDir
    let options = {
      fileCache: true,
      addAndroidDownloads : {
        useDownloadManager : true,
        notification : true,
        path:  PictureDir + "/image_"+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
        description : 'Image'
      }
    }
    config(options).fetch('GET', url).then((res) => {
      Alert.alert("Kartu Anggota Sukses didownload");
    });
  }
extention(filename){
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;


//////////end
  }

///////////////


renderID(){

  if (this.state.frontCard && this.state.backCard == null) {


    return(

      <View style={{flex: 1, flexDirection:'column', marginTop:35, marginLeft:0, marginRight:0}}>

         
        <Text>Kosong</Text>


          </View>

      )


  }else{

     return(

       <View style={{flex: 1, flexDirection:'column', marginTop:35, marginLeft:0, marginRight:0}}>

         
       <Image 

          source={{
            uri: this.state.frontCard,
            method: 'GET',
          headers: {
            Pragma: 'no-cache',
          },


           }} 
          
          style={{width: deviceWidth, height: deviceWidth/1.6}} />
       <Image source={{
            uri: this.state.backCard,
            method: 'GET',
          headers: {
            Pragma: 'no-cache',
          },


           }}  style={{width: deviceWidth, height: deviceWidth/1.6 , marginTop:2}} />

          


          </View>


      )

  }



}

renderCard(){

  if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }else{

      return(

       <View>{this.renderID()}</View>
      )

    }

}

  render() {
     
    return (
      <View style={CommonStyles.normalPage}>
       <GradientNavigationBar
          navigator={this.props.navigator}
          titleText='Kartu Anggota'
          rightButtons={
            [
              {
                key: 1,
                buttonIcon: require('../../img/fkppi/download_2.png'),
                buttonAction: this.onPress,
                buttonWidth: 22,
                buttonHeight: 20,
              }
            ]
          } 
        />

        <View style={styles.container}>
        
        <View style={[{flex:1}, styles.elementsContainer]}>
          {this.renderCard()}
         
        </View>
      </View>
        
      </View>
    );
  }



   _handleClickDownload() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }


}

const styles = StyleSheet.create({
  
  elementsContainer: {
    flex: 1,
    margin:0

  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    margin: 0,
    height: 50,
    marginTop: 24,
  }
});
