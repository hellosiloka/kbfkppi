import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   Dimensions,
  ImageBackground,
  ScrollView,
   ActivityIndicator,
  AsyncStorage,
  TouchableHighlight,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import GridView from 'react-native-super-grid';

import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';
import {DevApi, MainApi} from './fkppi/Api';
import ItemPesan from '../components/ItemPesan';
import HTML from 'react-native-render-html';
import moment from 'moment';
import 'moment/locale/id';
moment.locale('id');

const htmlContent = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
`;

export default class PesanDetailScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: {},
      myToken: '',
      loading: true,
      animating: true
    }
    
   
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);
       this.getKey();
    }
   
async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

//////


///////////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.pesanId
      var url =  DevApi + 'message/inbox/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            body: result.body,
            loading: false
         });

          console.log(that.state.body)

        })
        .catch((error) => { console.error(error); });
  }

  //////



renderBody(){

     if (this.state.loading) {

        return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)

     }else{
   

       return(

          <View>

            
   

           <HTML style={{color:'#2d2a26', fontSize: 15, lineHeight:28, fontFamily: 'Roboto-Light'}} html={this.state.body} imagesMaxWidth={Dimensions.get('window').width} />



          </View>


       )


     }


  }



  ////

  render() {

     const todayDate = moment(this.props.tanggal).format('LL');
     const today = moment(this.props.tanggal).format('dddd'); 
  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Pesan</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
         <ScrollView style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBoxNews}>
           <Text style={{fontFamily: 'Roboto-Bold', color:'#000', fontSize:13}}>Dari : {this.props.from} </Text>
            <Text style={{fontFamily: 'Roboto-regular', color:'#adadad', fontSize:11 , lineHeight: 20}}>{today}, {todayDate}</Text>
            <View style={{height:1, backgroundColor:'#e2e2e1', marginTop:15, marginBottom:5}}></View>
            <Text style={{fontFamily: 'Roboto-Bold', color:'#ffbf20', fontSize:18, lineHeight:24, marginTop:10, marginBottom:10}}>{this.props.title}</Text>
           
            {this.renderBody()}
          </View>
        </ScrollView>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//
  
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
