import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  Image,
  PixelRatio,
  Alert,
  TouchableOpacity,
  ToastAndroid,
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
//import { RNS3 } from 'react-native-aws3';
// import GradientButton from '../elements/GradientButton';
import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import NoNavigationBar from '../elements/NoNavigationBar2';
import GradientNavigationBar from '../elements/GradientNavigationBar';
// import SelectBox from '../elements/SelectBox';
import Loader from './fkppi/Loader';
import { TextField } from 'react-native-material-textfield';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { RaisedTextButton } from 'react-native-material-buttons';
import Modal from "react-native-simple-modal";
import {DevApi, MainApi} from './fkppi/Api';

import ImagePicker from 'react-native-image-picker';  

export default class SignUpScreen extends Component {
  static navigatorStyle = noNavTabbarNavigation;

  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
      this.onChangeText = this.onChangeText.bind(this);
      this.onSubmitFirstName = this.onSubmitFirstName.bind(this);
      this.onSubmitLastName = this.onSubmitLastName.bind(this);
      this.onSubmitAbout = this.onSubmitAbout.bind(this);
      this.onSubmitUsername = this.onSubmitUsername.bind(this);
      this.onSubmitEmail = this.onSubmitEmail.bind(this);
      this.onSubmitPassword = this.onSubmitPassword.bind(this);
      this.onSubmitPassword2 = this.onSubmitPassword2.bind(this);
      this.onAccessoryPress = this.onAccessoryPress.bind(this);

      this.firstnameRef = this.updateRef.bind(this, 'firstname');
      this.lastnameRef = this.updateRef.bind(this, 'lastname');
      this.aboutRef = this.updateRef.bind(this, 'about');
      this.usernameRef = this.updateRef.bind(this, 'username');
      this.emailRef = this.updateRef.bind(this, 'email');
      this.passwordRef = this.updateRef.bind(this, 'password');
      this.password2Ref = this.updateRef.bind(this, 'password2');

      this.onSignUpPress = this.onSignUpPress.bind(this);

      this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);
      this.state = {
          firstname: this.props.firstname,
          username:'',
          lastname: this.props.lastname,
          email:this.props.email,
          about: '',
          avatar:'',
          avatarx:'',
          secureTextEntry: true,
          avatarSource: null,
          videoSource: null,
           loading: false,
        };
  }

  state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });



   async getRegister() {
    
    this.setState({
          loading: true
        });
        this.onSignUpPress()
        //console.log('coords', coords)
      // this.onSignUpPress();
        // setTimeout(() => {
        //  //this.onSignUpPress();
        //   this.setState({
        //     loading: false,
        //   });
        // }, 2500);
      }

 

  ////////////////////////

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 300,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    
        console.log(file);

        // const options = {
        //   keyPrefix: "avatar/",
        //   bucket: "catcha-dev-fkppi",
        //   region: "ap-southeast-1",
        //   accessKey: "AKIAIB7TYNPAVHGBBIRQ",
        //   secretKey: "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",
        //   successActionStatus: 201
        // }
        
        // RNS3.put(file, options).then(response => {
        //   if (response.status !== 201)
        //     throw new Error("Failed to upload image to S3");
        //   console.log(response.body);

        //   ToastAndroid.show('Upload Sukses', ToastAndroid.SHORT)
        //   this.setState({
        //     avatarx: response.body.postResponse.location,
        //   });

        //   console.log(this.state.avatarx);
        //   /**
        //    * {
        //    *   postResponse: {
        //    *     bucket: "your-bucket",
        //    *     etag : "9f620878e06d28774406017480a59fd4",
        //    *     key: "uploads/image.png",
        //    *     location: "https://your-bucket.s3.amazonaws.com/uploads%2Fimage.png"
        //    *   }
        //    * }
        //    */
        // });

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatar: file,
          
        });
      }
    });
  }

  //////////////////////

  onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this[name];

        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
    }

    onChangeText(text) {
      ['firstname', 'lastname', 'username', 'about', 'email', 'password', 'password2']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    }

    onAccessoryPress() {
      this.setState(({ secureTextEntry }) => ({ secureTextEntry: !secureTextEntry }));
    }

    onSubmitFirstName() {
      this.lastname.focus();
    }

    onSubmitLastName() {
      this.about.focus();
    }

    onSubmitAbout() {
      this.email.focus();
    }

    onSubmitUsername() {
      this.username.focus();
    }

    onSubmitEmail() {
      this.password.focus();
    }

    onSubmitPassword() {
      this.password.blur();
    }

     onSubmitPassword2() {
      this.password2.blur();
    }

    onSubmit() {
      let errors = {};

      ['firstname', 'lastname', 'username' ,'email', 'password', 'password2']
        .forEach((name) => {
          let value = this[name].value();

          if (!value) {
            errors[name] = 'Should not be empty';
          } else {
            if ('password' === name && value.length < 6) {
              errors[name] = 'Too short';
            }

          }
        });

      this.setState({ errors });
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    renderPasswordAccessory() {
      let { secureTextEntry } = this.state;

      let name = secureTextEntry?
        'visibility':
        'visibility-off';

      return (
        <MaterialIcon
          size={24}
          name={name}
          color={TextField.defaultProps.baseColor}
          onPress={this.onAccessoryPress}
          suppressHighlighting
        />
      );
    }

    ///////////////

     onSignUpPress() {
        
  

        if(this.state.username === ''){

           ToastAndroid.show('Username tidak boleh kosong', ToastAndroid.SHORT);
           this.setState({
                          loading: false

                        });
           //this.openModal();
         }
         else if(this.state.email === ''){
           ToastAndroid.show('Email tidak boleh kosong', ToastAndroid.SHORT);
            this.setState({
                          loading: false

                        });
         }
         else if(this.state.firstname === ''){
           ToastAndroid.show('Firstname tidak boleh kosong', ToastAndroid.SHORT);
            this.setState({
                          loading: false

                        });
         }


        else if(this.state.avatar === ''){
        
          var url =  DevApi + 'user/register';

          var input = this.state.username;
          //remove space
          var username = input.replace(/\s/g, '');

          //make string lower
          var newusername = username.toLowerCase();


          var formData = new FormData();
       var _this = this;
       formData.append('firstname', this.state.firstname);
       formData.append('lastname', this.state.lastname);
       formData.append('username', newusername);
       formData.append('email', this.state.email);
       
      console.log(formData)

      var xhr = new XMLHttpRequest();
      xhr.open('POST', url);
      xhr.responseType = 'json';

      //xhr.timeout = 2000; // time in milliseconds

      console.log('OPENED', xhr.status);
      xhr.onprogress = function () {
        console.log('LOADING', xhr.status);
       
        
    };

    // xhr.ontimeout = function(e) {
    //   // XMLHttpRequest timed out. Do something here.
    //     ToastAndroid.show('Berhasil Register', ToastAndroid.SHORT)
    //     _this.openModal();
    //      _this.setState({
    //       loading: false,
    //     });
    // };
  
    xhr.onload = function(e) {
      if (this.status == 200) {
         //ToastAndroid.show('Berhasil Register', ToastAndroid.SHORT)
         _this.openModal();
         _this.setState({
          loading: false,
        });
      }else{

        ToastAndroid.show('Username / Email sudah terdaftar', ToastAndroid.SHORT)
        _this.setState({
          loading: false,
        });
      }
    };

     xhr.setRequestHeader('Content-Type', 'multipart/form-data');
     
     xhr.setRequestHeader('Accept', 'application/json');
     
     xhr.send(formData);


        }
        
        else{

        
       var url =  DevApi + 'user/register';

         var input = this.state.username;
          //remove space
          var username = input.replace(/\s/g, '');

          //make string lower
          var newusername = username.toLowerCase();

       var formData = new FormData();
       var _this = this;
       formData.append('firstname', this.state.firstname);
       formData.append('lastname', this.state.lastname);
       formData.append('username', newusername);
       formData.append('email', this.state.email);
       formData.append('avatar', this.state.avatar);
       
      console.log(formData)

      var xhr = new XMLHttpRequest();
      xhr.open('POST', url);
      xhr.responseType = 'json';

      //xhr.timeout = 2000; // time in milliseconds

      console.log('OPENED', xhr.status);
      xhr.onprogress = function () {
        console.log('LOADING', xhr.status);
       
        
    };

    // xhr.ontimeout = function(e) {
    //   // XMLHttpRequest timed out. Do something here.
    //     ToastAndroid.show('Berhasil Register', ToastAndroid.SHORT)
    //     _this.openModal();
    //      _this.setState({
    //       loading: false,
    //     });
    // };
  
    xhr.onload = function(e) {
      if (this.status == 200) {
         //ToastAndroid.show('Berhasil Register', ToastAndroid.SHORT)
         _this.openModal();
         _this.setState({
          loading: false,
        });
      }else{

        ToastAndroid.show('Username / Email sudah terdaftar', ToastAndroid.SHORT)
         _this.setState({
          loading: false,
        });
      }
    };

     xhr.setRequestHeader('Content-Type', 'multipart/form-data');
     
     xhr.setRequestHeader('Accept', 'application/json');
     
     xhr.send(formData);
     //////////////////////////////////////

      // var request = new XMLHttpRequest();
      // request.onreadystatechange = (e) => {
      //   if (request.readyState !== 4) {

      //     return;
      //   }

      //   if (request.status === 200) {
      //      Alert.alert("Berhasil Register");
      //      _this.setState({
      //       loading: false
      //     });
      //     console.log('success', request.responseText);
      //   } else {
      //     console.warn('error');
      //     _this.setState({
      //       loading: false
      //     });
      //   }
      // };

      // request.setRequestHeader('Content-Type', 'multipart/form-data');
     
      // request.setRequestHeader('Accept', 'application/json');

      // request.open('POST', url);
      // request.send(formData);

    }

     }


     /////////////

  render() {
      let { errors = {}, secureTextEntry, ...data } = this.state;
      let { firstname = 'name', lastname = 'house', username= '123' } = data;

      let defaultEmail = `${firstname}@${lastname}.com`
        .replace(/\s+/g, '_')
        .toLowerCase();
    return (
      <View style={CommonStyles.normalPage}>

    <NoNavigationBar
          navigator={this.props.navigator}
          titleText='Sign Up'
           isBack={true} 
        />
        <Loader
          loading={this.state.loading} />
        <ScrollView 
            style={CommonStyles.scrollView}
            keyboardShouldPersistTaps='handled'
            >
     
           

            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
              <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
              { this.state.avatar === '' ? <Image
              source={require('../../img/fkppi/photo.png')}
              style={{width: 76, height: 78}}
            /> :
                <Image style={styles.avatar} source={this.state.avatar} />
              }
          </View>
        </TouchableOpacity>
       
          <View style={[{flex:1}, styles.elementsContainer]}>
            <View style={{flex: 2}}>
                <TextField
                  ref={this.firstnameRef}
                  value={this.state.firstname}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}

                  tintColor='rgb(255, 191, 32)'
       
                   onChangeText={(firstname) => this.setState({firstname})}
                  returnKeyType='next'
                  label='First Name'
                  error={errors.firstname}
                  returnKeyType="next"
                />

                <TextField
                  ref={this.lastnameRef}
                  value={this.state.lastname}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  tintColor='rgb(255, 191, 32)'
        
                  onChangeText={(lastname) => this.setState({lastname})}
                  returnKeyType='next'
                  label='Last Name'
                  error={errors.lastname}
                />

                <TextField
                  ref={this.usernameRef}
                  value={this.state.username}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  tintColor='rgb(255, 191, 32)'
         
                  onChangeText={(username) => this.setState({username})}
                  returnKeyType='next'
                  label='Username'
                  error={errors.username}
                />
                <TextField
                  ref={this.emailRef}
                  value={this.state.email}
                  defaultValue={this.state.email}
                  keyboardType='email-address'
                  autoCapitalize='none'
                  autoCorrect={false}
                   tintColor='rgb(255, 191, 32)'
                  enablesReturnKeyAutomatically={true}
      
      
                  onChangeText={(email) => this.setState({email})}
                  returnKeyType='next'
                  label='Email Address'
                  error={errors.email}
                />
               

            </View>

             <View style={styles.container}>
              <RaisedTextButton style={{height: 50}} onPress={() => this.getRegister()} title='Sign Up' color='#ffbf20' titleColor='white' />
            </View>
            
          
        </View>
        </ScrollView>
        <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Berhasil Terdaftar
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Password dikirimkan ke email anda !
          </Text>

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={this._goToSignInScreen.bind(this)}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>OK</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </View>
    );
  }

  _goToSignInScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.SignInScreen",
      animated: true,
      animationType: 'fade',
      portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
          },
    });
  }


}

const styles = StyleSheet.create({
  formBox: {
    flex: 1,
    height: 155,
    backgroundColor: '#000000',
    alignItems: 'center', 
  },
  avatarBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 15,
     borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    borderRadius: 75,
    width: 150,
    height: 150

  },
  nameBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  elementsContainer: {
    flex: 1,
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 24
  },
  container: {
    margin: 0,
    height: 50,
    marginTop: 24,
  },
  avatarContainer: {
    borderColor: '#dce1e4',
    borderWidth: 3 / PixelRatio.get(),
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleBox: {
   justifyContent: 'flex-start'
  },
});
