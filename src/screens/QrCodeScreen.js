import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  Image,
  PixelRatio,
  Linking,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  PermissionsAndroid,
  ToastAndroid,
  Platform,
} from 'react-native';

import {
  CameraKitCamera, CameraKitCameraScreen
} from 'react-native-camera-kit';
// import GradientButton from '../elements/GradientButton';
import CommonStyles, {shadowOpt, spaceHeight, deviceWidth,} from '../styles/CommonStyles';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import GradientNavigationBar from '../elements/GradientNavigationBar';
import Modal from "react-native-simple-modal";

import {DevApi, MainApi} from './fkppi/Api';
import { RaisedTextButton } from 'react-native-material-buttons';
import {toJS } from 'mobx';
import CheckingScreen from './CheckingScreen';
import Loader from './fkppi/Loader';
export default class QrCodeScreen extends Component {
  static navigatorStyle = noNavTabbarNavigation;

  constructor(props) {
    super(props);
    this.state = {
      example: undefined,
        loading: false,
        qr: true,
        frame: true,
    };
  
  }

  componentDidMount() {
       //StatusBar.setHidden(true);
        var that = this;
        that.requestCameraPermission();
        AsyncStorage.removeItem('@QrID:key');
    }

  //

  async requestCameraPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            'title': 'Camera Permission',
            'message': 'FKPPI App needs access to your Camera '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera")
        } else {
          console.log("Camera permission denied")
        }
      } catch (err) {
        console.warn(err)
      }
    }


  ////////////////////////


   async getMain() {
    this.setState({
      loading: true
    });

    this.closeModal();

    setTimeout(() => {
     this._goToMainScreen();

      this.setState({
        loading: false,
      });
    }, 1000);
  }

  onSuccess(e) {
   
this.setState({
      loading: true,
      numberID: e.nativeEvent.codeStringValue
    });


   const ID = e.nativeEvent.codeStringValue;
    AsyncStorage.setItem('@QrID:key', ID,  (err, result) => {

  setTimeout(() => {
     this.openModal();
     //AsyncStorage.setItem('@TokenID:key', ID);
      this.setState({
        loading: false,
      });
    }, 1000);


                 
    });

// this.setState({
//       qr: false,
//       //frame:false,
//     });
// console.log(this.state.frame)
//ToastAndroid.show(this.state.frame, ToastAndroid.SHORT)
 //  if(e.nativeEvent.codeStringValue !== ''){
 // //Alert.alert(e.nativeEvent.codeStringValue)
 //  this.setState({
 //      qr: false
 //    });
 // console.log('xxx');
 //   //ToastAndroid.show(e.nativeEvent.codeStringValue, ToastAndroid.SHORT)
   

 //  }else{

 //  Alert.alert(e.nativeEvent.codeStringValue)

 //  }



 // this.setState({
 //      loading: true
 //    });
// ToastAndroid.show(e.nativeEvent.codeStringValue, ToastAndroid.SHORT)
//     this.setState({
//       qr: false
//     });
// Alert.alert(e.nativeEvent.codeStringValue)
   //   var _this = this;
   //  // ToastAndroid.show(e.nativeEvent.codeStringValue, ToastAndroid.SHORT)
   // _this.setState({
   //    loading: true
   //  });

   //const ID = e.nativeEvent.codeStringValue;

   //ToastAndroid.show(e.nativeEvent.codeStringValue, ToastAndroid.SHORT)

   //console.log('QR' + ' ' +e.nativeEvent.codeStringValue)
   //this._goToMainScreen(ID);

   //const num = 1405.04.22.00001'


    

    // setTimeout(() => {
    //  this._goToMainScreen();
    //  //AsyncStorage.setItem('@TokenID:key', ID);
    //   this.setState({
    //     loading: false,
    //      qr: false
    //   });
    // }, 2500);

   //  AsyncStorage.setItem('@TokenID:key', ID);
   // //console.log(`${list}`)
   // //this.getKey('`${ID}`');
 // const ID = e.nativeEvent.codeStringValue;
 //   this._goToMainScreen(ID)
 //  }
}
onBottomButtonPressed(event) {
    const captureImages = JSON.stringify(event.captureImages);
    Alert.alert(
      `${event.type} button pressed`,
      `${captureImages}`,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
  }
  //////////////////////


async getKey(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getMember(value, id);    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  getMember(value, id){

   
      var url =  DevApi + 'member/checker/' + id ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            firstname: result.firstname,
            lastname: result.lastname,
            loading: false
         });

           Alert.alert("Hai" + " " + result.firstname);

        })
        .catch((error) => { console.error(error); });

  }

  state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = (ID) => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });
 

    ///////////////

  render() {

     if (this.state.example) {
      const CameraScreen = this.state.example;
      return <CameraScreen />;
    }
     
    return (
      <View style={CommonStyles.normalPage}>
       <GradientNavigationBar
          navigator={this.props.navigator}
          titleText='QR CODE'
          
        />
<Loader
          loading={this.state.loading} />
        <View style={styles.container}>
        
        <View style={[{flex:1}, styles.elementsContainer]}>
          <View style={{flex: 1, flexDirection:'column', marginTop:10, marginLeft:0, marginRight:0}}>

         
           
           

            <CameraKitCameraScreen
        actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
        onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
        flashImages={{
          on: require('./../images/flashOn.png'),
          off: require('./../images/flashOff.png'),
          auto: require('./../images/flashAuto.png')
        }}
        showFrame={this.state.frame}
        scanBarcode={this.state.qr}
        laserColor={"blue"}
        surfaceColor={"black"}
        frameColor={"yellow"}
         onReadCode={this.onSuccess.bind(this)}
          //onReadQRCode={((event) => Alert.alert("Qr code found"))} //optional
        hideControls={true}
        // offsetForScannerFrame = {10}  
        // heightForScannerFrame = {300}  
        colorForScannerFrame={'blue'}
      />
         


          </View>
         
        </View>
      </View>
         <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           ID FKPPI {this.state.numberID}
          </Text>

          

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
           
            <TouchableOpacity onPress={() => this.getMain()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>CARI</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </View>
    );
  }



   _handleClickDownload() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }

  // Go to FindDoctorScreen
  _goToMainScreen(ID) {
    this.props.navigator.push({
      screen: "Fkppi.KontakListQrScreen",
      passProps: {
       ID: ID
      },
      animated: true,
      animationType: 'slide-horizontal',
      portraitOnlyMode: true,
   appStyle: {
    orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
  },
    });
  }
}

const styles = StyleSheet.create({
  
  elementsContainer: {
    flex: 1,
    margin:0

  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    margin: 0,
    height: 50,
    marginTop: 24,
  }
});
