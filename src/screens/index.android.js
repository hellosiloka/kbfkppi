import {Navigation} from 'react-native-navigation';


// Project Catcha
import StartUpScreen from './StartUpScreen';
import StartUpLogoScreen from './StartUpLogoScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import RegisterScreen from './RegisterScreen';
import SettingScreen from './SettingScreen';
import MainServiceScreen from './MainServiceScreen';
import DaftarScreen from './DaftarScreen';
import DaftarLoginScreen from './DaftarLoginScreen';
import KomunitasScreen from './KomunitasScreen';
import KomunitasDetailScreen from './KomunitasDetailScreen';
import MabesScreen from './MabesScreen';
import MabesSubScreen from './MabesSubScreen';
import FaqScreen from './FaqScreen';
import PesanScreen from './PesanScreen';
import KontakListScreen from './KontakListScreen';
import KontakListQrScreen from './KontakListQrScreen';
import KontakListOfficeScreen from './KontakListOfficeScreen';
import NewsScreen from './NewsScreen';
import NewsDetailScreen from './NewsDetailScreen';
import PesanDetailScreen from './PesanDetailScreen';
import FaqDetailScreen from './FaqDetailScreen';
import MabesDetailScreen from './MabesDetailScreen';
import ProfileScreen from './ProfileScreen';
import KartuScreen from './KartuScreen';
import QrCodeScreen from './QrCodeScreen';
import KontakScreen from './KontakScreen';
import DaftarMemberScreen from './DaftarMemberScreen';
import DaftarUlangScreen from './DaftarUlangScreen';
import KonfirmasiMemberScreen from './KonfirmasiMemberScreen';
import DaftarEditScreen from './DaftarEditScreen';
import Menu from './MainMenu';






export function registerScreens(store: {}, Provider: {}) {
  
  // Project
  Navigation.registerComponent('Fkppi.StartUpScreen', () => StartUpScreen, store, Provider);
  Navigation.registerComponent('Fkppi.StartUpLogoScreen', () => StartUpLogoScreen, store, Provider);
  Navigation.registerComponent('Fkppi.SignInScreen', () => SignInScreen, store, Provider);
  Navigation.registerComponent('Fkppi.SignUpScreen', () => SignUpScreen, store, Provider);
  Navigation.registerComponent('Fkppi.RegisterScreen', () => RegisterScreen, store, Provider); 
  Navigation.registerComponent('Fkppi.SettingScreen', () => SettingScreen, store, Provider);
  Navigation.registerComponent('Fkppi.MainServiceScreen', () => MainServiceScreen, store, Provider);
  Navigation.registerComponent('Fkppi.DaftarScreen', () => DaftarScreen, store, Provider);
  Navigation.registerComponent('Fkppi.DaftarLoginScreen', () => DaftarLoginScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KomunitasScreen', () => KomunitasScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KomunitasDetailScreen', () => KomunitasDetailScreen, store, Provider);
  Navigation.registerComponent('Fkppi.MabesScreen', () => MabesScreen, store, Provider);
  Navigation.registerComponent('Fkppi.MabesSubScreen', () => MabesSubScreen, store, Provider);
  Navigation.registerComponent('Fkppi.FaqScreen', () => FaqScreen, store, Provider);
  Navigation.registerComponent('Fkppi.PesanScreen', () => PesanScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KontakListScreen', () => KontakListScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KontakListQrScreen', () => KontakListQrScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KontakListOfficeScreen', () => KontakListOfficeScreen, store, Provider);
  Navigation.registerComponent('Fkppi.NewsScreen', () => NewsScreen, store, Provider);
  Navigation.registerComponent('Fkppi.NewsDetailScreen', () => NewsDetailScreen, store, Provider);
  Navigation.registerComponent('Fkppi.PesanDetailScreen', () => PesanDetailScreen, store, Provider);
  Navigation.registerComponent('Fkppi.FaqDetailScreen', () => FaqDetailScreen, store, Provider);
  Navigation.registerComponent('Fkppi.MabesDetailScreen', () => MabesDetailScreen, store, Provider);
  Navigation.registerComponent('Fkppi.ProfileScreen', () => ProfileScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KartuScreen', () => KartuScreen, store, Provider);
  Navigation.registerComponent('Fkppi.QrCodeScreen', () => QrCodeScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KontakScreen', () => KontakScreen, store, Provider);
  Navigation.registerComponent('Fkppi.DaftarMemberScreen', () => DaftarMemberScreen, store, Provider);
  Navigation.registerComponent('Fkppi.DaftarUlangScreen', () => DaftarUlangScreen, store, Provider);
  Navigation.registerComponent('Fkppi.DaftarEditScreen', () => DaftarEditScreen, store, Provider);
  Navigation.registerComponent('Fkppi.KonfirmasiMemberScreen', () => KonfirmasiMemberScreen, store, Provider);
  Navigation.registerComponent('Fkppi.Menu', () => Menu, store, Provider);




 
}
