import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   ToastAndroid,
  ImageBackground,
  ScrollView,
  RefreshControl,
  TouchableHighlight,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import GridView from 'react-native-super-grid';
import {DevApi, MainApi} from './fkppi/Api';
import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import Swipeable from 'react-native-swipeable';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import Kosong from './fkppi/Kosong';
import { appSingleNavigation } from '../styles/navigatorStyle';
import { ConnectivityRenderer } from 'react-native-offline';
import ItemPesan from '../components/ItemPesan';
import Net from './fkppi/Net' ;
export default class PesanScreen extends Component {
  


  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: [],
      myToken: '',
      loading: true,
      refreshing: false,
      animating: true,
     
    }
   
  }
  

   componentDidMount() {
       StatusBar.setHidden(true);
       //this.closeActivityIndicator();
       var that = this;
       that.getKey();
        
    }




_onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  }


  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getAll(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 
  ////////

      getAll(value) {
      // Set loading to true to display a Spinner

      const FETCH_TIMEOUT = 5000;
      let didTimeOut = false;

      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'message/inbox';
      var that = this;
      // return fetch(url, {
      //         method: 'GET',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Authorization': value 
      //         }
      //   })
      //   .then(function(response) {
      //     return response.json();
      //   }).then(function(result) {
      //     //componentWillUnmount();
      //     that.setState({ data : result,
      //                     loading: false

      //                   });
      //      //data : this.state.data(result.data),

      //      console.log(that.state.data)

      //   })
      //   .catch((error) => { console.error(error); });
  new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                          loading: false

                        });

                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });



  }


  renderList(){

    if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:'10%'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }

    if (this.state.data == '') {
     
     return(

        <Kosong />

     )

    }
    else{


      return(

        <View style={CommonStyles.wrapperBox}>
            {
              this.state.data.map((item, index) => (
                <ItemPesan
                  key={item.id}
                  itemFrom={item.from}
                  itemCreatedAt={item.createdAt}
                  itemHeaderText={item.title}
                  itemId={item.id}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.from, item.createdAt, item.body)}
                />
              ))
            }
          </View>

      )
    }


  }
  ///////

  renderMain(){

     return(

      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <View style={{flex:1,justifyContent: 'center'}}>{this.renderList()}</View>
        ) : (
          <Net />
          
        )
      )}
    </ConnectivityRenderer>


     )

  }

  ////
   
  render() {

 

    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Pesan</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
        
         <ScrollView
         style={CommonStyles.noTabScroll}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        >
          
           {this.renderMain()}
          

        </ScrollView>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//
  _handleClickDetail(pesanId, title, from, tanggal, body) {
    this.props.navigator.push({
      title: "Detail",
      passProps: {
       pesanId: pesanId,
       title: title,
       from: from,
       tanggal: tanggal,
       body:body
      },
      screen: "Fkppi.PesanDetailScreen",
      animated: true,
    });
  }
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
