import React, { Component } from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  Platform,
  AsyncStorage,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import AutoHeightImage from 'react-native-auto-height-image';
import Modal from "react-native-simple-modal";
import {DevApi, MainApi} from './fkppi/Api';
import Loader from './fkppi/Loader';

import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { appSingleNavigation } from '../styles/navigatorStyle';

export default class DaftarScreen extends Component {
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: {},
      myToken: '',
      //member_id:0,
      loaderx: true,
      animating: true,
        loading: false,
      
    }
  }

  componentDidMount() {
       StatusBar.setHidden(true);
        var that = this;
       that.getKey();
       //that.getNarasi();
    }

    ////////////////



  async getDaftar() {
    this.setState({
      loading: true
    });

     this.tutupModal();
    //console.log('coords', coords)

    setTimeout(() => {
    //this._goToLoginScreen();
     this._goToDaftarScreen(this.state.firstname, this.state.lastname, this.state.avatar, this.state.member_id)
      this.setState({
        loading: false,
      });
    }, 1500);
  }

  /////


  async getUlang() {
    this.setState({
      loading: true
    });
     this.closeModal();

    
    //console.log('coords', coords)

    setTimeout(() => {
    //this._goToLoginScreen();
     this._goToDaftarUlangScreen(this.state.firstname, this.state.lastname, this.state.avatar, this.state.member_id)
      this.setState({
        loading: false,
      });
    }, 1500);
  }

  ////


 
  async getEdit() {
    this.setState({
      loading: true
    });

     this.closeModal();
    //console.log('coords', coords)

    setTimeout(() => {
      this._goToLoginScreen();
     //this._goToDaftarEditScreen(this.state.firstname, this.state.lastname, this.state.avatar, this.state.member_id)
      this.setState({
        loading: false,
      });
    }, 1500);
  }

    async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

//////

//////



getNarasi(){


var url =  'https://raw.githubusercontent.com/dait23/nomad/master/narasi.jpg' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
          
        })
        .then(function(response) {
          return response;
        }).then(function(result) {
          that.setState({
            narasi: result.url
         });

          console.log(result.url)

        })
        .catch((error) => { console.error(error); });


}

///////////////////



   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loaderx: true
      });
      

      //console.log(this.state.myToken)
      
      //var ID = this.props.newsId

      var url =  DevApi + 'user/profile' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            firstname: result.firstname,
            lastname: result.lastname,
            no_anggota: result.idfkppi,
            avatar: result.avatar,
            member_id: result.member_id,
            loaderx: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }


  state = { open: false, buka:false };

  modalDidOpen = () => console.log("Modal did open.");
  modalDidBuka = () => console.log("Modal did Buka.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  modalDidTutup = () => {
    this.setState({ buka: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

   modalTutup = () => {
    this.setState({ buka: false });
    console.log("Modal did tutup.");
  };

  openModal = () => this.setState({ open: true });

  bukaModal = () => this.setState({ buka: true });


  closeModal = () => this.setState({ open: false });
  tutupModal = () => this.setState({ buka: false });
///////

renderButton(){
if (this.state.loaderx) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:50}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
  if(this.state.member_id == 0){


    return(

        <View>
               <TouchableOpacity onPress={this.openModal}>
                  <View style={[CommonStyles.buttonFieldYellow, styles.buttonBox, {marginBottom:5, marginTop: spaceHeight * 0.15}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#fff', }]}>
                      Pendaftaran Ulang * 
                     
                    </Text>
                   
                  </View>

                  <Text style={{fontSize:11, color:'#fff', marginBottom:20, }}>* Apabila sudah memiliki KTA lama</Text>
                </TouchableOpacity>
        
                 <TouchableOpacity onPress={this.bukaModal}>
                  <View style={[CommonStyles.buttonFieldLogin, styles.buttonBox, { marginTop:10, }]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Daftar Baru
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>


      )


  }else{


    return(
    <View>
        

           <View style={{marginTop:50}}>
        
        
                 <TouchableOpacity onPress={() => this.getEdit()}>
                  <View style={[CommonStyles.buttonFieldYellow, styles.buttonBox, {marginBottom: spaceHeight * 0.1}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Ubah Data
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
 </View>

      )


  }


}

  render() {
    return (
       
       <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
   <Loader
          loading={this.state.loading} />
      <View style={styles.container}>

       <View style={[{flex:2, marginTop: deviceHeight * 0.08}, styles.elementsContainer]}>

         <View style={{flex: 1}}>
             <View style={[styles.logoBox,{height: 130, width: 90}]}>
              <Image
                source={require('../../img/fkppi/logo2.png')}
                style={{width: 90, height: 130}}
              />
            </View>
         </View>
          <View style={{flex: 7}}>

               <View style={styles.formBox}>
                  
                 
                  {this.renderButton()}

             

              

            

               </View>
           
          </View>
          
       </View>

       <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 6,
            padding: 0,
            height: 'auto',
            backgroundColor: "#fff",
            margin: deviceWidth * 0.03,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
        <View style={{height:50, backgroundColor:'#404f3d', borderTopLeftRadius:6, borderTopRightRadius:6, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color:'#fff', fontSize:18}}>Pendaftaran Ulang :</Text>
        </View> 

        <View style={{marginLeft:0, marginRight:0}}> 
        
        <AutoHeightImage
          width={deviceWidth - 15}
          source={{uri: 'https://res.cloudinary.com/catcha/image/upload/v1542095081/narasi.jpg',method: 'POST',
    headers: {
      Pragma: 'no-cache',
    }}}
        />
        </View>

        <View style={{height: 40, marginBottom:30,marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.getUlang()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>Mengerti</Text>
            </TouchableOpacity>

          </View>

        
       
        </Modal>



        <Modal          
          open={this.state.buka}
          modalDidOpen={this.modalDidBuka}
          modalDidClose={this.modalDidTutup}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 6,
            padding: 0,
            height: 'auto',
            backgroundColor: "#fff",
            margin: deviceWidth * 0.025,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
        <View style={{height:50, backgroundColor:'#404f3d', borderTopLeftRadius:6, borderTopRightRadius:6, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color:'#fff', fontSize:18}}>Pendaftaran Baru :</Text>
        </View> 

        <View style={{marginLeft:0, marginRight:0}}> 
        
        <AutoHeightImage
          width={deviceWidth - 15}
          source={{uri: 'https://res.cloudinary.com/catcha/image/upload/v1542095078/narasi2.jpg',method: 'POST',
    headers: {
      Pragma: 'no-cache',
    }}}
        />
        </View>

        <View style={{height: 40, marginBottom:30,marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.getDaftar()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>Mengerti</Text>
            </TouchableOpacity>

          </View>

        
       
        </Modal>



      </View>
    </ImageBackground>
      
    );
  }

  _goToSignUpScreen() {
    this.props.navigator.push({
      screen: "Fkppi.SignUpScreen"
    });
  }

 _goToLoginScreen() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarLoginScreen",
      animated: true,
      animationType: 'fade',
      portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
          },
    });
  }


  _goToDaftarEditScreen(firstName, lastName, avatar, memberID) {
    this.props.navigator.push({
      screen: "Fkppi.DaftarEditScreen",
       passProps: {
       firstName: firstName,
       lastName: lastName,
       avatar: avatar,
       memberID: memberID,
       portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', 
          },
      },
    });
  }


  _goToDaftarScreen(firstName, lastName, avatar, memberID) {
    this.props.navigator.push({
      screen: "Fkppi.DaftarMemberScreen",
      passProps: {
       firstName: firstName,
       lastName: lastName,
       avatar: avatar,
       memberID: memberID
      },
      animated: true,
       animationType: 'slide-horizontal',
       portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', 
          },
    });
  }


  _goToDaftarUlangScreen(firstName, lastName, avatar, memberID) {
    this.props.navigator.push({
      screen: "Fkppi.DaftarUlangScreen",
      passProps: {
       firstName: firstName,
       lastName: lastName,
       avatar: avatar,
       memberID: memberID
      },
      animated: true,
       animationType: 'slide-up',
       portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', 
          },
    });
  }

  
//
}

const ELEMENT_HEIGHT = 377;
const spaceHeight = deviceHeight - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      },
    elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      flexDirection: 'row',
      marginTop: 20

    },
    textBox:{
      marginLeft: 24,
      marginRight: 24,

    },
    textItem:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Medium',fontSize: 16,lineHeight: 23
    },
    footerBox:{
      flex: 1,
      width: 40,
      height: 40,
      alignSelf:'flex-start'
    },
    textFooter:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Bold',fontSize: 16,lineHeight: 23
    },
    formBox: {
    marginTop: deviceHeight * 0.20,
    alignItems: 'center',
    marginBottom: spaceHeight * 0.05,
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
 titleBox: {
   justifyContent: 'flex-start'
  },
});
