/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import PropTypes from 'prop-types';
import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
export default class Buttonx extends Component<{}> {

  static contextTypes = {
    isLoggedIn: PropTypes.bool,
    login: PropTypes.func,
    logout: PropTypes.func,
    props: PropTypes.shape({})
  };

  constructor(props) {
      super(props);
    }
  
  

  renderTitle(){
      var Log = "Login";
   var Out = "Logout";

     if(!this.context.isLoggedIn){
                  return(
                     
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      <Icon name="facebook" size={16} color="#fff"/>  Register by Facebook
                    </Text>

                    )
                }else{
                  return(
                     
                   <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      <Icon name="facebook" size={16} color="#fff"/>  Logout Facebook
                    </Text>

                    )
                }

  }

  render() {
    return (
      <View style={[]}>
       

        


   <View style={[CommonStyles.buttonFieldFb, styles.buttonBox, {marginBottom: spaceHeight * 0.03, marginTop: spaceHeight * 0.005, }]}>
        
        
                 <TouchableWithoutFeedback onPress={() => {
                if(!this.context.isLoggedIn){
                  this.context.login()
                }else{
                  this.context.logout()
                }

              }}>
                  <View>
                    {this.renderTitle()}
                  </View>
                </TouchableWithoutFeedback>
              </View>


          </View>
    );
  }
}

//

const ELEMENT_HEIGHT = 377;
const spaceHeight = deviceHeight - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
   buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
});
