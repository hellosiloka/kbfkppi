import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
  ScrollView,
  RefreshControl,
  TouchableHighlight,
  AsyncStorage,
  ActivityIndicator,
  Animated
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import {DevApi, MainApi} from './fkppi/Api';
import GridView from 'react-native-super-grid';
import Kosong from './fkppi/Kosong';


import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemNews from '../components/ItemNews';






export default class NewsScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      data: [],
      popular:[],
      datax:[],
      hib:[],
      sport:[],
      umum:[],
      myToken: '',
      loading: true,
      refreshing: false,
      news: [
        {  
           id: 0,
           title: "FKPPI Apel di Monas, Angkat Jokowi sebagai Anggota Kehormatan",
           body: "Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet",
           image: "https://img.okeinfo.net/content/2017/11/10/337/1812060/gelar-apel-kebangsaan-fkppi-hentikan-ancaman-radikalisme-yang-mengancam-nkri-YUeSNIELmb.jpeg",
           publisher: "Detik",
           created_at: "2018-08-19 03:14:0",
           timeago: "1 Jam yang lalu."
        },
        {  
           id: 1,
           title: "FKPPI Apel di Monas, Angkat Jokowi sebagai Anggota Kehormatan",
           body: "Pengurus",
           image :"https://img.okeinfo.net/content/2017/11/10/337/1812060/gelar-apel-kebangsaan-fkppi-hentikan-ancaman-radikalisme-yang-mengancam-nkri-YUeSNIELmb.jpeg",
           publisher: "Detik",
            created_at: "2018-08-19 03:14:0",
           timeago: "1 Jam yang lalu."
        },
        {  
           id: 2,
           title: "FKPPI Apel di Monas, Angkat Jokowi sebagai Anggota Kehormatan",
           body: "Pengurus",
           image: "https://img.okeinfo.net/content/2017/11/10/337/1812060/gelar-apel-kebangsaan-fkppi-hentikan-ancaman-radikalisme-yang-mengancam-nkri-YUeSNIELmb.jpeg",
           publisher: "Detik",
            created_at: "2018-08-19 03:14:0",
           timeago: "1 Jam yang lalu."
        }
      ]
    }
   
  }


  

  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       that.getKey();
        
    }

 /////////

_onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  } 





  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getNews(value ); 
      this.getPol(value ); 
      this.getPopular(value ); 
      //this.getHiburan(value );
      this.getOlahraga(value ); 
      this.getUmum(value );   
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  ////

  getPol(value){

 
    this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/index/1';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ datax : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(result)

        })
        .catch((error) => { console.error(error); });

  }
  ////

   getHiburan(value){

 
    this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/index/2';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ hib: result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(result)

        })
        .catch((error) => { console.error(error); });

  }
 ///


 getOlahraga(value){

 
    this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/index/3';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ sport: result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(result)

        })
        .catch((error) => { console.error(error); });

  }
 


  ///


  getUmum(value){

 
    this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/index/4';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ umum: result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(result)

        })
        .catch((error) => { console.error(error); });

  }
 


  ///


  getNews(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/index';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ data : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(that.state.data)

        })
        .catch((error) => { console.error(error); });
  }
  ////////

getPopular(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'news/popular';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ popular : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

           console.log(that.state.data)

        })
        .catch((error) => { console.error(error); });
  }
///////






renderNews(){
  const Page = ({label}) => (
    <View style={styles.container}>
      <ScrollView 
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
       style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.data.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                  itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);


const Pol = ({label}) => (
    <View style={styles.container}>
      <ScrollView 
       refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
       style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {  

              this.state.datax.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                   itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);


const Hib = ({label}) => (
    <View style={styles.container}>
      <ScrollView 

      refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }

      style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.hib.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                   itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);


const Sport = ({label}) => (
    <View style={styles.container}>
      <ScrollView 

      refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }

      style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.sport.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                   itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);


const Umum = ({label}) => (
    <View style={styles.container}>
      <ScrollView 

      refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }

      style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.umum.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                   itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);


  const Pagex = ({label}) => (
    <View style={styles.container}>
      <ScrollView style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.popular.map((item, index) => (
                <ItemNews
                  key={item.id}
                  itemTitle={item.title}
                  itemContent={item.body}
                  itemImg={item.image}
                   itemLike={item.likes}
                  itemPublisher={item.publisher}
                  itemCreatedAt={item.created_at}
                  itemTime={item.created_at}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title, item.image)}
                />
              ))
            }
          </View>
        </ScrollView>
    </View>
);
 

  if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" style={{marginTop:20}}/>
             </View>)
    }else{

  return(

      
        <ScrollableTabView

              tabBarActiveTextColor="#ffbf20"
              tabBarBackgroundColor="#404f3d"
              tabBarInactiveTextColor="#fff"
              style={{height:50}}
              renderTabBar={() => <TabBar 
                underlineColor="#ffbf20"
                tabBarStyle={{ height:50, paddingTop:20, marginTop:-0 ,}}
                 />}>
            <Page tabLabel={{label: "TERBARU"}}  label="Terbaru"/>
            <Pagex tabLabel={{label: "TERPOPULER"}} label="Terpopuler"/>
            <Pol tabLabel={{label: "POLITIK"}} label="Politik"/>
            <Sport tabLabel={{label: "OLAHRAGA"}} label="Olahraga"/>
             <Umum tabLabel={{label: "UMUM"}} label="Umum"/>
          </ScrollableTabView>


    )
}

}

   
  render() {

  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.01}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Berita</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
               
              </Text>
            </View>
         </View>
        </View>
       

        <View

        style={{flex: 5, justifyContent: 'center', backgroundColor:'#fbfbfb' }}>
        
        {this.renderNews()}
       </View>

      

        
      </View>
      </ImageBackground>
    )
  }

//

_handleClickDetail(newsId, title, newsImg) {
    this.props.navigator.push({
      title: "Detail",
      passProps: {
       newsId: newsId,
       newsImg: newsImg,
       title: title,
      },
      screen: "Fkppi.NewsDetailScreen",
      animated: true,
      // animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }


  _handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }

  _handleClickNotificationButton() {
    this.props.navigator.push({
      screen: "Healer.NotificationScreen",
      animated: true,
    });
  }


}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },

});
