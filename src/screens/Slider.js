import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  AsyncStorage,
  Image,
  Alert,
    RefreshControl,
Dimensions,
  ActivityIndicator,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
   TouchableHighlight,
  Platform,
} from 'react-native';
import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import {DevApi, MainApi} from './fkppi/Api';
import Carousel from 'react-native-banner-carousel';
import {toJS } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import ImageSlider from 'react-native-image-slider';
import Kosong from './fkppi/Kosong';
import FitImage from 'react-native-fit-image';
 
const BannerWidth = Dimensions.get('window').width;

@inject('Main')
@observer
export default class Slider extends Component {


  constructor(props) {
    super(props);

    this.state = {
       data:[],
       animating: true,
       loading: true,
       refreshing: false,
       


    }

  }


 //  ////////////////////////
 componentDidMount() {
   
          var that = this;
       that.getKey();



      
           that.forceUpdate();
        
    }
 ///
 _onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  }
 componentWillUnmount() {

      this.props.Main.clearItems();

        
    }
    ///


////




   closeActivityIndicator = () => setTimeout(() => this.setState({ 
      animating: false }), 2000)

  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      //this.setState({myToken: value});
      //this.props.Main.fetchSlider(value); 
      this.getSlider(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

renderPage(image, index) {
        return (
            <View key={index}>
                <Image style={{ width: '100%', height: deviceHeight / 3.7}} source={{ uri: image }} />
            </View>
        );
    }

////////////////////////


getSlider(value) {

      const FETCH_TIMEOUT = 5000;
      let didTimeOut = false;
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'banner/index';
      var that = this;


  //////////////////////////////////////////////////////
     
     new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                          loading: false

                        });
                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });

   ///////
  }



renderSlider(){
 const {Main} = this.props;
    const store = this.props.Main;

    const listSlider = toJS(store.sliders);

 const images = this.state.images;

;

  return(
    

  
     


          <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                >
                    {this.state.data.map((img, index) => this.renderPage(img.image, index))}
                </Carousel>
     


    )


}

  render() {

    const {Main} = this.props;
    const store = this.props.Main;

    const listSlider = toJS(store.sliders);
  console.log(listSlider)

    const animating = this.state.animating
     
     if (this.state.loading){

         return(
         <View>
           <ActivityIndicator
               animating = {animating}
               color="#fff"
               size = "large"
               style = {styles.activityIndicator}/>
         </View>

         )

     }

    else if (this.state.data == '') {
     
     return(

        <Kosong />

     )

    }
     else{

      return (
    <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        >
      <View style={{flex:1}}>
       
          {this.renderSlider()}
        
      </View>
    </ScrollView>
    );


     }
     
  
  }




}

const styles = StyleSheet.create({
  
  elementsContainer: {
    flex: 1,
    margin:0

  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    margin: 0,
    height: 50,
    marginTop: 24,

  },
   slider: { height: deviceHeight / 3.7, width:'100%' },
    buttons: {
    zIndex: 1,
    height: 15,
    marginTop: -25,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',

  },
  button: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSelected: {
    opacity: 1,
    color: 'red',
  },
  customSlide: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  customImage: {
    width: deviceWidth / 1,
    height: deviceHeight / 4,
  },
  activityIndicator: {
      alignItems: 'center',
      justifyContent:'center',
      marginTop:80,

   }
});
