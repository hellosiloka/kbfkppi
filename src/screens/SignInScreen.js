import React, { Component } from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  Platform,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ImageBackground,
  TouchableHighlight,
  AsyncStorage,
  ToastAndroid,
  Switch,
  KeyboardAvoidingView
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-simple-modal";
//import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import {DevApi, MainApi} from './fkppi/Api';
import Loader from './fkppi/Loader';

import Buttonx from './Button';

import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { appSingleNavigation } from '../styles/navigatorStyle';

export default class SignInScreen extends Component {
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      hidePassword: true,
      email:'',
      token: "",
      error: "",
      logIn: false,
      data : {},
      myKey: null,
      loading: false,

    };
    this.onLoginPress = this.onLoginPress.bind(this);
    this.onForgotPass = this.onForgotPass.bind(this);
    this.toggleSwitch = this.toggleSwitch.bind(this);
  }

  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }
managePasswordVisibility = () =>
  {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

 toggleSwitch() {
    this.setState({ showPassword: !this.state.showPassword });
  }
  componentDidMount() {
       StatusBar.setHidden(true);
       //this.requestCameraPermission();
       //this.getMabes()
       this.forceUpdate();
      // _removeData = async () => {
      //               try {
      //                 await AsyncStorage.removeItem('@Token');
      //               } catch (error) {
      //                 // Error saving data
      //                 console.log(error);
      //               }
      //             }

        AsyncStorage.removeItem('@Token:key', (err, result) => {
                  console.log("Token Remove");
                });

         AsyncStorage.removeItem('@FormData:Key', (err, result) => {
                  console.log("FormData Remove");
                });
        
    }


    //////////////

    

    //////////////////

 async getLogin() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    this.onLoginPress();
    //this._goToMainScreen();

    // setTimeout(() => {
    //  this._goToMainScreen();
    //  //AsyncStorage.setItem('@Token:key', 'contohloginkali');
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }


  async getForgot() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    this.onForgotPass()
    //this._goToMainScreen();

    // setTimeout(() => {
    //  //this._goToMainScreen();
    //  //AsyncStorage.setItem('@Token:key', 'contohloginkali');
    //  this.closeModal();
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }

    ///////

  state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });


async saveKey(value) {
    try {
      await AsyncStorage.setItem('@Token:key', value);
      this.props.navigator.resetTo({

                        screen: "Fkppi.MainServiceScreen",
                        portraitOnlyMode: true,
                         appStyle: {
                          orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
                        },
                      });
      
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }

  /////// Login 

  onLoginPress() {

    if(this.state.username === ''){
       ToastAndroid.show('Username tidak boleh kosong', ToastAndroid.SHORT);
       this.setState({
                    loading: false,
                  });
     }
     else if(this.state.password === ''){
       ToastAndroid.show('Password tidak boleh kosong', ToastAndroid.SHORT);
       this.setState({
                    loading: false,
                  });
     }
     else {
        const FETCH_TIMEOUT = 5000;
       let didTimeOut = false;
        //ToastAndroid.show('OK', ToastAndroid.SHORT);
        let username = this.state.username;
        let password = this.state.password;
        let userDetails = {
          username:username,
          password:password
        }

        var url =  DevApi + 'user/login';
        var that = this;

        // return fetch(url,{
        //       method: 'POST',
        //       headers: {
        //         'Content-Type': 'application/json',
      
        //       },
        //       body: JSON.stringify({

        //         'username' : username,
        //         'password' : password


        //       }),
        // })
        //     .then((response) => {

        //         console.log(response.status);

                // if (response.status == '403') {

                //     ToastAndroid.show('Login Gagal', ToastAndroid.SHORT)
                  //   this.setState({
                  //   loading: false,
                  // });
                // }
        //         if (response.status == '200') {

         
        //            that.saveKey(response.headers.map.authorization[0]);
        //            // this.props.navigator.push({

        //            //      screen: "Fkppi.MainServiceScreen",
        //            //      portraitOnlyMode: true,
        //            //       appStyle: {
        //            //        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
        //            //      },
        //            //    });

        //            this.setState({
        //             loading: false,
        //           });
                  
        //         }
        //     });
      //////////////////

       new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',

              },
              body: JSON.stringify({

                'username' : username,
                'password' : password


              }),
        })

        .then((response) => {

           clearTimeout(timeout);

           if(!didTimeOut) {
                  
                if (response.status == '403') {

                    ToastAndroid.show('Login Gagal', ToastAndroid.SHORT)
                    that.setState({
                    loading: false,
                  });
                }
                if (response.status == '200') {
                    
                    AsyncStorage.setItem('@Username:key', username);
                    that.saveKey(response.headers.map.authorization[0]);
                    that.setState({
                    loading: false,
                  });

                }


           }


     //    });
     // .then((result) => {
     //     clearTimeout(timeout);
     //        if(!didTimeOut) {
     //             that.setState({ data : result,
     //                      loading: false

     //                    });

     //            console.log('fetch good! ', result);
     //            resolve(result);
     //        }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });




     }
  }
//

getMabes(value) {

      const FETCH_TIMEOUT = 5000;
      let didTimeOut = false;
      // Set loading to true to display a Spinner
      // this.setState({
      //     loading: true
      // });

      //this.forceUpdate();
      

      //console.log(this.state.myToken)
      
      
      var url =  'http://fkppi.catcha.id/public/api/mabes/index';
      var that = this;
      // return fetch(url, {
      //         method: 'GET',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Authorization': value 
      //         }
      //   })
      //   .then(function(response) {
      //     return response.json();
      //   }).then(function(result) {
      //     //componentWillUnmount();
      //     that.setState({ data : result,
      //                     loading: false

      //                   });
      //      //data : this.state.data(result.data),

      //      console.log(that.state.data)

      //   })
      //   .catch((error) => { console.error(error); });

  //////////////////////////////////////////////////////
     
     new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                          loading: false

                        });

                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });

   ///////
  }
  ////////////

  onForgotPass(){

    if(this.state.email === ''){

       ToastAndroid.show('Email tidak boleh kosong', ToastAndroid.SHORT);
       this.setState({
                    loading: false,
                  });

     }else{

        let email = this.state.email;

        var url =  DevApi + 'user/requestpassword';
        var that = this;


        return fetch(url,{
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
      
              },
              body: JSON.stringify({

                'email' : email,


              }),
        }).then((response) => {

                console.log(response.status);

                if (response.status == '200') {

                   this.closeModal();

                   ToastAndroid.show('Email Reset Password Terkirim', ToastAndroid.SHORT)
                  this.setState({
                    loading: false,
                  });

                }else{

                  ToastAndroid.show('Email Tidak Terdaftar', ToastAndroid.SHORT)
                  this.setState({
                    loading: false,
                  });
                }
            });




     }


  }


  render() {

    var _this = this;
    return (
       
       <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
<Loader
          loading={this.state.loading} />
      <View style={styles.container}>

       <View style={[{flex:2, marginTop: deviceHeight * 0.04}, styles.elementsContainer]}>

         <View style={{flex: 1}}>
             <View style={[styles.logoBox,{height: 130, width: 90}]}>
              <Image
                source={require('../../img/fkppi/logo2.png')}
                style={{width: 90, height: 130}}
              />
            </View>
         </View>
          <View style={{flex: 7}}>

               <View style={styles.formBox}>
                  <View style={CommonStyles.textInputField}>
                    <Image
                      source={require('../../img/fkppi/ic_user.png')}
                      style={{position:'absolute', bottom: 15,left: 20, width: 19, height: 18}}
                    />
                    <TextInput
                      placeholder='Username'
                      style={CommonStyles.textInput}
                      underlineColorAndroid='transparent'
                      keyboardType="default"
                      returnKeyType="next"
                      ref="loginUsername"
                      onChangeText={(username) => this.setState({username})}
                      onSubmitEditing={() => this.focusChangeField('loginPassword')}
                      value={this.state.username}
                    />
                  </View>
                  <View style={CommonStyles.textInputField}>
                   <Image
                    source={require('../../img/fkppi/ic_password.png')}
                    style={{position:'absolute',bottom: 15,left: 20, width: 15, height: 22}}
                  />
                  
                  <TextInput
                    secureTextEntry={this.state.hidePassword}
                    placeholder='Password'
                    style={CommonStyles.textInput}
                    underlineColorAndroid='transparent'
                    onChangeText={(password) => this.setState({password})}
                    keyboardType="default"
                    ref="loginPassword"
                    returnKeyType="done"
                   
                    value={this.state.password}
                  />
                   <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                    <Image source = { ( this.state.hidePassword ) ? require('../../img/fkppi/hide.png') : require('../../img/fkppi/view.png') } style = { styles.btnImage } />
                  </TouchableOpacity>
                  </View>
                   <View>
        
        
                 <TouchableOpacity underlayColor="#000" onPress={() => this.getLogin()}>
                  <View style={[CommonStyles.buttonFieldLogin, styles.buttonBox, {marginBottom: spaceHeight * 0.1, marginTop: spaceHeight * 0.09, }]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Sign in
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

               
             

               </View>
           
          </View>
           <View style={{flex:1, justifyContent: 'center', alignItems: 'center',}} >
            
            <View style={CommonStyles.fotBox}>

             <TouchableOpacity onPress={this.openModal}>
              <Text style={[
                      CommonStyles.light,
                      CommonStyles.shortSmallText,{color:'#fff'}]}>Forgot Password?</Text>
            </TouchableOpacity>

            <TouchableWithoutFeedback onPress={this._goToSignUpScreen.bind(this)} >
             <View>
              <Text style={[
                      CommonStyles.light,
                      CommonStyles.shortSmallText,{color:'#fff799'}]}>New here? SIgn Up</Text>
            </View>
            </TouchableWithoutFeedback>
            
           </View>

          </View>
       </View>

       <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 40,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold
          ,{ textAlign: 'center'}]}>
           Forgot Password
          </Text>

          <View style={{height: 60, marginTop: 20,borderColor:'#dce1e4',
    borderWidth: 0.5,
    borderStyle: 'solid',
    borderRadius: 0,
    backgroundColor:'#fbfbfb',}}>
           
                     <TextInput
                      placeholder='your e-mail address'
                      style={CommonStyles.textInputx}
                      underlineColorAndroid='transparent'
                      keyboardType='email-address'
                      onChangeText={(email) => this.setState({email})}
                      returnKeyType="done"
                     
                      value={this.state.email}
                    />

          </View>

          <View style={{height:55, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.getForgot()}>
            <View style={{width:'100%', height:55, justifyContent:'center'}}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>Reset my password</Text>
            </View>
            </TouchableOpacity>
         
          </View>
           
          </View>
       
        
  
        </Modal>



      </View>

    </ImageBackground>
      
    );
  }

  _goToSignUpScreen() {
    this.props.navigator.push({
      screen: 'Fkppi.RegisterScreen',
      animated: true,
      animationType: 'fade',
      portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
          },
    });
  }

    // Go to FindDoctorScreen
  _goToMainScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }
///


_goToRegisterScreen(firstname, lastname, email) {
    this.props.navigator.resetTo({
      screen: 'Fkppi.SignUpScreen',
      animated: true,
      animationType: 'slide-up',
      passProps: {
       firstname: firstname,
       lastname: lastname,
       email: email,
      },
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  // _goToUpdatePassScreen() {
  //   this.props.navigator.push({
  //     screen: "Fkppi.UpdatePassScreen",
  //     animationType: 'slide-up'
  //   });
  // }

  // _handleClickFortgotPass() {
  //   this.props.navigator.push({
  //     screen: "Fkppi.ForgotPasswordScreen"
  //   });
  // }
}

const ELEMENT_HEIGHT = 377;
const spaceHeight = deviceHeight - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      },
    elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      flexDirection: 'row',
      marginTop: 20

    },
    textBox:{
      marginLeft: 24,
      marginRight: 24,

    },
    textItem:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Medium',fontSize: 16,lineHeight: 23
    },
    footerBox:{
      flex: 1,
      width: 40,
      height: 40,
      alignSelf:'flex-start'
    },
    textFooter:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Bold',fontSize: 16,lineHeight: 23
    },
    formBox: {
    marginTop: deviceHeight * 0.18,
    alignItems: 'center',
    marginBottom: spaceHeight * 0.05,
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
 titleBox: {
   justifyContent: 'flex-start'
  },
   visibilityBtn:
  {
    position: 'absolute',
    right: 5,
    height: 30,
    width: 35,
    padding: 5,
    top:10,
    opacity:0.5
  },

  btnImage:
  {
    resizeMode: 'contain',
    height: '100%',
    width: '100%'
  },
});
