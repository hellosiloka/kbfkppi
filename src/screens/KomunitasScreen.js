import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
  StatusBar,
  Linking,
  ImageBackground,
   ScrollView,
  RefreshControl,
  TouchableHighlight,
  ActivityIndicator,
   ToastAndroid,
  AsyncStorage,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import GridView from 'react-native-super-grid';
import {DevApi, MainApi} from './fkppi/Api';
import Kosong from './fkppi/Kosong';
import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import MenuItemBox from '../components/MenuItemBox2';

import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import Net from './fkppi/Net' ;
import { appSingleNavigation } from '../styles/navigatorStyle';
import { ConnectivityRenderer } from 'react-native-offline';
export default class KomunitasScreen extends Component {
     static navigatorStyle = appSingleNavigation;
  constructor(props) {
    super(props);
    this. state = {
      data: [],
      myToken: '',
      loading: true,
      animating: true,
      refreshing: false,
    }
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);
        var that = this;
       that.getKey();
    }

   
  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getCom(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

_onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  }



getCom(value) {

      const FETCH_TIMEOUT = 5000;
      let didTimeOut = false;
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      this.forceUpdate();
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'community/index';
      var that = this;
      // return fetch(url, {
      //         method: 'GET',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Authorization': value 
      //         }
      //   })
      //   .then(function(response) {
      //     return response.json();
      //   }).then(function(result) {
      //     //componentWillUnmount();
      //     that.setState({ data : result,
      //                     loading: false

      //                   });
      //      //data : this.state.data(result.data),

      //      console.log(that.state.data)

      //   })
      //   .catch((error) => { console.error(error); });

  //////////////////////////////////////////////////////
     
     new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                          loading: false

                        });

                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });

   ///////
  }


renderList(){

    //const animating = this.state.animating
    
    if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:30}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
    if (this.state.data == '') {
     
     return(

        <Kosong />

     )

    }
    else{

      return(

          <GridView
        itemDimension={150}
        items={this.state.data}
        style={styles.gridView}
        renderItem={item => (
          <MenuItemBox
              boxTitle={item.title}
              boxIcon={{uri: item.image}}
              boxLink={item.link}
              onPressBoxItem={() =>this._handleClickDetail(item.id, item.name, item.link)}
            >
           
          </MenuItemBox>

        )}
      />


        )

    }
    

  }




   
  render() {

    const items = [
      { name: 'Fkppi', width: 67, height:96, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_fkppi.png' },
      { name: 'Bela Negara', width: 82, height:104, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_bela_negara.png' },
      { name: 'Fli', width: 106, height:103, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_fli.png' },
      { name: 'Perbakin', width: 80, height:98, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_perbakin.png' },
      { name: 'Pencak Silat', width: 86, height:86, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_ikatan_pencak_silat.png' },
      { name: 'Komunitas', width: 75, height:91, link:'https://google.com', url:'https://res.cloudinary.com/catcha/image/upload/v1534147477/logo_possi.png' },

    ];
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Komunitas</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
             
              </Text>
            </View>
         </View>
        </View>
       <View style={{flex: 5, backgroundColor: '#fbfbfb',justifyContent: 'center', flexDirection: 'row', }}>
     <ScrollView
         style={CommonStyles.noTabScroll}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        >
      
        
       
      {this.renderList()}

      
   </ScrollView>
    </View>
        
      </View>
      </ImageBackground>
    )
  }

//
  _handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }

  // _handleClickNotificationButton() {
  //   this.props.navigator.push({
  //     screen: "Healer.NotificationScreen",
  //     animated: true,
  //   });
  // }

  // // Go to FindHospitalScreen 
  // _handleClickFindHospital() {
  //   this.props.navigator.push({
  //     screen: "Healer.FindHospitalScreen",
  //     animated: true,
  //   });
  // }

  // // Go to ServicePriceScreen 
  // _handleClickServicePrice() {
  //   this.props.navigator.push({
  //     screen: "Healer.ServicePriceScreen",
  //     animated: true,
  //   });
  // }

  _handleClickDetail(Id, Name, Link) {
    this.props.navigator.push({
      title: "Detail",
      passProps: {
       Id: Id,
       Name: Name,
       Link: Link
      },
      screen: "Fkppi.KomunitasDetailScreen",
      animated: true,
      animationType: 'slide-horizontal'
    });
  }
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
