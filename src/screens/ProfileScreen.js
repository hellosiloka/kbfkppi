import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
   AsyncStorage,
  ScrollView,
  Easing,
  ActivityIndicator,
  TouchableHighlight,
  TouchableOpacity,
  ToastAndroid,
  TouchableWithoutFeedback
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';
import GridView from 'react-native-super-grid';
import ZoomImage from 'react-native-zoom-image';
import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import { ConnectivityRenderer } from 'react-native-offline';
import Net from './fkppi/Net' ;

import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemPesan from '../components/ItemPesan';
import Kosong from './fkppi/Kosong';

import Modal from "react-native-simple-modal";
import Loader from './fkppi/Loader';
export default class PesanScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.onLogoutPress = this.onLogoutPress.bind(this);
    this. state = {
      data: {},
      myToken: '',
      loader: true,
       loading: false,
      animating: true,
      
    }

  }
  

  componentDidMount() {
       //StatusBar.setHidden(true);
        var that = this;
       that.getKey();
    }

    ////////////////

    async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      console.log(value)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
  ///


 async getOut() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    //this.onLogoutPress()

    setTimeout(() => {
      this.onLogoutPress();
    }, 1500);
  }

//////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loader: true
      });

      const FETCH_TIMEOUT = 3000;
      let didTimeOut = false;
      

      //console.log(this.state.myToken)
      
      var ID = this.props.newsId

      var url =  DevApi + 'user/profile' ;
      var that = this;
      // return fetch(url, {
      //         method: 'GET',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Authorization': value 
      //         }
      //   })
      //   .then(function(response) {
      //     return response.json();
      //   }).then(function(result) {
         //  that.setState({
         //    firstname: result.firstname,
         //    lastname: result.lastname,
         //    email: result.email,
         //    username: result.username,
         //    avatar: result.avatar,
         //    jabatan: result.jabatan,
         //    dpw: result.dpw,
         //    no_anggota: result.no_anggota,
         //    isVerified: result.is_verified,
         //    loading: false
         // });

      //     console.log(result)

      //   })
      //   .catch((error) => { console.error(error); });

      new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loader: false

                        });
            
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                  that.setState({
                    firstname: result.firstname,
                    lastname: result.lastname,
                    email: result.email,
                    username: result.username,
                    avatar: result.avatar,
                    jabatan: result.jabatan,
                    dpw: result.dpw,
                    rayon: result.rayon,
                    member_id: result.member_id,
                    position: result.position,
                    no_anggota: result.idfkppi,
                    isVerified: result.is_verified,
                    loader: false
                 });
                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });
  }


///////////////

state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });


  onLogoutPress(){

     var that = this;
     AsyncStorage.removeItem('@Username:key', (err, result) => {
                  console.log("Username Remove");
                });

     AsyncStorage.removeItem('@Token:key', (err, result) => {

       this._goToLoginScreen();

      console.log("Logout");
      this.setState({
        loading: false,
      });

      this.closeModal();
      
      });

  }


  renderPesan(){
    
     if(this.state.no_anggota == null){
        
        return(
           <View style={{width:54}}></View>
          )

     }else{
  
       return(

          <TouchableOpacity onPress={this._handleClickPesan.bind(this)}>
                <Image
                  source={require('../../img/fkppi/ic_email.png')}
                  style={{width: 54, height: 54}}
                />
              </TouchableOpacity>

       )

     }



  }


  renderNomer(){

   if(this.state.no_anggota == null){

      return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>Belum Ada</Text>

        )
   }else{

     return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>{this.state.no_anggota}</Text>

        )

   }

  }
 /////////

  renderJabatan(){

   if(this.state.position == null){

      return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>Belum Ada</Text>

        )
   }else{

     return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>{this.state.position}</Text>

        )

   }

  }
  /////////

renderDpw(){

   if(this.state.rayon == null){

      return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>Belum Ada</Text>

        )
   }else{

     return(
          
            <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>{this.state.rayon}</Text>

        )

   }

  }

///////

  renderAvatar(){
let styles = {
  img: {} // custom styles of original image component
};


   if(this.state.avatar == null){

      return(
          
            <Image
                  source={require('../../img/fkppi/avatars.png')}
                  style={{width: 122, height: 122,borderRadius: 122,borderColor:'#fff',borderWidth:4}}
                />
        )
   }else{

     return(
          
             

              <ZoomImage
                source={{uri: this.state.avatar}}
                imgStyle={{width: 122, height: 122,borderRadius: 122,borderColor:'#fff',borderWidth:4}}
                style={styles.img}
                duration={2100}
                enableScaling={false}
                easingFunc={Easing.ease}
              />

        )

   }

  }
  ///////

  renderKartu(){
    
    if(this.state.no_anggota == null){
   
      return(
        
          <View></View>

        )

    }else{
   
       return(

       <TouchableOpacity onPress={this._handleClickKartu.bind(this)}>
                <View style={{flexDirection: 'row', marginTop:10}}> 
                <Image
                  source={require('../../img/fkppi/ic_id_card.png')}
                  style={{width: 21, height: 18, marginRight:10}}  />
                   
                   <Text style={{color:'#adadad', fontSize:16}}>Kartu Anggota</Text>
                  </View>
               </TouchableOpacity>


       )

    }

  }


  renderMain(){

      if (this.state.loader) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:'10%'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
    if(this.state.firstname == null){
      return(

         <Kosong />


      )
    }

    else{

    
    return(
       
       <View>
          
      
            <View style={styles.avatarContainer}>

              {this.renderPesan()}
              {this.renderAvatar()}
              <TouchableOpacity onPress={this._handleClickSetting.bind(this)}>
                <Image
                  source={require('../../img/fkppi/ic_setting.png')}
                  style={{width: 54, height: 54}}
                />
              </TouchableOpacity>


            </View>

            <View style={styles.overviewContainer}>
              
              <Text style={{fontFamily: 'Roboto-Bold', color: '#000', fontSize:21}}>
                {this.state.firstname} {this.state.lastname}
              </Text>

              
                  {this.renderKartu()}               

            </View>
          <ScrollView style={CommonStyles.noTabScroll}>
            <View style={{flex:1, height:'auto', margin:20}}>

             <View style={{borderTopWidth:1, borderColor:'#dce1e4', marginTop:10}}>
               <Text style={{color:'#959595', fontSize:13, fontFamily:'Roboto-Light', marginTop:10}}>Username</Text>
               <Text style={{color:'#000', fontSize:18, fontFamily:'Roboto-Regular'}}>{this.state.username}</Text>
             </View>

              <View style={{borderTopWidth:1, borderColor:'#dce1e4', marginTop:10}}>
               <Text style={{color:'#959595', fontSize:13, fontFamily:'Roboto-Light', marginTop:10}}>Nomor anggota</Text>
               {this.renderNomer()}
             </View>

              <View style={{borderTopWidth:1, borderColor:'#dce1e4', marginTop:10}}>
               <Text style={{color:'#959595', fontSize:13, fontFamily:'Roboto-Light', marginTop:10}}>Jabatan</Text>
               {this.renderJabatan()}
             </View>

             <View style={{borderTopWidth:1, borderColor:'#dce1e4', marginTop:10}}>
               <Text style={{color:'#959595', fontSize:13, fontFamily:'Roboto-Light', marginTop:10}}>Rayon</Text>
               {this.renderDpw()}
             </View>


             <View>
        
        
                 <TouchableOpacity onPress={this.openModal}>
                  <View style={[CommonStyles.buttonFieldYellow2, styles.buttonBox, {marginTop: spaceHeight * 0.20}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      Sign Out
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
          

            
        
            
          </View>
        </ScrollView>
       </View>


    )
}
  }




  renderOnline(){

    return(

    <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       <Loader
          loading={this.state.loading} />
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.03}, styles.elementsContainer]}>

          <View style={{flex: 1, }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Profile</Text>
              
            </View>
         </View>


        </View>

   
       <View style={{flex: 6, backgroundColor:'#f9f9f9' }}>
     <ScrollView style={CommonStyles.noTabScroll}>
         {this.renderMain()}
        
     </ScrollView>
       </View>
    
       <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 6,
            padding: 0,
            height: 200,
            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
        <View style={{height:50, backgroundColor:'#404f3d', borderTopLeftRadius:6, borderTopRightRadius:6, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color:'#fff', fontSize:18}}>Confirmation :</Text>
        </View>

        <View style={{marginLeft:50, marginRight:50}}> 
         <Text style={{textAlign: 'center',marginTop:15, fontSize: 15, color:'#2d2a26'}}>
         Are you sure you want to logout ?
        </Text>
        </View>

        <View style={{flexDirection:'row', justifyContent:'center', marginTop:20}}> 
          
          <View style={[CommonStyles.buttonFieldYellow2Full, {marginTop: spaceHeight * 0.0, marginRight:20}]}>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getOut()}>
                  <View style={{width:'100%', height:45, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#fff', textAlign:'center' , fontSize:15}]}>
                      Yes
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>


            <View style={[CommonStyles.buttonFieldYellow2Half, {marginTop: spaceHeight * 0.0}]}>
        
        
                 <TouchableWithoutFeedback onPress={this.closeModal}>
                  <View style={{width:'100%', height:45, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      No
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

        </View>
       
        </Modal>
        
      </View>


     
      </ImageBackground>



    )



  }

   
  render() {

  
    
    return (
      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <View>{this.renderOnline()}</View>
        ) : (
          <Net />
          
        )
      )}
    </ConnectivityRenderer>
    )
  }

//
  _handleClickPesan() {
    this.props.navigator.push({
      screen: "Fkppi.PesanScreen",
      animated: true,
    });
  }

  _handleClickKartu() {
    this.props.navigator.push({
      screen: "Fkppi.KartuScreen",
      animated: true,
    });
  }

  // Go to CallDoctorsScreen
  _handleClickSetting() {
    this.props.navigator.push({
      screen: "Fkppi.SettingScreen",
      animated: true,
    });
  }
     // Go to FindDoctorScreen
  _goToLoginScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.StartUpLogoScreen",
      animated: true,
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }
}

  

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  ///////
   avatarContainer: {
    height: 122,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 15,
    marginLeft: 27,
    marginRight: 27,
    marginTop:20,
    position: 'relative',

    
  },
  redCircle: {
    position: 'absolute',
    top: 10, 
    right: (deviceWidth - 130) / 2,
    width: 15,
    height: 15,
    borderRadius: 200,
  },
  overviewContainer: {
    alignItems: 'center',
  },
});
