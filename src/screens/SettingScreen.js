import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  Image,
  PixelRatio,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
  Platform,
  AsyncStorage,

} from 'react-native';
import { Navigation } from 'react-native-navigation';

// import GradientButton from '../elements/GradientButton';
import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {DevApi, MainApi} from './fkppi/Api';
import { TextField } from 'react-native-material-textfield';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { RaisedTextButton } from 'react-native-material-buttons';
import Loader from './fkppi/Loader';
import ImagePicker from 'react-native-image-picker';  
import Modal from "react-native-simple-modal";
export default class SettingScreen extends Component {
  static navigatorStyle = noNavTabbarNavigation;

  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
      this.onChangeText = this.onChangeText.bind(this);
      this.onSubmitFirstName = this.onSubmitFirstName.bind(this);
      this.onSubmitLastName = this.onSubmitLastName.bind(this);
      this.onSubmitAbout = this.onSubmitAbout.bind(this);
      this.onSubmitUsername = this.onSubmitUsername.bind(this);
      this.onSubmitEmail = this.onSubmitEmail.bind(this);
      this.onSubmitPassword = this.onSubmitPassword.bind(this);
      this.onSubmitPassword2 = this.onSubmitPassword2.bind(this);
      this.onAccessoryPress = this.onAccessoryPress.bind(this);

       this.getUpdateKey = this.getUpdateKey.bind(this);

      this.firstnameRef = this.updateRef.bind(this, 'firstname');
      this.lastnameRef = this.updateRef.bind(this, 'lastname');
      this.aboutRef = this.updateRef.bind(this, 'about');
      this.usernameRef = this.updateRef.bind(this, 'username');
      this.emailRef = this.updateRef.bind(this, 'email');
      this.passwordRef = this.updateRef.bind(this, 'password');
      this.password2Ref = this.updateRef.bind(this, 'password2');

      this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);
      this. state = {
      data: {},
      myToken: '',
      loader: true,
      loading: false,
      animating: true,
      firstname: '',
          username:'',
          lastname: '',
          password:'',
          password2:'',
          about: '',
          avatar:'',
          avatarx:'',
          secureTextEntry: true,
          avatarSource: null,
          videoSource: null
    }
  }

 ////////////

state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });

 async getUpdate() {
    this.setState({
      loading: true
    });

    

    this.getUpdateKey();

    // setTimeout(() => {
    //  // this._goToMainScreen();
    //  // AsyncStorage.setItem('@Token:key', 'contohloginkali');
    //  this.getUpdateKey();
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }


 async getProfile() {
    this.setState({
      loading: true
    });

    setTimeout(() => {
     this.closeModal();
     this._goToProfile();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
      this.setState({
        loading: false,
      });
    }, 2000);
  }


async getOut() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    //this.onLogoutPress()

    setTimeout(() => {
      this.onLogoutPress();
    }, 1500);
  }

///
 onLogoutPress(){

     var that = this;

     AsyncStorage.removeItem('@Token:key', (err, result) => {

       this._goToLoginScreen();

      console.log("Logout");
      this.setState({
        loading: false,
      });

      //this.closeModal();
      
      });

  }

  ////////////////////////

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 300,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    
        console.log(file);

       
        this.setState({
          avatar: file,
          avatarSource: source
        });
      }
    });
  }

  //////////////////////
   
   componentDidMount() {
       //StatusBar.setHidden(true);
        var that = this;
       that.getKey();
    }

    ////////////////

    async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
  ///////

  async getUpdateKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.onUpdatePress(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
//////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loader: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.newsId

      var url =  DevApi + 'user/profile' ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            firstname: result.firstname,
            lastname: result.lastname,
             email: result.email,
             username: result.username,
             // password: result.password,
            avatar: result.avatar,
            loader: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }
 ////////////





 onUpdatePress(value) {


       // let firstname = this.state.firstname;
       //  let lastname = this.state.lastname;
       //  let email = this.state.email;
       //  let avatar = this.state.avatarx;
       //  let username = this.state.username;
       //  let password = this.state.password;
        
       

        //console.log('xxx:'+firstname)

       var url =  DevApi + 'user/profile/update';

      //var profiePicture = { uri: this.state.avatar, type: 'image/jpg', name: 'testPhotoName', } 

      if(this.state.password ===''){

          // ToastAndroid.show('Kosong', ToastAndroid.SHORT)
          //  this.setState({
          //     loading: false,
          //   });
           var input = this.state.username;
          //remove space
          var username = input.replace(/\s/g, '');

          //make string lower
          var newusername = username.toLowerCase();
           var formData = new FormData
           var _this = this;


           formData.append('firstname', this.state.firstname);
           formData.append('lastname', this.state.lastname);
           formData.append('username', newusername);
           formData.append('email', this.state.email);
           formData.append('avatar', this.state.avatar);

           console.log(formData)


          var xhr = new XMLHttpRequest();
          xhr.open('POST', url);
          xhr.responseType = 'json';
          console.log('OPENED', xhr.status);

          xhr.onprogress = function () {
            console.log('LOADING', xhr.status);
            //Alert.alert("Sedang Mengajukan");
            //ToastAndroid.show('Sedang Mengajukan', ToastAndroid.SHORT);
        };
      
        xhr.onload = function(e) {
          if (this.status == 200) {
            _this.openModal();
            _this.setState({
              loading: false,
            });
            //_this.getKey();

          }
        };

        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.setRequestHeader('Authorization', value); 
        xhr.setRequestHeader('Accept', 'application/json');
         
        xhr.send(formData);



      }else{ 



       if(this.state.password == this.state.password2){
          
            var input = this.state.username;
          //remove space
          var username = input.replace(/\s/g, '');

          //make string lower
          var newusername = username.toLowerCase();


            var formData = new FormData
           var _this = this;


           formData.append('firstname', this.state.firstname);
           formData.append('lastname', this.state.lastname);
           formData.append('username', newusername);
           formData.append('email', this.state.email);
           formData.append('password', this.state.password);
           formData.append('avatar', this.state.avatar);

           console.log(formData)


          var xhr = new XMLHttpRequest();
          xhr.open('POST', url);
          xhr.responseType = 'json';
          console.log('OPENED', xhr.status);

          xhr.onprogress = function () {
            console.log('LOADING', xhr.status);
            //Alert.alert("Sedang Mengajukan");
            //ToastAndroid.show('Sedang Mengajukan', ToastAndroid.SHORT);
        };
      
        xhr.onload = function(e) {
          if (this.status == 200) {
            _this.openModal();
            _this.setState({
              loading: false,
            });
            //_this.getKey();

          }
        };

        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.setRequestHeader('Authorization', value); 
        xhr.setRequestHeader('Accept', 'application/json');
         
        xhr.send(formData);

       }else{

            setTimeout(() => {
               ToastAndroid.show('password tidak sama', ToastAndroid.SHORT);
              this.setState({
                loading: false,
              });
            }, 1500);






       }

          

      }

     
       //  if (this.state.password ===''){
          
       //       var formData = new FormData();

       //      var _this = this;


       // formData.append('firstname', this.state.firstname);
       // formData.append('lastname', this.state.lastname);
       // formData.append('username', this.state.username);
       // formData.append('email', this.state.email);
       // formData.append('avatar', this.state.avatar);
        
       //  console.log(formData)

        //    var xhr = new XMLHttpRequest();
        //   xhr.open('POST', url);
        //   xhr.responseType = 'json';
        //   console.log('OPENED', xhr.status);

        //   xhr.onprogress = function () {
        //     console.log('LOADING', xhr.status);
        //     //Alert.alert("Sedang Mengajukan");
        //     //ToastAndroid.show('Sedang Mengajukan', ToastAndroid.SHORT);
        // };
      
        // xhr.onload = function(e) {
        //   if (this.status == 200) {
        //     _this.openModal();
        //     _this.setState({
        //       loading: false,
        //     });
        //     //_this.getKey();

        //   }
        // };

        // xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        // xhr.setRequestHeader('Authorization', value); 
        //  xhr.setRequestHeader('Accept', 'application/json');
         
        //  xhr.send(formData);


       //  ///end
       //  }else{

       //    var formData = new FormData();

       //      var _this = this;

       //      formData.append('firstname', this.state.firstname);
       //     formData.append('lastname', this.state.lastname);
       //     formData.append('username', this.state.username);
       //     formData.append('email', this.state.email);
       //     formData.append('password', this.state.password);
       //     formData.append('avatar', this.state.avatar);


       //     console.log(formData)

       //       var xhr = new XMLHttpRequest();
       //      xhr.open('POST', url);
       //      xhr.responseType = 'json';
       //      console.log('OPENED', xhr.status);

       //      xhr.onprogress = function () {
       //        console.log('LOADING', xhr.status);
       //        //Alert.alert("Sedang Mengajukan");
       //        //ToastAndroid.show('Sedang Mengajukan', ToastAndroid.SHORT);
       //    };
        
       //    xhr.onload = function(e) {
       //      if (this.status == 200) {
       //        _this.openModal();
       //        _this.setState({
       //          loading: false,
       //        });
       //        //_this.getKey();

       //      }
       //    };

       //    xhr.setRequestHeader('Content-Type', 'multipart/form-data');
       //    xhr.setRequestHeader('Authorization', value); 
       //     xhr.setRequestHeader('Accept', 'application/json');
           
       //     xhr.send(formData);




       //     ///end
       //  }
       
       // formData.append('firstname', this.state.firstname);
       // formData.append('lastname', this.state.lastname);
       // formData.append('username', this.state.username);
       // formData.append('email', this.state.email);
       // formData.append('password', this.state.password);
       // formData.append('avatar', this.state.avatar);
       
      



     }


  //////

  onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this['name','firstname', 'lastname', 'username', 'about', 'email', 'password'];

        if (ref && ref.isFocused()) {
          delete errors['firstname', 'lastname', 'username', 'about', 'email', 'password'];
        }
      }

      this.setState({ errors });
    }

    onChangeText(text) {
      ['firstname', 'lastname', 'username', 'about', 'email', 'password', 'password2']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    }

    onAccessoryPress() {
      this.setState(({ secureTextEntry }) => ({ secureTextEntry: !secureTextEntry }));
    }

    onSubmitFirstName() {
      this.lastname.focus();
    }

    onSubmitLastName() {
      this.about.focus();
    }

    onSubmitAbout() {
      this.email.focus();
    }

    onSubmitUsername() {
      this.username.focus();
    }

    onSubmitEmail() {
      this.password.focus();
    }

    onSubmitPassword() {
      this.password.blur();
    }

     onSubmitPassword2() {
      this.password2.blur();
    }

    onSubmit() {
      let errors = {};

      ['firstname', 'lastname', 'username' ,'email', 'password', 'password2']
        .forEach((name) => {
          let value = this[name].value();

          if (!value) {
            errors[name] = 'Tidak Boleh Kosong';
          } else {
            if ('password' === name && value.length < 6) {
              errors[name] = 'Password Minimal 6 Karakter';
            }

            else{

              console.log('xxx');
            }

          }
        });

      this.setState({ errors });
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    renderPasswordAccessory() {
      let { secureTextEntry } = this.state;

      let name = secureTextEntry?
        'visibility':
        'visibility-off';

      return (
        <MaterialIcon
          size={24}
          name={name}
          color={TextField.defaultProps.baseColor}
          onPress={this.onAccessoryPress}
          suppressHighlighting
        />
      );
    }

    ///////////////


  renderAvatar(){

    if(this.state.avatar === ''){

      return(
 
          <Image
              source={require('../../img/fkppi/photo.png')}  style={{width: 150, height: 150,borderRadius: 150,borderColor:'#ccc',borderWidth:4}}/>
        )

    }else{


      return(

         <Image
              source={{uri: this.state.avatar}}  style={{width: 150, height: 150,borderRadius: 150,borderColor:'#ccc',borderWidth:4}}/>

        )
    }


  }


  renderMain(){

    let { errors = {}, secureTextEntry, ...data } = this.state;
      let { firstname = 'name', lastname = 'house', username= '123' } = data;

      let defaultEmail = `${firstname}@${lastname}.com`
        .replace(/\s+/g, '_')
        .toLowerCase();

    if (this.state.loader) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:'10%'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }else{


      return(

         <View>

            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
              <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
              { this.state.avatarSource === null ? this.renderAvatar()
              :
                <Image style={styles.avatar} source={this.state.avatarSource} />
              }
          </View>
        </TouchableOpacity>
       
          <View style={[{flex:1}, styles.elementsContainer]}>
            <View style={{flex: 2}}>
                <TextField
                  ref={this.firstnameRef}
                  value={this.state.firstname}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
      
                  tintColor='rgb(255, 191, 32)'
           
                  onChangeText={(firstname) => this.setState({firstname})}
                  returnKeyType='next'
                  label='First Name'
                  error={errors.firstname}
                />

                <TextField
                  ref={this.lastnameRef}
                  value={this.state.lastname}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}

                  tintColor='rgb(255, 191, 32)'
 
                  onChangeText={(lastname) => this.setState({lastname})}
                  returnKeyType='next'
                  label='Last Name'
                  error={errors.lastname}
                />

                <TextField
                  ref={this.usernameRef}
                  value={this.state.username}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
  
                  tintColor='rgb(255, 191, 32)'
        
                  onChangeText={(username) => this.setState({username})}
                  returnKeyType='next'
                  label='Username'
                  error={errors.username}
                />
                <TextField
                  ref={this.emailRef}
                  value={this.state.email}
                  defaultValue={defaultEmail}
                  keyboardType='email-address'
                  autoCapitalize='none'
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
     
                  tintColor='rgb(255, 191, 32)'

                  onChangeText={(email) => this.setState({email})}
                  returnKeyType='next'
                  label='Email Address'
                  error={errors.email}
                />
                <TextField
                  ref={this.passwordRef}
                  value={this.state.password}
                  secureTextEntry={secureTextEntry}
                  autoCapitalize='none'
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  clearTextOnFocus={true}                  onFocus={this.onFocus}
                  tintColor='rgb(255, 191, 32)'
    
                  onChangeText={(password) => this.setState({password})}
                  returnKeyType='done'
                  label='New password'
                  error={errors.password}
             
                  maxLength={30}
                  characterRestriction={20}
                  renderAccessory={this.renderPasswordAccessory}
                />
                 <TextField
                  ref={this.password2Ref}
                  value={this.state.password2}
                  secureTextEntry={secureTextEntry}
                  autoCapitalize='none'
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  clearTextOnFocus={true}                  onFocus={this.onFocus}
                  tintColor='rgb(255, 191, 32)'
    
                  onChangeText={(password2) => this.setState({password2})}
                  returnKeyType='done'
                  label='Confirm new password'
                  error={errors.password2}
             
                  maxLength={30}
                  characterRestriction={20}
                  renderAccessory={this.renderPasswordAccessory}
                />
            </View>

             <View style={styles.container}>
              <RaisedTextButton style={{height: 50}} onPress={() => this.getUpdate()} title='Update' color='#ffbf20' titleColor='white' />
            </View>
            
          
        </View>


         </View>


      )
    }


  }

  render() {
      
    return (
      <View style={CommonStyles.normalPage}>
       <GradientNavigationBar
          navigator={this.props.navigator}
          titleText='Setting Profile'
        />
        <Loader
          loading={this.state.loading} />
        <ScrollView 
            style={CommonStyles.scrollView}
            keyboardShouldPersistTaps='handled'
            >
     
           
           {this.renderMain()}
           
        </ScrollView>
         <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Berhasil
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Profile anda berhasil diupdate
          </Text>

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.getProfile()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>OK</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </View>
    );
  }
//
_goToLoginScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.StartUpLogoScreen",
      animated: true,
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  _goToProfile() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

}

const styles = StyleSheet.create({
  formBox: {
    flex: 1,
    height: 155,
    backgroundColor: '#000000',
    alignItems: 'center', 
  },
  avatarBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 15,
     borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    borderRadius: 75,
    width: 150,
    height: 150

  },
  nameBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  elementsContainer: {
    flex: 1,
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 24
  },
  container: {
    margin: 0,
    height: 50,
    marginTop: 24,
  },
  avatarContainer: {
    borderColor: '#dce1e4',
    borderWidth: 3 / PixelRatio.get(),
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleBox: {
   justifyContent: 'flex-start'
  },
});
