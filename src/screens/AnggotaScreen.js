import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   Alert,
  ImageBackground,
  DatePickerAndroid,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  TouchableHighlight,
  TouchableWithoutFeedback
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Container, Header, Button, Body, Content, Card, CardItem, Form, TabHeading, Item, Picker, Label, Input, Textarea, Tab, Tabs, ScrollableTab } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  CameraKitCamera
} from 'react-native-camera-kit';
import {toJS } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';

//import CustomTabBar from '../components/CommonTabBar';
import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import { appSingleNavigation } from '../styles/navigatorStyle';
import ItemKontak from '../components/ItemKontak';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';


const Loading = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih Provinsi Terlebih  Dahulu</Text>
    </View>
)
////

const Loadingx = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih PD Terlebih Dahulu</Text>
    </View>
)


@inject('Dropdown', 'Provinsi', 'Rayon')
@observer
export default class AnggotaScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
        value: null,
        data: null,
      }
    
  }
  

  
  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       that.getToken();
       that.getTokenProv();

       
    }


componentWillUnmount() {

      this.props.Provinsi.clearItems();
      this.props.Dropdown.clearItems();
      //this.props.Rayon.clearItems();
        
    }




async getToken() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.props.Dropdown.fetchJabatan(value);
      this.props.Rayon.fetchPd(value);

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  ///

  async getTokenProv() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchProvinsi(value); 
      //this.props.Provinsi.fetchProvinsi(value); 
      //this.props.Provinsi.fetchProvinsi(value); 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  /////

  async getPc(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchPc(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

   async getRayon(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchRayon(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  /////

  onSelectedJab = (jabatanx_id) => {
     const Jab = jabatanx_id.toString();
    this.setState({ jabatan_id: Jab, jabatanx_id });

    //console.log(this.state.religion_id)
  };
   
  onSelectedPd = (pd_id) => {

    const GPD = pd_id.toString();
    this.setState({ pd: GPD, pd_id });

    this.getPc(pd_id);

    console.log(GPD)
  };
  

  onSelectedPc = (pc_id) => {
    const GPC= pc_id.toString();
    this.setState({ pc: GPC, pc_id });

    this.getRayon(pc_id);

     console.log(GPC)

    //console.log(this.state.g_dpw_id)
  };

  onSelectedRayon = (g_dprax_id) => {
    const GRAYON= g_dprax_id.toString();
    this.setState({ g_dpra_id: GRAYON, g_dprax_id });
     console.log(GRAYON)
  };

   
  render() {

    const {Dropdown, Provinsi, Rayon} = this.props;
    const store = this.props.Dropdown;
     const ray = this.props.Rayon;
     const prov = this.props.Provinsi;

    const listPd = toJS(ray.pd)
    const listPc = toJS(ray.pc)
    const listRayon = toJS(ray.rayon)
    const listJabatan = toJS(store.jabatan)
    
    return (
    <View style={{flex: 1, backgroundColor: '#fbfbfb'}}>
      <View style={{ marginTop:20, marginLeft:20, marginRight:20, marginBottom:20}}>
        
         <Form>
            <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Anggota <Text style={{color:'red'}}>*</Text></Label>
           <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.anggota} 
                          onChangeText={(anggota) => this.setState({anggota})}
                          />
                        </Item>
        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>No Anggota </Label>
           <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.anggota} 
                          onChangeText={(anggota) => this.setState({anggota})}
                          />
                        </Item>
        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jabatan</Label>
           
                        
                      <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listJabatan.map((category) => (
                           
                           {name: category.position, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Jabatan"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedJab.bind(this)}
                          selectedItems={this.state.jabatanx_id}
                        />
                     </View>
              <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PD</Label>
                     <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listPd.map((categoryz, index) => (
                           
                           {name: categoryz.pd, idz: categoryz.id,}


                          ))}
                          uniqueKey='idz'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih PD"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPd.bind(this)}
                          selectedItems={this.state.pd_id}
                        />
                      </View>
                <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PC</Label>
           
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listPc.map((category) => (
                           
                           {name: category.pc, idx: category.id,}


                          ))}
                          uniqueKey='idx'
                          single={true}
                          confirmText="Pilih"
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih PC"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPc.bind(this)}
                          selectedItems={this.state.pc_id}
                        />
                       </View>
              <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Rayon</Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listRayon.map((category) => (
                           
                           {name: category.rayon, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText="Pilih Rayon"
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedRayon.bind(this)}
                          selectedItems={this.state.g_dprax_id}
                        />
                    </View>
        </Form>

      </View>
    </View>
      
    )
  }

//


  _handleClickQr() {
    this.props.navigator.push({
      screen: "Fkppi.QrCodeScreen",
      animated: true,
    });
  }

  // Go to ServicePriceScreen 
  _handleClickKontakList() {
    this.props.navigator.push({
      screen: "Fkppi.KontakListScreen",
      animated: true,
    });
  }
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({

   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },
    buttonBox: {
    height: 50,
    flexDirection: 'row',
    marginBottom: 15,
  },
  signature: {
        flex: 1,
        borderColor: '#000033',
        borderWidth: 1,
    },
    buttonStyle: {
        flex: 1, justifyContent: "center", alignItems: "center", height: 50,
        backgroundColor: "#eeeeee",
        margin: 10
    }


});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});
