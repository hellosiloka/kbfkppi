import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
  ScrollView,
   RefreshControl,
  ToastAndroid,
  AsyncStorage,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Kosong from './fkppi/Kosong';
import GridView from 'react-native-super-grid';
import {DevApi, MainApi} from './fkppi/Api';
import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import { ConnectivityRenderer } from 'react-native-offline';
import { appSingleNavigation } from '../styles/navigatorStyle';
import Net from './fkppi/Net' ;

import ItemWithImage from '../components/ItemWithImage2';

export default class FaqScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.state = {
       data: [],
      loading: true,
      refreshing: false,
      faqsList: [
        {
          id: 0,
          itemImg: require('../../img/fkppi/ic_menu_daftar.png'),
          iconWidth: 51,
          iconHeight: 21,
          name: 'Daftar'
        },
        {
          id: 1,
          itemImg: require('../../img/fkppi/ic_menu_pesan.png'),
          iconWidth: 25,
          iconHeight: 44,
          name: 'Pesan'
        },
        {
          id: 2,
          itemImg: require('../../img/fkppi/ic_menu_mabes.png'),
          iconWidth: 44,
          iconHeight: 32,
          name: 'Mabes'
        },
        {
          id: 3,
          itemImg: require('../../img/fkppi/ic_menu_berita.png'),
          iconWidth: 40,
          iconHeight: 40,
          name: 'Berita'
        },
        {
          id: 4,
          itemImg: require('../../img/fkppi/ic_menu_kontak.png'),
          iconWidth: 40,
          iconHeight: 40,
          name: 'Kontak'
        },
        {
          id: 5,
          itemImg: require('../../img/fkppi/ic_menu_komunitas.png'),
          iconWidth: 36,
          iconHeight: 41,
          name: 'Komunitas'
        }
      ]
    }
   
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       that.getKey();
    }
   
   async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getFaqs(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
/////

_onRefresh = () => {
    this.setState({refreshing: true});
    this.getKey().then(() => {
      this.setState({refreshing: false});
    });
  }

  ////


      getFaqs(value) {

      const FETCH_TIMEOUT = 5000;
      let didTimeOut = false;
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'faq/index';
      var that = this;
      
  //////////////////////////////////////////////////////
     
     new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })

        .then((response) => response.json())
     .then((result) => {
         clearTimeout(timeout);
            if(!didTimeOut) {
                 that.setState({ data : result,
                          loading: false

                        });
                console.log('fetch good! ', result);
                resolve(result);
            }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });

   ///////
  }

  ////////



  renderList(){

    //const animating = this.state.animating
    
    if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center', marginTop:50}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }
    
    if (this.state.data == '') {
     
     return(

        <Kosong />

     )

    }

    else{

      return(

          <ScrollView style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            {
              this.state.data.map((item, index) => (
               <ItemWithImage
                  key={item.id}
                  itemImg={item.icon}
                  iconWidth={item.icon_width}
                  iconHeight={item.icon_height}
                  itemHeaderText={item.title}
                  onPressItem={() =>this._handleClickDetail(item.id, item.title)}

                />


              ))
            }
          </View>
        </ScrollView>


        )

    }
    

  }
 

////

renderMain(){

     return(

      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <View style={{flex:1,justifyContent: 'center'}}>{this.renderList()}</View>
        ) : (
          <Net />
          
        )
      )}
    </ConnectivityRenderer>


     )

  }

  render() {

  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>FAQ</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                Frequently Asked Questions
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        

         <ScrollView
         style={CommonStyles.noTabScroll}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        >
          {this.renderMain()}
        </ScrollView>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//
 _handleClickDetail(faqId, faqName) {
    this.props.navigator.push({
      title: "Detail",
      passProps: {
       faqId: faqId,
       faqName: faqName
      },
      screen: "Fkppi.FaqDetailScreen",
      animated: true,
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
