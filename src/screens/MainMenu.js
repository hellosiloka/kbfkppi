import React, {Component} from 'react';
import {
  Button,
  Text,
  TextInput,
  View,
  StyleSheet,
  ScrollView,
  AsyncStorage,
  Image,
  Alert,
  ActivityIndicator,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
   TouchableHighlight,
  Platform,
} from 'react-native';
import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';
import {DevApi, MainApi} from './fkppi/Api';

import MenuItemBox from '../components/MenuItemBox';
import { Navigation } from 'react-native-navigation';

import FitImage from 'react-native-fit-image';
import GridView from 'react-native-super-grid';
import { appSingleNavigation } from '../styles/navigatorStyle';
export default class Menu extends Component {
     static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);

    this._handleClickKomunitas = this._handleClickKomunitas.bind(this)
     this._handleClickMabes = this._handleClickMabes.bind(this)
     this._handleClickPesan = this._handleClickPesan.bind(this)
     this._handleClickKontakList = this._handleClickKontakList.bind(this)
     this._handleClickNews = this._handleClickNews.bind(this)
     this._handleClickDaftar = this._handleClickDaftar.bind(this)

  }
  state = { animating: true }

 componentDidMount() {
       //StatusBar.setHidden(true);
       this.closeActivityIndicator()
        
    }
 


   closeActivityIndicator = () => setTimeout(() => this.setState({ 
      animating: false }), 1500)

 //  async getKey() {
 //    try {
 //      const value = await AsyncStorage.getItem('@Token:key');
 //      this.setState({myToken: value});
 //      this.getCard(value );    
 //      //console.log(this.state.myToken)
 //    } catch (error) {
 //      console.log("Error retrieving data" + error);
 //    }
 //  }


renderMenu(){

  
  const linkDaftar =  this._handleClickDaftar.bind(this) ;
    const linkPesan =  this._handleClickPesan.bind(this) ;
    const linkMabes =  this._handleClickMabes.bind(this) ;
    const linkNews =  this._handleClickNews.bind(this) ;
    const linkKontak =  this._handleClickKontak.bind(this) ;
    const linkKomunitas =  this._handleClickKomunitas.bind(this) ;

  
    const items = [
      { name: 'Daftar', width: 96, height:75, unRead:0, link: linkDaftar, url: require('../../img/fkppi/ic_menu_daftarx.png') },
      { name: 'Pesan', width: 43, height:75, unRead:this.state.unRead,link: linkPesan, url:require('../../img/fkppi/ic_menu_pesan.png')  },
      { name: 'KB FKPPI', width: 78, height:57, unRead:0,link: linkMabes, url:require('../../img/fkppi/ic_menu_mabes.png')  },
      { name: 'Berita', width: 59, height:60, unRead:0, link: linkNews, url:require('../../img/fkppi/ic_menu_berita.png')  },
      { name: 'Kontak', width: 63, height:63, unRead:0, link: linkKontak, url:require('../../img/fkppi/ic_menu_kontak.png')  },
      { name: 'Komunitas', width: 59, height:66, unRead:0, link: linkKomunitas, url:require('../../img/fkppi/ic_menu_komunitas.png')  },

    ];


  return(
    
      
  
    <View>

       
      <GridView
        itemDimension={140}
        items={items}
        style={styles.gridView}
        renderItem={item => (
          <MenuItemBox
              boxTitle={item.name}
              boxIcon={{uri: item.url}}
              boxIconWidth={item.width} 
              boxIconHeight={item.height}
              onPressBoxItem={item.link}
            >
           
          </MenuItemBox>

        )}
      />
      

    </View>
     


    )



}

  render() {
    const animating = this.state.animating
     
     if (this.state.animating){

         return(
         <View style={{flex:1}}>
           <ActivityIndicator
               animating = {animating}
               color="#404f3d"
               size = "large"
               style = {styles.activityIndicator}/>
         </View>

         )

     }else{

      return (
      <View>
       
          {this.renderMenu()}
        
      </View>
    );


     }
     
  
  }

_handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
      animationType: 'slide-horizontal'
    });
  }

   _handleClickKomunitas() {
    this.props.navigator.push({
      screen: "Fkppi.KomunitasScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

  _handleClickMabes() {
    this.props.navigator.push({
      screen: "Fkppi.MabesScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

  _handleClickKontakList() {
    this.props.navigator.push({
      screen: "Fkppi.KontakListScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

   _handleClickKontak() {
    this.props.navigator.push({
      screen: "Fkppi.KontakScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

  _handleClickNews() {
    this.props.navigator.push({
      screen: "Fkppi.NewsScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }

 

  _handleClickPesan() {
    this.props.navigator.push({
      screen: "Fkppi.PesanScreen",
      animated: true,
       animationType: 'slide-horizontal'
    });
  }


}

const styles = StyleSheet.create({
  
  elementsContainer: {
    flex: 1,
    margin:0

  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    margin: 0,
    height: 50,
    marginTop: 24,
  },
   slider: { height: deviceHeight / 3.5, width:'100%' },
    buttons: {
    zIndex: 1,
    height: 15,
    marginTop: -25,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  button: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSelected: {
    opacity: 1,
    color: 'red',
  },
  customSlide: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  customImage: {
    width: deviceWidth,
    height: deviceHeight / 3.5,
  },
  activityIndicator: {
      alignItems: 'center',
      justifyContent:'center',
      marginTop:50,

   }
});
