import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
  Alert,
   StatusBar,
  ImageBackground,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  TouchableWithoutFeedback,
  TouchableHighlight,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Container, Header, Button, Body, Content, Card, CardItem, Form, TabHeading, Item, Picker, Label, Input, Textarea, Tab, Tabs, ScrollableTab, FormGroup  } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';
import GridView from 'react-native-super-grid';

import CommonStyles, {
  deviceHeight,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';
import Loader from './fkppi/Loader';
import ItemKontak from '../components/ItemKontak';

export default class KontakListQrScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this. state = {
      loader: false,
       loading: false,
       error: false,
       data:{},
      messages: [
        {  
           id: 0,
           name: "Ikhsan Laharo",
           jabatan: "Ketua Rayon Kec. Gamplong",
           imageUrl: require('../../img/fkppi/ic_default_profile.png'),
           telepon: "027 565687",
           handphone: "0992303122",
           status: "Aktif",
           organisasi: "Fkppi pemuda",
           DPD: "DI Yogyakarta",
           DPW: "sleman",
           rayon: "Gamplong"
        },
        {  
           id: 1,
           name: "Ikhsan Laharo",
           jabatan: "Ketua Rayon Kec. Gamplong",
           imageUrl: require('../../img/fkppi/ic_default_profile.png'),
           telepon: "027 565687",
           handphone: "0992303122",
           status: "Aktif",
           organisasi: "Fkppi pemuda",
           DPD: "DI Yogyakarta",
           DPW: "sleman",
           rayon: "Gamplong"
        },
        {  
           id: 2,
           name: "Ikhsan Laharo",
           jabatan: "Ketua Rayon Kec. Gamplong",
           imageUrl: require('../../img/fkppi/ic_default_profile.png'),
           telepon: "027 565687",
           handphone: "0992303122",
           status: "Aktif",
           organisasi: "Fkppi pemuda",
           DPD: "DI Yogyakarta",
           DPW: "sleman",
           rayon: "Gamplong"
        }
      ]
    }
   
  }
  

  componentDidMount() {
       StatusBar.setHidden(true);

    //    this.setState({
    //   loading: true
    // });

   
    // //console.log('coords', coords)
    // //this.getKey();


    // setTimeout(() => {
    //  //this.onSubmit();
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);

       //console.log(this.props.ID)

        var that = this;
        that.getKey();
    }

    async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      const QrID = await AsyncStorage.getItem('@QrID:key');
      //this.setState({myToken: value});
      this.getMember(value, QrID);    
      console.log(value)
      console.log(QrID)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  //////

async getMain() {
    this.setState({
      loader: true
    });

    setTimeout(() => {
     this._handleClickMain();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
     AsyncStorage.removeItem('@QrID:key', (err, result) => {
                  console.log("QRID Remove");
                });
      this.setState({
        loader: false,
      });
    }, 2000);
  }


  async getQr() {
    this.setState({
      loader: true
    });

    setTimeout(() => {
     this._handleClickQr();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
     AsyncStorage.removeItem('@QrID:key', (err, result) => {
                  console.log("QRID Remove");
                });
      this.setState({
        loader: false,
      });
    }, 2000);
  }


  ////
   


  async getData(value) {
    try {
      const result = await (value);
      
      console.log(result)
      this.setState({
            data : result,
            firstname: result.firstname,
            lastname: result.lastname,
            jabatan: result.organizations.position,
            statusAnggota: result.organizations.registered_date,
            avatar: result.photo,
            mobile:result.mobile,
            pd: result.organizations.pd,
            pc: result.organizations.pc,
            rayon: result.organizations.rayon,
            status: result.organizations.status,
            loading: false,
            error:false
         });

      console.log(this.state.statusAnggota)

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
}
     getMember(value, QrID){

     this.setState({
          loading: true
      });
      var url =  DevApi + 'member/checker/' + QrID ;
      console.log(url)
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then((response) => {

                console.log(response.status);

                if (response.status == '200') {
                    

                    this.getData(response.json());


                   console.log(response.json())
                  this.setState({
                    loading: false,
                  });

                }else{
                   
                   console.log('gagal')
                  //ToastAndroid.show('Email Tidak Terdaftar', ToastAndroid.SHORT)
                  this.setState({
                    loading: false,
                    error:true
                  });
                }
            });




     

  }



 renderError(){


    return(

     
     <View style={{marginLeft:20,marginRight:20, marginTop:10, justifyContent:'center'}}>
                   <Image
                        source={require('../../img/fkppi/ic_pattern_opps.png')}
                        style={{width:50, height:50,alignSelf:'center', marginTop:20, marginBottom:40}}
                      />
              
                    
                  <Body style={{justifyContent:'center', alignItems:'center'}}>
                   <Text style={{textAlign:'center', fontSize:20, color:'#000000', fontFamily:'Roboto-Bold'}}>Tidak Ditemukan!</Text>
                   <Text style={{textAlign:'center', fontSize:14, color:'#adadad', fontFamily:'Roboto-Regular',lineHeight:20}}>Anggota tidak/belum terdaftar.</Text>
                    <Image
                        source={require('../../img/fkppi/ic_ava_not_found.png')}
                        style={{width:90, height:130,alignSelf:'center', marginTop:10}}
                      />
                  </Body>

                   
            
                <View style={{flexDirection:'row', justifyContent:'center', marginTop:10}}> 
          
          <View style={[CommonStyles.buttonFieldYellow2Full, {marginTop: spaceHeight * 0.0, marginRight:20}]}>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getQr()}>
                  <View style={{width:'100%', height:55, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#fff', textAlign:'center' , fontSize:15}]}>
                      Scan Ulang
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>


            <View style={[CommonStyles.buttonFieldYellow2Half, {marginTop: spaceHeight * 0.0}]}>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getMain()}>
                  <View style={{width:'100%', height:55, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      Kembali
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

        </View>

               </View>



      )


 }


 renderMain(){

  if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }else{


     if(this.state.error == true){

     return(

     <View>{this.renderError()}</View>

      )

  }else{

    return(

       <View>{this.renderMember()}</View>

      )

  }




    }

  


 }


 renderStatus(){
   
     const statusAnggotax = this.state.statusAnggota;


     const Anggota = statusAnggotax;
    return(

                             <View style={{justifyContent:'space-between', flexDirection:'row'}}><Text style={{fontFamily: 'Roboto-Regular',color:'#2d2a26',  width:'50%'}}>Keanggotaan Sejak</Text><View style={{alignContent:'flex-start'}}><Text style={{color:'#2d2a26', textAlign:'left'}}>{Anggota}</Text></View></View>
    )

 }



 ///////////////


 renderMember(){
   
   
  
   

    
   
    return(


          <View style={{marginLeft:20,marginRight:20, marginTop:5, justifyContent:'center'}}>
                   <Image
                        source={require('../../img/fkppi/ic_success.png')}
                        style={{width:45, height:45,alignSelf:'center', marginTop:10, marginBottom:20}}
                      />
                  
                  <Body style={{justifyContent:'center', alignItems:'center',}}>
                   <Text style={{textAlign:'center', fontSize:20, color:'#000000', fontFamily:'Roboto-Bold'}}>Terkonfirmasi!</Text>
                   <Text style={{textAlign:'center', fontSize:14, color:'#adadad', fontFamily:'Roboto-Regular',lineHeight:20}}>Anggota terdaftar secara resmi.</Text>
                    <Image
                        source={{uri: this.state.avatar}}
                        style={{width:85, height:85,borderRadius:85, alignSelf:'center', marginTop:10}}
                      />
                  </Body>
                   
      
                 <View style={{backgroundColor:'#fff',marginTop:20, marginLeft:20, marginRight:20}}>
                   
                   <View style={styles.centerInfo}>
                     <Text style={{fontFamily: 'Roboto-Bold', color:'#2d2a26', fontSize:14}}>{this.state.firstname} {this.state.lastname}</Text>
                      <Text style={{fontFamily: 'Roboto-Medium', color:'#2d2a26', fontSize:13, lineHeight: 20, marginBottom:10}}>{this.state.jabatan}</Text>
                  
                     <View style={{justifyContent:'space-between', flexDirection:'row'}}><Text style={{fontFamily: 'Roboto-Regular',color:'#2d2a26',  width:'50%'}}>Status</Text><View style={{alignContent:'flex-start'}}><Text style={{color:'#2d2a26', textAlign:'left'}}>{this.state.status}</Text></View></View>

                    {this.renderStatus()}

                     <View style={{justifyContent:'space-between', flexDirection:'row'}}><Text style={{fontFamily: 'Roboto-Regular',color:'#2d2a26',  width:'50%'}}>PD</Text><Text style={{color:'#2d2a26'}}>{this.state.pd}</Text></View>
                     <View style={{justifyContent:'space-between', flexDirection:'row'}}><Text style={{fontFamily: 'Roboto-Regular',color:'#2d2a26',  width:'50%'}}>PC</Text><Text style={{color:'#2d2a26'}}>{this.state.pc}</Text></View>
                      <View style={{justifyContent:'space-between', flexDirection:'row'}}><Text style={{fontFamily: 'Roboto-Regular',color:'#2d2a26',  width:'50%'}}>Rayon</Text><Text style={{color:'#2d2a26'}}>{this.state.rayon}</Text></View>

                   </View>


                  </View>
                <View style={{flexDirection:'row', justifyContent:'center', marginTop:10}}> 
          
          <View style={[CommonStyles.buttonFieldYellow2Full, {marginTop: spaceHeight * 0.0, marginRight:20}]}>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getQr()}>
                  <View style={{width:'100%', height:55, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#fff', textAlign:'center' , fontSize:15}]}>
                      Scan Ulang
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>


            <View style={[CommonStyles.buttonFieldYellow2Half, {marginTop: spaceHeight * 0.0}]}>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getMain()}>
                  <View style={{width:'100%', height:55, justifyContent:'center'}}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      Kembali
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

        </View>

               </View>


      )



 }




 
  render() {

  
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
        <Loader
          loading={this.state.loader} />
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Kontak</Text>
              <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                Hasil Pencarian QRCode
              </Text>
            </View>
         </View>
        </View>

       <View style={{flex: 5, backgroundColor: '#fbfbfb' }}>
        
         <ScrollView style={CommonStyles.noTabScroll}>
          <View style={CommonStyles.wrapperBox}>
            <Container>
              <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
              
                {this.renderMain()}



              </Content>

            </Container>
          </View>
        </ScrollView>

       </View>

        
      </View>
      </ImageBackground>
    )
  }

//

 _handleClickQr() {
    this.props.navigator.push({
      screen: "Fkppi.QrCodeScreen",
      animated: true,
    });
  }
  _

  _handleClickMain() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

 
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
     centerInfo: {
    flex: 1, 
    margin: 10,
  },
  Info: {
    flex: 1, 
    margin: 10,
    justifyContent:'space-between',
    flexDirection:'row',
  },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
