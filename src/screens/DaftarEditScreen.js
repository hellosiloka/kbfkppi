import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   Alert,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  AsyncStorage,
  ToastAndroid,
  DatePickerAndroid,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,

  Animated
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import {DevApi, MainApi} from './fkppi/Api';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import Select from 'react-native-select-plus';
import GridView from 'react-native-super-grid';
import RNFetchBlob from 'rn-fetch-blob';
import {toJS } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Loader from './fkppi/Loader';
import Modal from "react-native-simple-modal";
import { Container, Header, Button, Body, Content, Card, CardItem, Form, TabHeading, Item, Picker, Label, Input, Textarea, Tab, Tabs, ScrollableTab } from 'native-base';





import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';

import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';

import { appSingleNavigation } from '../styles/navigatorStyle';

import ItemNews from '../components/ItemNews';

import moment from 'moment';
import 'moment/locale/id';
moment.locale('id');

const Loading = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih Provinsi Terlebih  Dahulu</Text>
    </View>
)

const Loadingx = props => (
  props.hasErrored ?
    <TouchableWithoutFeedback>
      <View style={styles.center}>
        <Text>oops... something went wrong. Tap to reload</Text>
      </View>
    </TouchableWithoutFeedback>
    :
    <View style={styles.center}>
      <ActivityIndicator size="large" color="#404f3d"/>
      <Text style={{textAlign:'center', marginTop:20}}>Pilih PD Terlebih Dahulu</Text>
    </View>
)





@inject('Dropdown', 'Provinsi', 'Rayon')
@observer
export default class DaftarEditScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      member: {},
      city:[],
      selectedItems: [],
      provinsi: [],
      kotax: [],
       loader: true,
      kec: [],
      kel: [],
      myToken: '',
      loading: false,
      avatar:'',
      avatarx:'',
      secureTextEntry: true,
      avatarSource: null,
      ktaBack: null,
      ktaFront: null,
      foto:null,
      ktp:null,
      simpleDate: new Date(),
      skep_pensiun:null,
       skep_pensiun_back:null,
      kta_tnipolri:null,
      kta_tnipolri_back:null,
      dok_tambahan:null,
      aktalahir:null,
      kk:null,
      videoSource: null,
      selected2: undefined,
      title1:'',
      firstname:'',
      lastname:'',
      title2:'',
      alias:'',
      nik:'',
      birthplace:'',
      simpleText: '',
      birthday: '',
      sex:'',
      religions_id:'',
      degree:'',
      occupations:'',
      maritalstatus:'',
      spousename:'',
      h_province_id:'',
      h_cities_id:'',
      h_districts_id:'',
      h_villages_id:'',
      h_address_1:'',
      h_address_2:'',
      h_rtrw:'',
      h_postcodes_id:'',
      h_phone:'',
      mobile:'',
      o_province_id:'',
      o_cities_id:'',
      o_districts_id:'',
      o_villages_id:'',
      o_address_1:'',
      o_address_2:'',
      o_rtrw:'',
      o_postcodes_id:'',
      o_phone:'',
      p_nrp:'',
      p_name:'',
      p_military_status_id:'',
      p_military_uo_id:'',
      p_military_rank:'',
      p_military_position:'',
      p_childrens:null,
      p_religions_id:'',
      p_kinship_id:'',
      p_info:'',
      g_members_memberstatus_id:'',
      g_organizations_id:'',
      g_registered_at:'',
      g_dpd_id:'',
      g_dpw_id:'',
      g_dpra_id:'',
      g_jabatan_id:'',
      g_jabatan_start:'',
      g_jabatan_end:'',
      d_kta_front:'',
      d_kta_back:'',
      d_foto:'',
      d_ktp:'',
      d_skep_pensiun:'',
      d_kta_tnipolri:'',
      d_dok_tambahan:'',
      d_aktalahir:'',
      d_kk:'',
      kota:[],
      



    }
   
  }

  ///


  state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });
 
////////

async getSubmit() {
    this.setState({
      loading: true
    });

    
    //console.log('coords', coords)

    this.getKeyx();
  }

onSelectedProvinsi = (provinsi) => {
    const Prov = provinsi.toString();
    this.setState({
      h_province_id: Prov, 
      provinsi,
      h_cities_id:'',
      h_districts_id:'',
      h_villages_id:'',
      regencyx:'Pilih Kota / Kabupaten',
      subdistrictx:'Pilih Kecamatan',
      villagex:'Pilih Kelurahan',
      kotax:'Pilih Kota / Kabupaten',
      kec:'Pilih Kecamatan',
      h_villagesx_id:'Pilih Kelurahan'
 


    });
    this.getKota(provinsi);
    //console.log(Prov)
  }

onSelectedKotax = (kotax) => {

   const Cit = kotax.toString();
    this.setState({h_cities_id: Cit, kotax });
    this.getKecamatan(kotax);
    //console.log(this.state.kotax)
  }

onSelectedKecx = (kec) => {

    const Dist = kec.toString();
    this.setState({h_districts_id: Dist, kec });
    this.getKelurahan(kec);
    //console.log(selectedItems)
  }


onSelectedKelx = (h_villagesx_id) => {

   const Vil = h_villagesx_id.toString();
    this.setState({h_villages_id: Vil, h_villagesx_id});
    //this.getKodepos(h_villages_id);
    //console.log(h_villages_id)
  }

showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'day'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'day'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

////

showPickerAnggota = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'day'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'day'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };


  showPickerStart = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_start'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_start'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

   showPickerRegis = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_at'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_at'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };



   showPickerEnd = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + '_end'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + '_end'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };



  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       that.getToken();
       //that.getKey();
       that.getTokenProv();

        // AsyncStorage.removeItem('@Card:key', (err, result) => {
        //           console.log("Hapus card");
        //         });

        // AsyncStorage.removeItem('@CardDepan:key', (err, result) => {
        //           console.log("Hapus card");
        //         });


        // AsyncStorage.removeItem('@CardBelakang:key', (err, result) => {
        //           console.log("Hapus card");
        //         });
        
        console.log('member' + this.props.memberID)
    }



  componentWillUnmount() {

      this.props.Provinsi.clearItems();
      this.props.Dropdown.clearItems();
      this.props.Rayon.clearItems();
        
    }
    //

    async getMain() {
    this.setState({
      loading: true
    });

    setTimeout(() => {
     this._goToMainScreen();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
      this.setState({
        loading: false,
      });
    }, 2000);
  }
   
///////////

async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getMember(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

 getMember(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loader: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.memberID ;

      var url =  DevApi + 'member/profile/' + ID ;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {

          //const title1 = toJs(result.title1)
          that.setState({
            title1Name: result.title1_name,
            firstname: result.firstname,
            lastname: result.lastname,
            title2Name: result.title2_name,
            alias: result.alias,
            nik: result.nik,
            birthplace: result.birthplace,
            birthday: result.birthday,
            religion: result.religion,
            religions_id: result.religion_id,
            spousename: result.spousename,
            mothername: result.mothername,
            gender: result.gender,
            degreeMem: result.degree,
            jobs: result.occupation,
            marital: result.maritalstatus,
            h_province_id:result.addresses[0].province_id,
            province: result.addresses[0].province,
            h_cities_id:result.addresses[0].regency_id,
            regencyx:result.addresses[0].regency,
            h_districts_id:result.addresses[0].subdistrict_id,
            subdistrictx:result.addresses[0].subdistrict,
            h_villages_id:result.addresses[0].village_id,
            villagex:result.addresses[0].village,
            h_address_1: result.addresses[0].address_1,
            h_address_2: result.addresses[0].address_2,
            h_rtrw: result.addresses[0].rtrw,
            h_postcodes_id: result.addresses[0].postcode,
            h_phone: result.addresses[0].phone,
            mobile: result.mobile,
            //o_province_id:result.addresses[1].province_id,
            provinceOffice: result.addresses[1].province,
            regencyOffice:result.addresses[1].regency,
            subdistrictOffice:result.addresses[1].subdistrict,
            villageOffice:result.addresses[1].village,
            o_address_1: result.addresses[1].address_1,
            o_address_2: result.addresses[1].address_2,
            o_rtrw: result.addresses[1].rtrw,
            o_phone: result.addresses[1].phone,
            o_postcodes_id: result.addresses[1].postcode,
            p_nrp: result.parents.nrp,
            p_name: result.parents.name,
            p_childrens: result.parents.childrens,
            p_religions_id: result.parents.religion_id,
            p_info: result.parents.info,
            militerStat: result.parents.military_status,
            militerUo: result.parents.military_uo,
            p_military_ranks: result.parents.military_rank,
            p_military_position: result.parents.military_position,
            p_religion: result.parents.religion,
            p_Kinship: result.parents.kinship,
            p_childrens: result.parents.childrens,
            g_registered_at: result.organizations.registered_date,
            g_jabatan_start: result.organizations.position_start,
            g_jabatan_end: result.organizations.position_end,
            g_dpd_id: result.organizations.pd_id,
            g_dpw_id: result.organizations.pc_id,
            g_dpra_id: result.organizations.rayon_id,
            pdName: result.organizations.pd,
            pcName: result.organizations.pc,
            rayonName: result.organizations.rayon,
            statusName: result.organizations.status,
            g_members_memberstatus_id:result.organizations.membership_id,
            jabatanOrg: result.organizations.position,
            ktaDepan: result.documents[0].path,
            ktaBelakang: result.documents[1].path,
            fotoMember: result.documents[2].path,
            fotoKtp: result.documents[3].path,
            fotoSkep: result.documents[4].path,
            fotoSkepBack: result.documents[9].path,
            fotoKtaMember: result.documents[5].path,
            fotoKtaMemberBack: result.documents[10].path,
            fotoDoc: result.documents[6].path,
            fotoAkte: result.documents[7].path,
            fotoKK: result.documents[8].path,



            loader: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }

////////


  async getToken() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Dropdown.fetchReligion(value); 
      this.props.Dropdown.fetchTitle1(value);
      this.props.Dropdown.fetchTitle2(value);
      this.props.Dropdown.fetchGender(value);
      this.props.Dropdown.fetchOccupations(value);
      this.props.Dropdown.fetchMartial(value);
      this.props.Dropdown.fetchDegree(value);
      this.props.Dropdown.fetchMilitary(value);
      this.props.Dropdown.fetchKesatuan(value);
      this.props.Dropdown.fetchKinship(value);
      this.props.Dropdown.fetchMembership(value);
      this.props.Dropdown.fetchJabatan(value);
      this.props.Rayon.fetchPd(value);
      this.getMember(value );
      //this.props.Provinsi.fetchProvinsi(value); 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getTokenProv() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchProvinsi(value); 
      this.props.Provinsi.fetchProvinsi(value); 
      //this.props.Provinsi.fetchProvinsi(value); 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getKota(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKota(value, id); 

      this.getCity(value, id) 

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getPc(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchPc(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getRayon(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Rayon.fetchRayon(value, id);  

      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getKecamatan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKecamatan(value, id);  
      //console.log(this.state.myToken)
      this.getCamat(value, id) 
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getCamatan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchKecamatan(value, id);  
      //console.log(this.state.myToken)
      //this.getCamat(value, id) 
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


  async getKelurahan(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      //this.props.Provinsi.fetchKelurahan(value, id); 
      this.getKel(value, id)  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getLurah(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getMabes(value );
      this.props.Provinsi.fetchKelurahan(value, id); 
      //this.getKel(value, id)  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

   async getKodeposx(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getKodepos2(value, id );
      //this.props.Provinsi.fetchKodepos(value, id);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async getKode(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getKodepos(value, id );
      //this.props.Provinsi.fetchKodepos(value, id);  
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }


onCancel = () => {
    this.SectionedMultiSelect._removeAllItems()

    this.setState({
      selectedItems: '',
    })
    console.log(this.state.selectedItems);
  }

   onCancel2 = () => {
    this.SectionedMultiSelect2._removeAllItems()

    this.setState({
      selectedItems: '',
    })
    console.log(this.state.selectedItems);
  }


 /////////

 onSelectedReligion = (key, value) => {
    this.setState({ religion_id: key });

    //console.log(this.state.religion_id)
  };


  // onInputChange(e) {
  //   this.setState({ input: e.target.value }, () => {
  //       console.log(`state: ${this.state}, value: ${e.target.value}`);
  //     });
  // }

  // onSelectedTitle1 = (key, value) => {
  //   this.setState({ title1: key });

  //   //console.log(this.state.religion_id)
  // };

  onSelectedTitle1 = (titlex1) => {

    const Title1 = titlex1.toString();
    this.setState({title1: Title1, titlex1 });
    //console.log(Title1)
  }

   onSelectedTitle2 = (titlex2) => {
    const Title2 = titlex2.toString();
    this.setState({ title2: Title2, titlex2 });

    //console.log(this.state.religion_id)
  };

  onSelectedGender = (sexx) => {
   const Sex = sexx.toString();
    this.setState({ sex: Sex, sexx });

    //console.log(this.state.religion_id)
  };

  onSelectedReligion = (religionsx_id) => {

    const religi = religionsx_id.toString();
    this.setState({ religions_id: religi, religionsx_id });

    //console.log(religi)
  };

  onSelectedReligion2 = (p_religionsx_id) => {
    const p_religi = p_religionsx_id.toString();
    this.setState({ p_religions_id: p_religi, p_religionsx_id });

    //console.log(this.state.religion_id)
  };

   onSelectedDegree = (degreex) => {
     const Degree = degreex.toString();
    this.setState({ degree: Degree, degreex });

    //console.log(this.state.religion_id)
  };

  onSelectedOccu = (occupationsx) => {
    const Occu = occupationsx.toString();
    this.setState({ occupations: Occu, occupationsx });

    //console.log(this.state.religion_id)
  };

   onSelectedMartial = (maritalstatusx) => {
    const Mart = maritalstatusx.toString();
    this.setState({ maritalstatus: Mart, maritalstatusx });

    //console.log(this.state.religion_id)
  };

  onSelectedMilitary = (p_military_statusx_id) => {
    const MilStat = p_military_statusx_id.toString();
    this.setState({ p_military_status_id: MilStat, p_military_statusx_id });

    //console.log(this.state.religion_id)
  };


  onSelectedKesatuan = (p_military_uox_id) => {
     const Kesat = p_military_uox_id.toString();
    this.setState({ p_military_uo_id: Kesat, p_military_uox_id });

    //console.log(this.state.religion_id)
  };

  onSelectedKinship = (p_kinshipx_id) => {
     const Kinsh = p_kinshipx_id.toString();
    this.setState({ p_kinship_id: Kinsh, p_kinshipx_id });

    //console.log(this.state.religion_id)
  };

  onSelectedMembership = (g_members_memberstatusx_id) => {

    const Mem = g_members_memberstatusx_id.toString();
    this.setState({ g_members_memberstatus_id: Mem, g_members_memberstatusx_id });

    //console.log(this.state.religion_id)
  };

  onSelectedJab = (g_jabatanx_id) => {
     const Jab = g_jabatanx_id.toString();
    this.setState({ g_jabatan_id: Jab, g_jabatanx_id });

    //console.log(this.state.religion_id)
  };

  onSelectedProv = (key, value) => {
    this.setState({ h_province_id: key });

    this.getKota(key);
    //this.props.Provinsi.clearItems();

    console.log(this.state.h_province_id)
  };

  onSelectedPd = (g_dpdx_id) => {

    const GPD = g_dpdx_id.toString();
    this.setState({ 
      g_dpd_id: GPD, 
      g_dpdx_id,
      g_dpw_id:'',
      g_dpra_id:'',
      pcName:'Pilih PC',
      rayonName:'Pilih Rayon'

       });

    this.getPc(g_dpdx_id);

    console.log(GPD)
  };

  onSelectedPc = (g_dpwx_id) => {
    const GPC= g_dpwx_id.toString();
    this.setState({ g_dpw_id: GPC, g_dpwx_id });

    this.getRayon(g_dpwx_id);

    //console.log(this.state.g_dpw_id)
  };

  onSelectedRayon = (g_dprax_id) => {
    const GRAYON= g_dprax_id.toString();
    this.setState({ g_dpra_id: GRAYON, g_dprax_id });
  };

  onSelectedProv2 = (o_provincex_id) => {
    const ProvOf = o_provincex_id.toString();
    this.setState({ 
      o_province_id: ProvOf, 
      o_provincex_id,
      o_cities_id:'',
      o_districts_id:'',
      o_villages_id:'',
      regencyOffice:'Pilih Kota / Kabupaten',
      subdistrictOffice:'Pilih Kecamatan',
      villageOffice:'Pilih Kelurahan'



    });

    this.getKota(o_provincex_id);

    //console.log(this.state.religion_id)
  };

   onSelectedKota = (key, value) => {
    this.setState({ h_cities_id: key });

    this.getKecamatan(key);

    //console.log(this.state.religion_id)
  };

  onSelectedKota2 = (o_citiesx_id) => {
     const CityOf = o_citiesx_id.toString();

    this.setState({ o_cities_id: CityOf, o_citiesx_id });

    this.getCamatan(o_citiesx_id);

    //console.log(this.state.religion_id)
  };

   onSelectedKec = (key, value) => {
    this.setState({ h_districts_id: key });

    this.getKelurahan(key);

    //console.log(this.state.religion_id)
  };

  onSelectedKec2 = (o_districtsx_id) => {
      const KecOf = o_districtsx_id.toString();
    this.setState({ o_districts_id: KecOf, o_districtsx_id });

    this.getLurah(o_districtsx_id);

    //console.log(this.state.religion_id)
  };

  onSelectedKel = (h_villagesx_id) => {

      const Kell = h_villagesx_id.toString();
    this.setState({ h_villages_id: Kell, h_villagesx_id });

    this.getKodeposx(h_villagesx_id);

    //console.log(this.state.religion_id)
  };

  onSelectedKel2 = (o_villagesx_id) => {
     const Kell2 = o_villagesx_id.toString();
    this.setState({ o_villages_id: Kell2, o_villagesx_id });

    this.getKode(o_villagesx_id);

    //console.log(this.state.religion_id)
  };

 ////////////

selectPhotoKtaFront() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktaFront: file
        });

        console.log(this.state.ktaFront)
      }
    });
  }
 /////////////////
 selectPhotoKtaBack() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktaBack: file
        });

        console.log(this.state.ktaBack)
      }
    });
  }

  /////////

  getPos(value, id) {
      // Set loading to true to display a Spinner
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'list/postcode/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
         //  that.setState({
         //    h_postcodes_id: result.postcode,
         // });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }

  getKodepos(value, id) {
      // Set loading to true to display a Spinner
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'list/postcode/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            o_postcodes_id: result.postcode,
         });

          //console.log(result.postcode)

        })
        .catch((error) => { console.error(error); });
  }

  getKodepos2(value, id) {
      // Set loading to true to display a Spinner
  

      //console.log(this.state.myToken)
      
      
      var url =  DevApi + 'list/postcode/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            h_postcodes_id: result.postcode,
         });

          console.log(result.postcode)

        })
        .catch((error) => { console.error(error); });
  }


  /////////

  getCity(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/regency/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            city: result,
         });

          console.log(that.state.city)

        })
        .catch((error) => { console.error(error); });
  }

  /////////////////

  getCamat(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/subdistrict/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            district: result,
         });

          console.log(that.state.district)

        })
        .catch((error) => { console.error(error); });
  }
/////////////////////////
   getKel(value, id) {
      // Set loading to true to display a Spinner
      //console.log(this.state.myToken)    
      
      var url =  DevApi + 'list/village/' + id;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            village: result,
         });

          console.log(that.state.village)

        })
        .catch((error) => { console.error(error); });
  }

  /////////////////
 selectPhotoFoto() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          foto: file
        });

        console.log(this.state.foto)
      }
    });
  }

  /////////////////
 selectPhotoKtp() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ktp: file
        });

        console.log(this.state.ktp)
      }
    });
  }

   /////////////////
 selectPhotoSkep() {
    const options = {
      quality: 1.0,
      maxWidth: 800,
      maxHeight: 800,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          skep_pensiun: file
        });

        console.log(this.state.skep_pensiun)
      }
    });
  }
  ///
  selectPhotoSkepBack() {
    const options = {
      quality: 1.0,
      maxWidth: 800,
      maxHeight: 800,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          skep_pensiun_back: file
        });

        console.log(this.state.skep_pensiun_back)
      }
    });
  }

 /////////////////
 selectPhotoKtaPolri() {
    const options = {
      quality: 1.0,
      maxWidth: 800,
      maxHeight: 800,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          kta_tnipolri: file
        });

        console.log(this.state.kta_tnipolri)
      }
    });
  }

 /////////////////
 selectPhotoKtaPolriBack() {
    const options = {
      quality: 1.0,
      maxWidth: 800,
      maxHeight: 800,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          kta_tnipolri_back: file
        });

        console.log(this.state.kta_tnipolri_back)
      }
    });
  }

/////////////////
 selectPhotoDok() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          dok_tambahan: file
        });

        console.log(this.state.dok_tambahan)
      }
    });
  }
  ///

  /////////////////
 selectPhotoAkta() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          aktalahir: file
        });

        console.log(this.state.aktalahir)
      }
    });
  }
  ///

   /////////////////
 selectPhotoKK() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response };

        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
    

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          kk: file
        });

        console.log(this.state.kk)
      }
    });
  }
  ///

  onPress = () => {
    this.getKey();
  }


  async getKeyx() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getNews(value ); 
      this.onSubmit(value );     
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 

  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });

    console.log(this.state.selected2)
  }


  onSubmit(value){
  const FormData = require('form-data');
   // const fetch = require('node-fetch');

   //  var data = new FormData();

   //  data.append('title1', 'Dr');
   //  data.append('firstname', 'Bruce');
   //  data.append('lastname', 'Banner');
  

   //   console.log(data)
   //   var url =  DevApi + 'member/register';
     // fetch(url, { 
     //     method: 'POST', 
     //    headers: {
     //            'Accept': 'application/json',
     //             'Content-Type': 'multipart/form-data',
     //            'Authorization': value 
     //          },
     //     body: data, 
     //   })
     //  .then(function(res) {
     //      return res.json();
     //  }).then(function(json) {
     //      console.log(json);
     //  });

    //  let xhr = new XMLHttpRequest();
    // xhr.open('POST', url);
    // let formdata = new FormData();
    // // image from CameraRoll.getPhotos(
    // formdata.append('title1', 'Dr');
    // formdata.append('firstname', 'Bruce');
    // formdata.append('lastname', 'Banner');
    // xhr.send(formdata);
    var url =  DevApi + 'member/edit/' + this.props.memberID;
    var _this = this;
    var formData = new FormData();
    formData.append('title1', this.state.title1);
    formData.append('title2', this.state.title2);
    formData.append('firstname', this.state.firstname);
    formData.append('lastname', this.state.lastname);
    formData.append('email', null);
    formData.append('alias', this.state.alias);
    formData.append('nik', this.state.nik);
    formData.append('birthplace', this.state.birthplace);
    formData.append('birthday', this.state.birthday);
    formData.append('sex', this.state.sex);
    formData.append('religions_id', this.state.religions_id);
    formData.append('degree', this.state.degree);
    formData.append('occupations', this.state.occupations);
    formData.append('maritalstatus', this.state.maritalstatus);
    formData.append('spousename', this.state.spousename);
    formData.append('mothername', this.state.mothername);
    formData.append('h_province_id', this.state.h_province_id);
    formData.append('h_cities_id', this.state.h_cities_id);
    formData.append('h_districts_id', this.state.h_districts_id);
    formData.append('h_villages_id', this.state.h_villages_id);
    formData.append('h_address_1', this.state.h_address_1);
    formData.append('h_address_2', this.state.h_address_2);
    formData.append('h_rtrw', this.state.h_rtrw);
    formData.append('h_postcodes_id', this.state.h_postcodes_id);
    formData.append('h_phone', this.state.h_phone);
    formData.append('mobile', this.state.mobile);
    formData.append('o_province_id', this.state.o_province_id);
    formData.append('o_cities_id', this.state.o_cities_id);
    formData.append('o_districts_id', this.state.o_districts_id);
    formData.append('o_villages_id', this.state.o_villages_id);
    formData.append('o_address_1', this.state.o_address_1);
    formData.append('o_address_2', this.state.o_address_2);
    formData.append('o_rtrw', this.state.o_rtrw);
    formData.append('o_postcodes_id', this.state.o_postcodes_id);
    formData.append('o_phone', this.state.o_phone);
    formData.append('p_nrp', this.state.p_nrp);
    formData.append('p_name', this.state.p_name);
    formData.append('p_military_status_id', this.state.p_military_status_id);
    formData.append('p_military_uo_id', this.state.p_military_uo_id);
    formData.append('p_military_ranks', this.state.p_military_ranks);
    formData.append('p_military_position', this.state.p_military_position);
    formData.append('p_childrens', this.state.p_childrens);
    formData.append('p_religions_id', this.state.p_religions_id);
    formData.append('p_kinship_id', this.state.p_kinship_id);
    formData.append('p_info', this.state.p_info);
    formData.append('g_members_memberstatus_id', this.state.g_members_memberstatus_id);
    formData.append('g_registered_at', this.state.g_registered_at);
    formData.append('g_dpd_id', this.state.g_dpd_id);
    formData.append('g_dpw_id', this.state.g_dpw_id);
    formData.append('g_dpra_id', this.state.g_dpra_id);
    formData.append('g_jabatan_id', this.state.g_jabatan_id);
    formData.append('g_jabatan_start', this.state.g_jabatan_start);
    formData.append('g_jabatan_end', this.state.g_jabatan_end);
    formData.append('d_kta_front', this.state.ktaFront);
    formData.append('d_kta_back', this.state.ktaBack);
    formData.append('d_foto', this.state.foto);
    formData.append('d_ktp', this.state.ktp);
    formData.append('d_skep_pensiun', this.state.skep_pensiun);
    formData.append('d_skep_pensiun_back', this.state.skep_pensiun_back);
    formData.append('d_kta_tnipolri', this.state.kta_tnipolri);
    formData.append('d_kta_tnipolri_back', this.state.kta_tnipolri_back);
    formData.append('d_dok_tambahan', this.state.dok_tambahan);
    formData.append('d_aktalahir', this.state.aktalahir);
    formData.append('d_kk', this.state.kk);

    console.log(formData)

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    //xhr.responseType = 'json';
    console.log('OPENED', xhr.status);

    xhr.onprogress = function () {
      console.log('LOADING', xhr.status);
    };

    // xhr.onload = function () {
    //     console.log('DONE', xhr.status);
    //      setTimeout(() => {
    //       _this.openModal();
    //       _this.setState({
    //         loading: false,
    //       });
    //     }, 1500);
    // };

    xhr.onload = function(e) {
      if (this.status == 200) {

        setTimeout(() => {
          _this.openModal();
          _this.setState({
            loading: false,
          });
        }, 1500);
  

        /////////////
        //Alert.alert("Berhasil Terdaftar");
        //console.log('response', this.response.printedcard); // JSON response  
      }else{

        ToastAndroid.show('Ubah Data Gagal', ToastAndroid.SHORT)
        _this.setState({
            loading: false,
          });
      }
    };
    
    xhr.setRequestHeader('Authorization', value);
    xhr.setRequestHeader('Content-Type', 'multipart/form-data');
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send(formData);


  }


//   getDownload(value){


//   var date      = new Date();
//     var url       = this.props.print;
//     var ext       = this.extention(url);
//     ext = "."+ext[0];
//     const { config, fs } = RNFetchBlob
//     let PictureDir = fs.dirs.PictureDir
//     let options = {
//       fileCache: true,
//       addAndroidDownloads : {
//         useDownloadManager : true,
//         notification : true,
//         path:  PictureDir + "/image_"+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
//         description : 'Image'
//       }
//     }
//     config(options).fetch('GET', url).then((res) => {
//       Alert.alert("Success Downloaded");
//     });
//   }
// extention(filename){
//     return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;


// //////////end
//   }
////////////////////  render


// renderPasangan(){
   
//     if(this.state.spousename === null){


    
//       return(
//               <Item regular>
//                           <Input 
//                           placeholder=''
//                           keyboardType="default"
//                           returnKeyType="next"
//                           value={this.state.spousename} 
//                           onChangeText={(spousename) => this.setState({spousename})}
//                           />
//                         </Item>

//         )
//     }else{
      
//     
// }


renderMain(){

  const { h_province_id, o_province_id} = this.state;
    const {Dropdown, Provinsi, Rayon} = this.props;
    const store = this.props.Dropdown;
    const prov = this.props.Provinsi;
    const ray = this.props.Rayon;

    const listCity = this.state.city || []
    const listCamat = this.state.district || []
    const listKel = this.state.village || []
    //const inList = this.props.data.allInterests || []


    console.log(listCity)

    const listTitle1 = toJS(store.title1)
    const listTitle2 = toJS(store.title2)
    const listGender = toJS(store.gender)
    const listReligi = toJS(store.religions)
    const listDegree = toJS(store.degree)
    const listOccu = toJS(store.occupations)
    const listMartial = toJS(store.maritalstatus)
    const listMilitary = toJS(store.militarystatus)
    const listKesatuan = toJS(store.kesatuan)
    const listKinship = toJS(store.kinship)
    const listMembership = toJS(store.membership)
    const listJabatan = toJS(store.jabatan)

    const listProvinsi = toJS(prov.provinsi)
    const listKota = toJS(prov.kota)
    const listKecamatan = toJS(prov.kecamatan)
    const listKelurahan = toJS(prov.kelurahan)

    const listPd = toJS(ray.pd)
    //console.log(listPd)
    const listPc = toJS(ray.pc)
    const listRayon = toJS(ray.rayon)

    const KODEPOS = this.state.h_postcodes_id;

 if (this.state.loader) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" />
             </View>)
    }else{


     return(


       <Tabs 
         renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#404f3d" }}/>}>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Personal</Text>
              </TabHeading>}>

              <ScrollView>

                  <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form> 
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Gelar Depan</Label>
                         <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listTitle1.map((category) => (
                           
                           {name: category.title, id: category.id,}


                          ))}
                          uniqueKey='id'
                          ref={SectionedMultiSelect => this.SectionedMultiSelect = SectionedMultiSelect}
                           onCancel={this.onCancel}
                          showCancelButton
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.title1Name}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedTitle1.bind(this)}
                          selectedItems={this.state.titlex1}
                        />
                        </View>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Depan</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.firstname} 
                          onChangeText={(firstname) => this.setState({firstname})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Belakang</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.lastname} 
                          onChangeText={(lastname) => this.setState({lastname})}
                          />
                        </Item>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Gelar Belakang</Label>
                         <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listTitle2.map((category) => (
                           
                           {name: category.title, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          ref={SectionedMultiSelect2 => this.SectionedMultiSelect2 = SectionedMultiSelect2}
                           onCancel={this.onCancel2}
                          showCancelButton
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.title2Name}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedTitle2.bind(this)}
                          selectedItems={this.state.titlex2}
                        />
                        </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Alias</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.alias == 'null' ? '' : this.state.alias} 
                          onChangeText={(alias) => this.setState({alias})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor KTP / Kartu Pelajar</Label>
                        <Item regular disabled>
                          <Input 
                         
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.nik} 
                          onChangeText={(nik) => this.setState({nik})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Tempat Lahir</Label>
                        <Item regular>
                          <Input
                         
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.birthplace} 
                          onChangeText={(birthplace) => this.setState({birthplace})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Tanggal Lahir</Label>
                       

                        <Item regular>
                          <DatePicker
                            style={{width: '100%'}}
                            date={this.state.birthday}
                            mode="date"
                            placeholder="Isi Tanggal Lahir"
                            format="YYYY-MM-DD"
                            minDate="1945-12-31"
                            maxDate="2018-12-31"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateIcon: {
                                position: 'absolute',
                                right: 0,
                                top: 4,
                                marginLeft: 0
                              },
                             dateText:{
                              textAlign:'left',
                              marginLeft: 5
                             },
                             placeholderText:{
                                marginLeft: 5
                             },
                              dateInput: {
                                marginLeft: 0,
                                borderWidth: 0,
                                alignItems: 'flex-start',
                                height:50 
                              }
                              // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(birthday) => {this.setState({birthday: birthday})}}
                          />
                        </Item>

                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jenis Kelamin</Label>
                  

                        
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Agama</Label>
                         <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listReligi.map((category) => (
                           
                           {name: category.religion, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.religion}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedReligion.bind(this)}
                          selectedItems={this.state.religionsx_id}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pendidikan Terakhir</Label>
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listDegree.map((category) => (
                           
                           {name: category.degree, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.degreeMem}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedDegree.bind(this)}
                          selectedItems={this.state.degreex}
                        />
                        </View>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pekerjaan</Label>
                         <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listOccu.map((category) => (
                           
                           {name: category.occupation, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.jobs}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedOccu.bind(this)}
                          selectedItems={this.state.occupationsx}
                        />
                       </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Status Perkawinan</Label>
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listMartial.map((category) => (
                           
                           {name: category.status, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.marital}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedMartial.bind(this)}
                          selectedItems={this.state.maritalstatusx}
                        />
                        </View>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Pasangan</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.spousename == 'null' ? '' : this.state.spousename} 
                          onChangeText={(spousename) => this.setState({spousename})}
                          />
                        </Item>

                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Gadis Ibu Kandung </Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                         
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.mothername == 'null' ? '' : this.state.mothername} 
                          onChangeText={(mothername) => this.setState({mothername})}
                          />
                        </Item>

                     </Form>

                 </Content>

              </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Alamat Rumah</Text>
              </TabHeading>}>

              <ScrollView>

                 <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Provinsi</Label>
                   <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                   <SectionedMultiSelect
                          items={this.props.Provinsi.provinsi.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.province}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedProvinsi}
                          selectedItems={this.state.provinsi}
                        />
                 </View>
                 <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kota / Kabupaten</Label>
           
                  <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={listCity.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.regencyx}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKotax}
                          selectedItems={this.state.kotax}
                        />
                  </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kecamatan</Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                  <SectionedMultiSelect
                          items={listCamat.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.subdistrictx}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKecx}
                          selectedItems={this.state.kec}
                        />
                  </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kelurahan</Label>
           <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                 <SectionedMultiSelect
                          items={listKel.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.villagex}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKel}
                          selectedItems={this.state.h_villagesx_id}
                        />
                  </View>
                   <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Alamat Lengkap</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          maxLength={50}

                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.h_address_1 == 'null' ? '' : this.state.h_address_1} 
                          onChangeText={(h_address_1) => this.setState({h_address_1})}
                          />
                        </Item>

                  
                    
                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>RT / RW sesuai KTP</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.h_rtrw == 'null' ? '' : this.state.h_rtrw} 
                          onChangeText={(h_rtrw) => this.setState({h_rtrw})}
                          />
                        </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>KodePos</Label>
                        <Item regular>
                       

                          <TextInput
                          placeholder=''
                          style={CommonStyles.textInputLeft}
                          underlineColorAndroid='transparent'
                          keyboardType="numeric"
                          returnKeyType="next"
                          onChangeText={(h_postcodes_id) => this.setState({h_postcodes_id})}
                          value={this.state.h_postcodes_id}
                        />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Telepon</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.h_phone == 'null' ? '' : this.state.h_phone} 
                          onChangeText={(h_phone) => this.setState({h_phone})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Handphone</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.mobile == 'null' ? '' : this.state.mobile} 
                          onChangeText={(mobile) => this.setState({mobile})}
                          />
                        </Item>

                </Form>

                </Content> 
             </ScrollView>
             
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Alamat Kantor</Text>
              </TabHeading>}>

              <ScrollView>

                 <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                     <Form>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Provinsi</Label>
                    
                  <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={this.props.Provinsi.provinsi.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.provinceOffice}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedProv2.bind(this)}
                          selectedItems={this.state.o_provincex_id}
                        />
                    </View>
                 <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kota / Kabupaten</Label>
           
                  
                     <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listCity.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                         confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.regencyOffice}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKota2.bind(this)}
                          selectedItems={this.state.o_citiesx_id}
                        />
                      </View>

                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kecamatan</Label>
           
                  <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                     <SectionedMultiSelect
                          items={listKecamatan.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                         confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.subdistrictOffice}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKec2.bind(this)}
                          selectedItems={this.state.o_districtsx_id}
                        />
                      </View>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kelurahan</Label>
                <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                    <SectionedMultiSelect
                          items={listKelurahan.map((category) => (
                           
                           {name: category.name, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loading
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                         confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.villageOffice}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedKel2.bind(this)}
                          selectedItems={this.state.o_villagesx_id}
                        />
                    </View>
                   <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Alamat Kantor Lengkap</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          maxLength={28}
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.o_address_1 == 'null' ? '' : this.state.o_address_1} 
                          onChangeText={(o_address_1) => this.setState({o_address_1})}
                          />
                        </Item>

                 
                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>RT / RW Kantor</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.o_rtrw == 'null' ? '' : this.state.o_rtrw} 
                          onChangeText={(o_rtrw) => this.setState({o_rtrw})}
                          />
                        </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>KodePos</Label>
                        <Item regular>
                      
                          <TextInput
                          placeholder=''
                          style={CommonStyles.textInputLeft}
                          underlineColorAndroid='transparent'
                          keyboardType="numeric"
                          returnKeyType="next"
                          onChangeText={(o_postcodes_id) => this.setState({o_postcodes_id})}
                          value={this.state.o_postcodes_id}
                        />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nomor Telepon Kantor</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.o_phone == 'null' ? '' : this.state.o_phone} 
                          onChangeText={(o_phone) => this.setState({o_phone})}
                          />
                        </Item>
                       
                      

                </Form>


                </Content> 
             </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Orang Tua</Text>
              </TabHeading>}>
              <ScrollView>
                <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                  <Form>
                    <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>N.R.P/N.I.P Orang Tua</Label>
                        <Item regular>
                          <Input
                         
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_nrp} 
                          onChangeText={(p_nrp) => this.setState({p_nrp})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Nama Orang Tua</Label>
                        <Item regular>
                          <Input 
                          disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_name} 
                          onChangeText={(p_name) => this.setState({p_name})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Status Kemiliteran Orang Tua</Label>
                   
            

                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>  
                        <SectionedMultiSelect
                          items={listMilitary.map((category) => (
                           
                           {name: category.status, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.militerStat} 
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedMilitary.bind(this)}
                          selectedItems={this.state.p_military_statusx_id}
                        />
                       </View>
                          <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Kesatuan Dinas Orang Tua</Label>
                   
              
                       <Item regular>
                          <Input 
                          disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.militerUo} 
                          onChangeText={(p_name) => this.setState({p_name})}
                          />
                        </Item>


                          <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Pangkat Terakhir Orang Tua</Label>
                        <Item regular>
                          <Input 
                       
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_military_ranks} 
                          onChangeText={(p_military_ranks) => this.setState({p_military_ranks})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jabatan Terakhir Orang Tua</Label>
                        <Item regular>
                          <Input 

                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_military_position} 
                          onChangeText={(p_military_position) => this.setState({p_military_position})}
                          />
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jumlah Anak Orang Tua</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_childrens} 
                          onChangeText={(p_childrens) => this.setState({p_childrens})}
                          />
                        </Item>
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Agama Orang Tua</Label>
           
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                         <SectionedMultiSelect
                          items={listReligi.map((category) => (
                           
                           {name: category.religion, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.p_religion}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedReligion2.bind(this)}
                          selectedItems={this.state.p_religionsx_id}
                        />
                        </View>

                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Hubungan Orang Tua dengan anggota</Label>
           
                  <Item regular>
                          <Input 
                          disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_Kinship} 
                          onChangeText={(p_name) => this.setState({p_name})}
                          />
                        </Item>
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Keterangan Lainnya</Label>
                        <Item regular>
                          <Input 
                          placeholder=''
                          keyboardType="default"
                          returnKeyType="next"
                          value={this.state.p_info == 'null' ? '' : this.state.p_info} 
                          onChangeText={(p_info) => this.setState({p_info})}
                          />
                        </Item>

                  </Form>

                  </Content> 
               </ScrollView>
            
          </Tab>

          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Data Organisasi</Text>
              </TabHeading>}>

              <ScrollView>
               <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
                  <Form>
                  
                  <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Keanggotaan Sejak</Label>
                        <Item regular>
                          <Input 
                          
                          placeholder=''
                          keyboardType="numeric"
                          returnKeyType="next"
                          value={this.state.g_registered_at} 
                          onChangeText={(g_registered_at) => this.setState({g_registered_at})}
                          />
                        
                        </Item>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PD saat ini</Label>
           
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}> 
                        <SectionedMultiSelect
                          items={listPd.map((category) => (
                           
                           {name: category.pd, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                         confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.pdName}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPd.bind(this)}
                          selectedItems={this.state.g_dpdx_id}
                        />
                        </View>
                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>PC saat ini </Label>
           
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listPc.map((category) => (
                           
                           {name: category.pc, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                         confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.pcName}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedPc.bind(this)}
                          selectedItems={this.state.g_dpwx_id}
                        />
                        </View>

                        <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Rayon saat ini </Label>
           
                       <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}> 
                        <SectionedMultiSelect
                          items={listRayon.map((category) => (
                           
                           {name: category.rayon, id: category.id,}


                          ))}
                          loadingComponent={
                            <Loadingx
                              hasErrored={this.state.hasErrored}
                              
                            />
                          }
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.rayonName}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedRayon.bind(this)}
                          selectedItems={this.state.g_dprax_id}
                        />
                       </View>
                        
                         <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Jabatan</Label>
           
                        <View style={{height:45, backgroundColor:'#fff', borderColor:'#e2e2e1', borderWidth:1, paddingTop:2,}}>
                        <SectionedMultiSelect
                          items={listJabatan.map((category) => (
                           
                           {name: category.position, id: category.id,}


                          ))}
                          uniqueKey='id'
                          single={true}
                          confirmText="Pilih"
                          searchPlaceholderText="Cari"
                          colors={{ primary: '#404f3d',}}
                          selectChildren={true}
                          subKey='parent'
                          style={{fontSize:8}}
                          selectText={this.state.jabatanOrg}
                          showDropDowns={true}
                          readOnlyHeadings={false}
                          onSelectedItemsChange={this.onSelectedJab.bind(this)}
                          selectedItems={this.state.g_jabatanx_id}
                        />
                       </View>
                        
                        
                  </Form>
               </Content>
              </ScrollView>
            
          </Tab>
          <Tab heading={ 
              <TabHeading style={{ backgroundColor: "#404f3d" }}>
                  <Text style={{ color:'#fff' }}>Dokumen</Text>
              </TabHeading>}>

               <ScrollView>
                  <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>

                 

                <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Foto KTA</Label>
                        <Item regular>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='Tampak Depan'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.ktaFront === null ? <Button   onPress={this.selectPhotoKtaFront.bind(this)}  style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtaFront.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktaFront} />
                              </TouchableWithoutFeedback>
                            }
                          
        
                          <Image style={{width:50, height:50}} source={{uri: this.state.ktaDepan}} />
                      </Item>

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='Tampak Belakang'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.ktaBack === null ? <Button   onPress={this.selectPhotoKtaBack.bind(this)}  style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtaBack.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktaBack} />
                              </TouchableWithoutFeedback>
                            }
                          <Image style={{width:50, height:50}} source={{uri: this.state.ktaBelakang}} />
                      </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Dokumen Anggota</Label>

                      <Item regular>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='Foto Anggota'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.foto === null ? <Button onPress={this.selectPhotoFoto.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoFoto.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.foto} />
                             </TouchableWithoutFeedback>
                            }
                            <Image style={{width:50, height:50}} source={{uri: this.state.fotoMember}} />
                      </Item>

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='KTP / Kartu Pelajar'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.ktp === null ? <Button onPress={this.selectPhotoKtp.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtp.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.ktp} />
                              </TouchableWithoutFeedback>
                            }
                          <Image style={{width:50, height:50}} source={{uri: this.state.fotoKtp}} />
                      </Item>

                       <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Dokumen Orang Tua Pilih Salah Satu Skep / KTA</Label>
                      <Item regular>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='SKEP Pensiun Tampak Depan'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.skep_pensiun === null ? <Button  onPress={this.selectPhotoSkep.bind(this)}  style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                              <TouchableWithoutFeedback onPress={this.selectPhotoSkep.bind(this)}>
                               <Image style={{width:50, height:50}} source={this.state.skep_pensiun} />
                              </TouchableWithoutFeedback>
                            }
                            <Image style={{width:50, height:50}} source={{uri: this.state.fotoSkep}} />
                      </Item>

                        <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='SKEP Pensiun Tampak Belakang'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.skep_pensiun_back === null ? <Button  onPress={this.selectPhotoSkepBack.bind(this)}  style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                              <TouchableWithoutFeedback onPress={this.selectPhotoSkepBack.bind(this)}>
                               <Image style={{width:50, height:50}} source={this.state.skep_pensiun_back} />
                              </TouchableWithoutFeedback>
                            }
                            <Image style={{width:50, height:50}} source={{uri: this.state.fotoSkepBack}} />
                      </Item>
 
                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='KTA TNI / POLRI Orang Tua Tampak Depan'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.kta_tnipolri === null ? <Button onPress={this.selectPhotoKtaPolri.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtaPolri.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.kta_tnipolri} />
                            </TouchableWithoutFeedback>
                            }
                          
                          <Image style={{width:50, height:50}} source={{uri: this.state.fotoKtaMember}} />
                      </Item>

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='KTA TNI / POLRI Orang Tua Tampak Belakang'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.kta_tnipolri_back === null ? <Button onPress={this.selectPhotoKtaPolriBack.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                             <TouchableWithoutFeedback onPress={this.selectPhotoKtaPolriBack.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.kta_tnipolri_back} />
                            </TouchableWithoutFeedback>
                            }
                          
                          <Image style={{width:50, height:50}} source={{uri: this.state.fotoKtaMemberBack}} />
                      </Item>

                      

                      <Item regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='Keterangan Lainnya'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
      
                          />
                          
                          { this.state.dok_tambahan === null ? <Button onPress={this.selectPhotoDok.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                            <TouchableWithoutFeedback onPress={this.selectPhotoDok.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.dok_tambahan} />
                            </TouchableWithoutFeedback>
                            }
                          <Image style={{width:50, height:50}} source={{uri: this.state.fotoDoc}} />
                      </Item>

                      <Label style={{color:'#010101', fontSize:12, marginTop:10, marginBottom:10, fontFamily:'Roboto-Regular'}}>Dokumen Hubungan</Label>

                     <Item regular>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='Akte Kelahiran'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.aktalahir === null ? <Button  onPress={this.selectPhotoAkta.bind(this)}  style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                            <TouchableWithoutFeedback onPress={this.selectPhotoAkta.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.aktalahir} />
                             </TouchableWithoutFeedback>
                            }
                            <Image style={{width:50, height:50}} source={{uri: this.state.fotoAkte}} />
                      </Item>

                      <Item regular regular style={{marginTop:10}}>
                          <Input disabled
                          style={{backgroundColor:'#f9f9f9'}}
                          placeholder='KK / Keterangan Hubungan Darah'
                          keyboardType="default"
                          style={{fontSize:12}}
                          placeholderStyle={{ color: "#bfc6ea", fontSize:10 }}
                          returnKeyType="next"
            
                          />
                           { this.state.kk === null ? <Button onPress={this.selectPhotoKK.bind(this)}   style={{backgroundColor:'#404f3d',width:50, height:50,justifyContent:'center' }}>
                            <Icon name='camera' style={{fontSize: 16, color: '#fff', }}/>
                          </Button>:
                            <TouchableWithoutFeedback onPress={this.selectPhotoKK.bind(this)}>
                              <Image style={{width:50, height:50}} source={this.state.kk} />
                             </TouchableWithoutFeedback>
                            }
                            <Image style={{width:50, height:50}} source={{uri: this.state.fotoKK}} />
                      </Item>



              <View>
        
        
                 <TouchableWithoutFeedback onPress={() => this.getSubmit()}>
                  <View style={[CommonStyles.buttonFieldYellow2x, styles.buttonBox, {marginBottom: spaceHeight * 0.1}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Ubah Data
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

                  </Content>
               </ScrollView>
            
          </Tab>
        </Tabs>




     )


    }


}
 
  render() {
    
    
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       <Loader
          loading={this.state.loading} />
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>
         
         <View style={{flex:1, flexDirection:'row'}}>
          
          <View style={{marginTop:40, marginLeft:10}}>
             <Image
              source={{uri: this.props.avatar}}
              style={{width: 40, height: 40,borderRadius: 40,borderColor:'#ccc',borderWidth:4}} />
          </View>

          <View style={{marginTop:40, marginLeft:10}}>
            <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.itemHeaderText,
              CommonStyles.whiteColor]}>{this.props.firstName} {this.props.lastName}</Text>
             <Text style={[ CommonStyles.normalText,CommonStyles.blackColor]}>
                Edit Pendaftaran
              </Text>
          </View>
 

         </View>

         
        </View>
       
        <View style={{flex: 5, justifyContent: 'center', backgroundColor:'#000' }}>
          <Container>
         {this.renderMain()}
      </Container>

       </View>

      

        
      </View>
      <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Sukses
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Data Telah Terupdate. data anda akan di verifikasi terlebih dahulu dan akan menerima email lanjutan dari kami

          </Text>

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.getMain()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>OK</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </ImageBackground>
    )
  }

//

_goToMainScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }

  _handleClickNotificationButton() {
    this.props.navigator.push({
      screen: "Healer.NotificationScreen",
      animated: true,
    });
  }


}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  bg:{
    backgroundColor:'#404f3d',
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
 titleBox: {
   justifyContent: 'flex-start'
  },
});
