import React, {
  Component
} from 'react';
import FitImage from 'react-native-fit-image';
import AutoHeightImage from 'react-native-auto-height-image';
import ResponsiveImage from 'react-native-responsive-image';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Image,
  Platform,
  StatusBar,
  AsyncStorage,
  ImageBackground
} from 'react-native';

import {Navigation} from 'react-native-navigation';
import CommonStyles, { deviceHeight,shadowOpt,deviceWidth } from '../styles/CommonStyles';
import { appSingleNavigation } from '../styles/navigatorStyle';

 const dt = new Date();

 const years = dt.getFullYear();

export default class StartUpScreen extends Component {
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
  }
  
  componentDidMount() {
       StatusBar.setHidden(true);
      
    }
  render() {
    setTimeout(
      () => {
        Navigation.startSingleScreenApp({
          screen: {
            screen: 'Fkppi.StartUpLogoScreen'
          },
          animated: true,
          animationType: 'slide-up',
          portraitOnlyMode: true,
           appStyle: {
            orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
          },
        });
      }, 3000
    );

    return (
      <ImageBackground
      source={require('../../img/fkppi/bg.png')}
      imageStyle={{resizeMode: 'cover'}}
      style={{width: '100%', height: '100%'}}
    >
    <StatusBar
         backgroundColor="#857d60"
         barStyle="light-content"
         hidden={true}

       />
      <View style={styles.container}>
        
        <View style={[{flex:1}, styles.elementsContainer]}>
          <View style={{flex: 6, justifyContent: 'center',flexDirection: 'column'}} >
            <View style={[styles.logoBox,{height: 198, width: 137}]}>
             
              <AutoHeightImage
                width={deviceWidth/3}
                source={require('../../img/fkppi/logo.png')}
              />
            </View>
           <View style={styles.textBox}>
                <Text style={styles.textItem}>
                 Keluarga Besar Forum Komunikasi Putra Putri Purnawirawan dan Putra Putri TNI POLRI
                </Text>
           </View>
          </View>
          <View style={{flex:1, justifyContent: 'flex-end',flexDirection: 'row'}} >

           <View style={styles.footerBox}>
                <Text style={styles.textFooter}>
                 ©{years} FKPPI all rights reserved.
                </Text>
           </View>

          </View>
        </View>
      </View>
    </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      },
    elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center'
    },
    textBox:{
      marginLeft: 24,
      marginRight: 24,

    },
    textItem:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Medium',fontSize: deviceWidth * 0.038 ,lineHeight: 23
    },
    footerBox:{
      flex: 1,
      width: 40,
      height: 40,
      alignSelf:'center'
    },
    textFooter:{
      
      color:'#fff', textAlign:'center', marginTop: 20,  fontFamily: 'Roboto-Light',fontSize: deviceWidth * 0.028,lineHeight: 23
    },
});