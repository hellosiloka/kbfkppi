import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
  ImageBackground,
  ScrollView,
  ToastAndroid,
   Dimensions,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ActivityIndicator,
    AsyncStorage,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';
import GridView from 'react-native-super-grid';

import CommonStyles, {
  deviceHeight,
   deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import HTML from 'react-native-render-html';
import { appSingleNavigation } from '../styles/navigatorStyle';
import Modal from "react-native-simple-modal";

import moment from 'moment';
import 'moment/locale/id';
moment.locale('id');


export default class NewsDetailScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);

    this. state = {
      data: {},
      myToken: '',
      loading: true,
      animating: true
    }
    
   
  }
  
  componentDidMount() {
       StatusBar.setHidden(true);
       //this.closeActivityIndicator();
       var that = this;
       that.getKey();
        
    }

 /////////

  async getLike() {

   this.getKeyx(this.props.newsId);
   console.log('like' + ' ' + this.props.newsId );

  }


  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.getDetail(value );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

   async getKeyx(id) {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      this.likes(value, id );    
      //console.log(this.state.myToken)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  /////

  likes(value, id){


       const FETCH_TIMEOUT = 2000;
       let didTimeOut = false;

        var url =  DevApi + 'news/like/' + id;
        var that = this;
        new Promise(function(resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            //reject(new Error('Request timed out'));
            ToastAndroid.show('Koneksi Putus', ToastAndroid.SHORT)
            that.setState({
                          loading: false

                        });
            console.log('Koneksi Putus');
        }, FETCH_TIMEOUT);
        
        fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 

              }
        })

        .then((response) => {

           clearTimeout(timeout);

           if(!didTimeOut) {
                  
                // if (response.status == '403') {

                //     ToastAndroid.show('Login Gagal', ToastAndroid.SHORT)
                //     that.setState({
                //     loading: false,
                //   });
                // }
                if (response.status == '200') {
                    
                    ToastAndroid.show('Suka berita ini', ToastAndroid.SHORT)
                    //AsyncStorage.setItem('@Username:key', username);
                    //that.saveKey(response.headers.map.authorization[0]);
                  //   that.setState({
                  //   loading: false,
                  // });

                }else{


                   ToastAndroid.show('No Like', ToastAndroid.SHORT)

                }


           }


     //    });
     // .then((result) => {
     //     clearTimeout(timeout);
     //        if(!didTimeOut) {
     //             that.setState({ data : result,
     //                      loading: false

     //                    });

     //            console.log('fetch good! ', result);
     //            resolve(result);
     //        }
     }).catch(function(err) {
            console.log('fetch failed! ', err);
            console.log('Koneksi Putus');
            // Rejection already happened with setTimeout
            if(didTimeOut) return;
            // Reject with error
            reject(err);
        });
    })
    .then(function() {
        // Request success and no timeout
        console.log('good promise, no timeout! ');
    })
    .catch(function(err) {
        // Error: response error, request timeout or runtime error
        console.log('promise error! ', err);
    });


}

//////

   getDetail(value) {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });
      

      //console.log(this.state.myToken)
      
      var ID = this.props.newsId

      var url =  DevApi + 'news/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': value 
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            body: result.body,
            publisher: result.publisher,
            created_at: result.created_at,
            loading: false
         });

          console.log(result)

        })
        .catch((error) => { console.error(error); });
  }


////////Modal

state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });


 renderBody(){

   if (this.state.loading) {
      return (<View style={{flex:1,justifyContent: 'center'}}>

                <ActivityIndicator size="large" color="#404f3d" style={{marginTop:20}}/>
             </View>)
    }else{

     
     return(

<HTML style={{color:'#2d2a26', fontSize: 15, lineHeight:30, fontFamily: 'Roboto-Light'}} html={this.state.body} imagesMaxWidth={Dimensions.get('window').width} />
         


     )


    }


 }


   
  render() {

  const todayDate = moment(this.state.created_at).format('LL');
    
    return (
      
     
      <View style={CommonStyles.mainPage}>

       
        
      
       <View style={[{flex:1}, styles.elementsContainer]}>

          <View style={{flex: 1}}>
          <ImageBackground
          source={{uri: this.props.newsImg}}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: 200}}
        />
          <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true}
          rightButtons={
            [
              {
                key: 1,
                buttonIcon: require('../../img/fkppi/ic_like_2.png'),
                buttonAction: this.getLike.bind(this),
                buttonWidth: 20,
                buttonHeight: 20,
              },
              
            ]
          } 
          
        />
         
            <View style={{margin: 15}}>
               
            <Text style={{color:'#959595', fontSize: 12, lineHeight:13, fontFamily: 'Roboto-Light',}}>{this.state.publisher}, {todayDate} </Text>
            <Text style={{fontFamily: 'Roboto-Bold', color:'#ffbf20', fontSize:16, lineHeight:24, marginTop:5, marginBottom:5}}>{this.props.title} </Text>
         
            
            </View>
            
          <View style={{flex: 1, marginLeft: 15, marginRight:15, marginBottom:10, marginTop:0}} >

             
             <ScrollView style={CommonStyles.noTabScroll}>
             
              {this.renderBody()}
         
            </ScrollView>



          </View>
          </View>


         
        </View>
       
            
       <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 6,
            padding: 0,
            height: 300,
            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
        <View style={{height:50, backgroundColor:'#404f3d', borderTopLeftRadius:6, borderTopRightRadius:6, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color:'#fff', fontSize:18}}>Laporkan Berita :</Text>
        </View>

        <View style={{marginLeft:50, marginRight:50}}> 
         <Text style={{textAlign: 'center',marginTop:15, fontSize: 15, color:'#2d2a26'}}>
         Alasannya ?
        </Text>
        </View>
        
          <View style={{margin: 20,borderColor:'#dce1e4',
            borderWidth: 0.5,
            borderStyle: 'solid',
            borderRadius: 0,
            backgroundColor:'#fbfbfb',}}>
           
           <TextInput
                      placeholder='your e-mail address'
                      multiline={true}
                      style={CommonStyles.textInputy}
                      underlineColorAndroid='transparent'
                      placeholder="Keterangan"
                      placeholderTextColor="grey"
                      numberOfLines={10}
                      multiline={true}
                 
                    />

          </View>
        <View style={{flexDirection:'row', justifyContent:'center', marginTop:20}}> 
          
          <View style={[CommonStyles.buttonFieldYellow2Full, {marginTop: spaceHeight * 0.0, marginRight:20}]}>
        
        
                 <TouchableWithoutFeedback onPress={this.onLogoutPress}>
                  <View>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#fff', textAlign:'center' , fontSize:15}]}>
                      Kirim
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>


            <View style={[CommonStyles.buttonFieldYellow2Half, {marginTop: spaceHeight * 0.0}]}>
        
        
                 <TouchableWithoutFeedback onPress={this.closeModal}>
                  <View>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'#ffbf20', textAlign:'center' , fontSize:15}]}>
                      Batal
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

        </View>
       
        </Modal>
        
      </View>
      

     
    
    )
  }

//
  _handleClickDaftar() {
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }

  _handleClickNotificationButton() {
    this.props.navigator.push({
      screen: "Healer.NotificationScreen",
      animated: true,
    });
  }

  
  _handleClickHeartButton() {
    // TODO: Click heart button
    this.props.navigator.push({
      screen: "Fkppi.DaftarScreen",
      animated: true,
    });
  }
}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
  

    elementsContainer: {
    flex: 1,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0
  },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
