import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  Platform,
   StatusBar,
   Alert,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  AsyncStorage,
  ToastAndroid,
  CheckBox,
  DatePickerAndroid,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DevApi, MainApi} from './fkppi/Api';
import {toJS } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import Loader from './fkppi/Loader';


import { Container, Header, Button, Body, Content, Card, CardItem, Form, TabHeading, Item, Picker, Label, Input, Textarea, Tab, Tabs, ScrollableTab, FormGroup  } from 'native-base';

import CommonStyles, {
  deviceHeight,
  deviceWidth,
  NAV_HEIGHT,
  TAB_HEIGHT,
} from '../styles/CommonStyles';


import NoNavigationBar from '../elements/NoNavigationBar';
import { noNavTabbarNavigation } from '../styles/navigatorStyle';
import Modal from "react-native-simple-modal";
import { appSingleNavigation } from '../styles/navigatorStyle';
import SignatureCapture from 'react-native-signature-capture';



@inject('Dropdown', 'Provinsi', 'Rayon')
@observer
export default class KonfirmasiMemberScreen extends Component {
  
  static navigatorStyle = appSingleNavigation;

  constructor(props) {
    super(props);
    this.state = {
      datax: [],
      myToken: '',
      loading: false,
      terms:false,
       value: null,
        data: null,

    }
   
  }
 /////////


 state = { open: false };

  modalDidOpen = () => console.log("Modal did open.");
 

  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  
   modalClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  openModal = () => this.setState({ open: true });


  closeModal = () => this.setState({ open: false });


 async getConfirm() {
    this.setState({
      loading: true
    });

   
    //console.log('coords', coords)

 this.getKey();

    // setTimeout(() => {
    //      this.getKey();
    //   this.setState({
    //     loading: false,
    //   });
    // }, 2500);
  }

async getMain() {
    this.setState({
      loading: true
    });

    setTimeout(() => {
     this._goToMainScreen();
     //AsyncStorage.setItem('@Token:key', 'contohloginkali');
      this.setState({
        loading: false,
      });
    }, 2000);
  }




  componentDidMount() {
       StatusBar.setHidden(true);
       var that = this;
       //that.getKey();
        that.forceUpdate();

    }


_onSaveEvent(result) {
        // //result.encoded - for the base64 encoded png
        // //result.pathName - for the file path name

        // const base64 = `data:image/png;base64,${result.encoded}`;
        // this.setState({data: base64});

        // // setTimeout(() => {
        // //   this.setState({ttd: result.pathName});
        // // }, 2000)

        const base64String = result.pathName;
        //this.setState({data: base64String});

        console.log(result);
    }


// async getKey() {
//     try {
//       //const Data = await AsyncStorage.getItem('@FormData:Key');
//       const value = await AsyncStorage.getItem('@Token:Key');
//       this.setState({myToken: value});
//       //this.getDetail(value ); 
//       this.onSubmit(value)   
//       console.log(value)
//     } catch (error) {
//       console.log("Error retrieving data" + error);
//     }
//   }

async getKey() {
    try {
      const value = await AsyncStorage.getItem('@Token:key');
      this.setState({myToken: value});
      //this.getDetail(value );  
       this.onSubmit(value)   
      console.log(value)
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 

  
  onSubmit(value){
  const FormData = require('form-data');
   // const fetch = require('node-fetch');

   //  var data = new FormData();

   //  data.append('title1', 'Dr');
   //  data.append('firstname', 'Bruce');
   //  data.append('lastname', 'Banner');
  

   //   console.log(data)
   //   var url =  DevApi + 'member/register';
     // fetch(url, { 
     //     method: 'POST', 
     //    headers: {
     //            'Accept': 'application/json',
     //             'Content-Type': 'multipart/form-data',
     //            'Authorization': value 
     //          },
     //     body: data, 
     //   })
     //  .then(function(res) {
     //      return res.json();
     //  }).then(function(json) {
     //      console.log(json);
     //  });

    //  let xhr = new XMLHttpRequest();
    // xhr.open('POST', url);
    // let formdata = new FormData();
    // // image from CameraRoll.getPhotos(
    // formdata.append('title1', 'Dr');
    // formdata.append('firstname', 'Bruce');
    // formdata.append('lastname', 'Banner');
    // xhr.send(formdata);
    // var url =  DevApi + 'member/register';

  
    var url =  DevApi + 'member/register';
    var _this = this;
    var formData = new FormData();

    formData.append('title1', this.props.title1);
    formData.append('title2', this.props.title2);
    formData.append('firstname', this.props.namaDepan);
    formData.append('lastname', this.props.namaBelakang);
    formData.append('email', null);
    formData.append('alias', this.props.alias);
    formData.append('nik', this.props.nik);
    formData.append('birthplace', this.props.birthplace);
    formData.append('birthday', this.props.birthday);
    formData.append('sex', this.props.sex);
    formData.append('religions_id', this.props.religions_id);
    formData.append('degree', this.props.degree);
    formData.append('occupations', this.props.occupations);
    formData.append('maritalstatus', this.props.maritalstatus);
    formData.append('spousename', this.props.spousename);
    formData.append('mothername', this.props.mothername);
    formData.append('h_province_id', this.props.h_province_id);
    formData.append('h_cities_id', this.props.h_cities_id);
    formData.append('h_districts_id', this.props.h_districts_id);
    formData.append('h_villages_id', this.props.h_villages_id);
    formData.append('h_address_1', this.props.h_address_1);
    formData.append('h_address_2', this.props.h_address_2);
    formData.append('h_rtrw', this.props.h_rtrw);
    formData.append('h_postcodes_id', this.props.h_postcodes_id);
    formData.append('h_phone', this.props.h_phone);
    formData.append('mobile', this.props.mobile);
    formData.append('o_province_id', this.props.o_province_id);
    formData.append('o_cities_id', this.props.o_cities_id);
    formData.append('o_districts_id', this.props.o_districts_id);
    formData.append('o_villages_id', this.props.o_villages_id);
    formData.append('o_address_1', this.props.o_address_1);
    formData.append('o_address_2', this.props.o_address_2);
    formData.append('o_rtrw', this.props.o_rtrw);
    formData.append('o_postcodes_id', this.props.o_postcodes_id);
    formData.append('o_phone', this.props.o_phone);
    formData.append('p_nrp', this.props.p_nrp);
    formData.append('p_name', this.props.p_name);
    formData.append('p_military_status_id', this.props.p_military_status_id);
    formData.append('p_military_uo_id', this.props.p_military_uo_id);
    formData.append('p_military_ranks', this.props.p_military_ranks);
    formData.append('p_military_position', this.props.p_military_position);
    formData.append('p_childrens', this.props.p_childrens);
    formData.append('p_religions_id', this.props.p_religions_id);
    formData.append('p_kinship_id', this.props.p_kinship_id);
    formData.append('p_info', this.props.p_info);
    formData.append('g_members_memberstatus_id', '1');
    formData.append('g_registered_at', this.props.g_registered_at);
    formData.append('g_dpd_id', this.props.g_dpd_id);
    formData.append('g_dpw_id', this.props.g_dpw_id);
    formData.append('g_dpra_id', this.props.g_dpra_id);
    formData.append('g_jabatan_id', this.props.g_jabatan_id);
    formData.append('g_jabatan_start', '10/31/2018');
    formData.append('g_jabatan_end', '10/31/2018');
    formData.append('d_kta_front', this.props.ktaFront);
    formData.append('d_kta_back', this.props.ktaBack);
    formData.append('d_foto', this.props.foto);
    formData.append('d_ktp', this.props.ktp);
    formData.append('d_skep_pensiun', this.props.skep_pensiun);
    formData.append('d_skep_pensiun_back', this.props.skep_pensiun_back);
    formData.append('d_kta_tnipolri', this.props.kta_tnipolri);
    formData.append('d_kta_tnipolri_back', this.props.kta_tnipolri_back);
    formData.append('d_dok_tambahan', this.props.dok_tambahan);
    formData.append('d_aktalahir', this.props.aktalahir);
    formData.append('d_kk', this.props.kk);

    console.log(formData)
  

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.responseType = 'json';
    console.log('OPENED', xhr);
   
    //xhr.timeout = 2000; // time in milliseconds

    xhr.onprogress = function () {
        console.log('LOADING', xhr);
         // _this.openModal();
         //  _this.setState({
         //    loading: false,
         //  });
  
        //Alert.alert("Sedang Mengajukan");
        //ToastAndroid.show('Sedang Mengfajukan', ToastAndroid.SHORT);
    };


    xhr.onload = function(e) {
      if (this.status == 200) {

        setTimeout(() => {
          _this.openModal();
          _this.setState({
            loading: false,
          });
        }, 1500);
  

        /////////////
        //Alert.alert("Berhasil Terdaftar");
        //console.log('response', this.response.printedcard); // JSON response  
      }
    };
    
    xhr.setRequestHeader('Authorization', value);
    xhr.setRequestHeader('Content-Type', 'multipart/form-data');
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send(formData);


     //////


 
 }
  render() {
    
      const {data} = this.state;
    return (
      <ImageBackground
          source={require('../../img/fkppi/bg.png')}
          imageStyle={{resizeMode: 'cover'}}
          style={{width: '100%', height: '100%'}}
        >
       <Loader
          loading={this.state.loading} />
      <View style={CommonStyles.mainPage}>
        <NoNavigationBar
          navigator={this.props.navigator}
          isBack={true} 
          
        />
         <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>
         
         <View style={{flex:1, flexDirection:'row'}}>
         

            <View style={[{flex:1, marginTop: deviceHeight * 0.02}, styles.elementsContainer]}>

         <View style={{flex: 1, justifyContent: 'center' }}>
             <View style={styles.logoBox}>
               <Text style={[
              CommonStyles.mediumBold,
              CommonStyles.headerText,
              CommonStyles.whiteColor]}>Konfirmasi</Text>
             
            </View>
         </View>
        </View>
 

         </View>

         
        </View>
       
        <View style={{flex: 5, justifyContent: 'center', backgroundColor:'#000' }}>
          <Container>

           <Content style={{marginTop:10, marginLeft:10, marginRight:10, marginBottom:20}}>
               
               <Card style={{marginLeft:20,marginRight:20, marginTop:10}}>

               <View style={{justifyContent:'center', borderBottomWidth:1, borderBottomColor:'#e2e2e1', marginTop:10, marginLeft:20, marginRight:20}}>
                 
                 <Text style={{alignSelf:'center', marginBottom:10,  color:'#404f3d', fontFamily:'Roboto-Bold', fontSize:14}}>Pernyataan</Text>

               </View>
                    
                <CardItem bordered>
                  <Body>
                    <Text style={{textAlign:'center', fontSize:12}}>
                      Dengan ini mengajukan permohonan menjadi anggota FKPPI dan untuk itu saya bersedia mentaati Anggaran Dasar dan Anggaran Rumah Tangga FKPPI serta seluruh ketentuan yang berlaku dalam Organisasi FKPPI.

                    </Text>
                  </Body>
                </CardItem>


                  </Card>

                  

                 <Card style={{height:250,backgroundColor:'#ccc', marginLeft:20,marginRight:20, marginTop:20}}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
               <Text style={{color:'#333', fontFamily:'Roboto-light', fontSize:11, margin:10}}>Tanda Tangan Anggota</Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>
               

            </View>
           
                 </Card>
                  <View style={{ flex: 1, flexDirection: "row", justifyContent:'flex-end', marginLeft:10,marginRight:10 }}>
                    <TouchableHighlight 
                     underlayColor="#ccc"
                     style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Simpan TTD</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor="#ccc" style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Reset</Text>
                    </TouchableHighlight>
                 
                 
                </View>

                <View style={{marginLeft:20, marginRight:20, marginTop:10}}>
             
                        <View style={{ flexDirection: 'row' }}>
                      <CheckBox
                        value={this.state.checked}
                        onValueChange={() => this.setState({ checked: !this.state.checked })}
                      />
                      <Text style={{marginTop: 5, textAlign:'justify', marginRight:20, color:'#404f3d', fontFamily:'Roboto-light', fontSize:12}}> Saya telah membaca dan memahami AD/ART dan
                  Peraturan Organisasi secara seksama
                  </Text>
                    </View>
              
               {this.state.checked &&
                 <View >
        
        
                 <TouchableWithoutFeedback onPress={() => this.getConfirm()}>
                  <View style={[CommonStyles.buttonFieldKonfirmasi, styles.buttonBox, {marginBottom: spaceHeight * 0.3}]}>
                    <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.normalText,{color:'white', }]}>
                      Konfirmasi
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            }
                </View>
            
              
           
           </Content>
        
         </Container>

       </View>

      

        
      </View>
      <Modal

          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center" }}
          modalStyle={{
            borderRadius: 10,
            padding: 25,

            backgroundColor: "#fff",
            margin: deviceWidth * 0.1,

          }}
           containerStyle={{
              justifyContent: "center"
            }}
        

        >
           <View style={styles.titleBox}>
           <Image
                      source={require('../../img/fkppi/ic_success.png')}
                      style={{width: 45, height: 45, alignSelf: 'center', marginBottom:10}}
                    />
            <Text style={[
            CommonStyles.forgotText,
            CommonStyles.blackColor,
            CommonStyles.extraBold,{ textAlign: 'center'}]}>
           Sukses
          </Text>

          <Text style={{textAlign: 'center',marginTop:15, fontSize: 16, color:'#adadad'}}>
         Terima Kasih sudah mendaftar, data anda akan di verifikasi terlebihh dahulu dan akan menerima email lanjutan dari kami

          </Text>

         

          <View style={{height: 40, marginTop: 30,borderColor:'#ffbf20', backgroundColor: '#ffbf20', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
           
            <TouchableOpacity onPress={() => this.getMain()}>
              <Text style={[
                      CommonStyles.mediumBold,
                      CommonStyles.itemHeaderText,{color:'#fff', textAlign:  'center' }]}>OK</Text>
            </TouchableOpacity>

          </View>
           
          </View>
       
        
  
        </Modal>
      </ImageBackground>
    )
  }

//

saveSign() {
        this.refs["sign"].saveImage();
    }

    resetSign() {
        this.refs["sign"].resetImage();
    }

    _onDragEvent() {
         // This callback will be called when the user enters signature
        console.log("dragged");
    }

  _handleClick() {
    this.props.navigator.push({
      screen: "Fkppi.KonfirmasiMemberScreen",
      animated: true,
    });
  }

  _goToMainScreen() {
    this.props.navigator.resetTo({
      screen: "Fkppi.MainServiceScreen",
      animated: true,
      animationType: 'slide-up',
      portraitOnlyMode: true,
       appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    });
  }


}

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - NAV_HEIGHT - TAB_HEIGHT - ELEMENT_HEIGHT;

const styles = StyleSheet.create({
  titleBox: {
    marginTop: spaceHeight * 0.1 + NAV_HEIGHT, 
    paddingLeft: 27,
    paddingRight: 27,
  },
  bg:{
    backgroundColor:'#404f3d',
  },
  fullField: {
    flex: 3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft:15,
    marginRight: 15,
    marginTop: 15,

  },
   fullFields: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ccc',
     alignItems: 'flex-start', 
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
    marginLeft: 10,
    alignItems: 'center'
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
      marginRight: 8,
  },
   elementsContainer: {
      flex: 1,
    
    },
    logoBox:{
      alignSelf:'center',
      alignItems: 'center', 
      flexDirection: 'column',

    },

    /////////

    gridView: {
    paddingTop: 5,
    flex: 1,
    margin:10,

  },
  itemContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 50,
    backgroundColor: '#000000'
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  buttonBox: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },

  signature: {
        flex: 1,
        borderColor: '#000033',
        borderWidth: 1,
        height:200,
        backgroundColor:'#e2e2e1'
    },
buttonStyle: {
        flex: 1, justifyContent: "center", alignItems: "center", height: 40,
        backgroundColor: "#eeeeee",
        margin: 10
    },
     titleBox: {
   justifyContent: 'flex-start'
  },
});
