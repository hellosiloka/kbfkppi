import {
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';

export const noNavTabbarNavigation = {
  orientation: 'portrait',
  portraitOnlyMode: true,
  navBarHidden: true,
  tabBarHidden: true,
  statusBarBlur: false,
  statusBarColor: 'rgba(162,153,117,1)',
}

export const appSingleNavigation = {
  navBarHidden: true,
  orientation: 'portrait',
  portraitOnlyMode: true,
  statusBarColor: '#857d60',
  statusBarTextColorSchemeSingleScreen: '#000',
}

export const singleScreenNavigation = {
  orientation: 'portrait',
  portraitOnlyMode: true,
  navBarHidden: true,
  tabBarHidden: true,
  statusBarColor: '#857d60',
  statusBarTextColorScheme: '#000',
}
