// @flow

import { observable, action, computed, } from 'mobx';
import axios from "axios";



import {DevApi, MainApi} from '../screens/fkppi/Api';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.delete['Content-Type'] = 'application/json';


class Store {

   @observable provinsi;
   @observable kabupaten;
   @observable kecamatan;
   @observable kelurahan;
   @observable kodepos;

 //////////////////////////////////

     constructor() {
        this.provinsi = [];
        this.kota = [];
        this.kecamatan = [];
        this.kelurahan = [];
        this.kodepos = {};
      }

///////////////////////////////// Fetch

  async fetchProvinsi(token) {
    let { data } = await axios.get(
      `${DevApi}list/province`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setProvinsi(data) 
  }
///////////////////////////
 

 async fetchKota(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/regency/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKota(data) 
  }


  async fetchKecamatan(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/subdistrict/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKecamatan(data) 
  }


  async fetchKelurahan(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/village/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKelurahan(data) 
  }


  async fetchKodepos(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/postcode/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKodepos(data) 
  }
  
  

///////////////////////////////// Actions 

  @action setProvinsi(data) {

    this.provinsi = data;
   
   }

   @action setKota(data) {

    this.kota = data;
   
   }

   @action setKecamatan(data) {

    this.kecamatan = data;
   
   }

    @action setKelurahan(data) {

    this.kelurahan = data;
   
   }

    @action setKodepos(data) {

    this.kodepos = data;
   
   }

   

  
   @action clearItems() {

        this.provinsi = [];
        this.kota = [];
        this.kecamatan = [];
        this.kelurahan = [];
        this.kodepos = {};
    }

}

export default new Store();
