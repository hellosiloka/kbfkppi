// @flow

import { observable, action, computed, } from 'mobx';
import axios from "axios";

//import {DevApi, MainApi} from '../Screens/fkppi/Api';

class Store {


   @observable items;
   @observable items2;

 //////////////////////////////////

     constructor() {
        this.items = [];
        this.items2 = [];
      }

///////////////////////////////// Fetch

  async fetchData() {
    let { data } = await axios.get(
      `https://jsonplaceholder.typicode.com/posts?userId=1`
    );
    //console.log(data);
    this.setData(data) 
  }
  async fetchData2() {
    let { data } = await axios.get(
      `https://jsonplaceholder.typicode.com/posts?userId=4`
    );
    //console.log('Item2=' + data);
    this.setData2(data) 
  }

///////////////////////////////// Actions 

  @action setData(data) {

    this.items = data;
   
   }

   @action setData2(data) {

    this.items2 = data;
   
   }

  
   @action clearItems() {
      this.items = [];
      this.items2 = [];
    }

}

export default new Store();
