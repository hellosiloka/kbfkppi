// @flow

import { observable, action, computed, } from 'mobx';
import axios from "axios";



import {DevApi, MainApi} from '../screens/fkppi/Api';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.delete['Content-Type'] = 'application/json';


class Store {

   @observable mabes;
   @observable messages;
   @observable faqs;
   @observable news;
   @observable komunitas;
   @observable sliders;

  @observable isLoading = false;

 //////////////////////////////////

     constructor() {
        this.mabes = [];
        this.messages = [];
        this.faqs = [];
        this.news = [];
        this.komunitas = [];
        this.sliders = [];
      }

///////////////////////////////// Fetch

  async fetchMabes(token) {
    let { data } = await axios.get(
      `${DevApi}mabes/index`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKB(data) 
  }
///////////////////////////
 
 async fetchPesan(token) {
    let { data } = await axios.get(
      `${DevApi}message/inbox`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setPesan(data) 
  }

  //////////

  async fetchFaq(token) {
    this.isLoading = true;
    let { data } = await axios.get(
      `${DevApi}faq/index`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setFaq(data) 
  }

  //////////////////////////

  async fetchSlider(token) {
    this.isLoading = true;
    let { data } = await axios.get(
      `${DevApi}banner/index`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setSlider(data) 
  }
  

///////////////////////////////// Actions 

  @action setKB(data) {

    this.mabes = data;
   
   }

   @action setPesan(data) {

    this.messages = data;
   
   }


 @action setFaq(data) {

    this.faqs = data;
   
   }


   @action setSlider(data) {

    this.sliders = data;
   
   }

   

  
   @action clearItems() {

        this.mabes = [];
        this.messages = [];
        this.faqs = [];
        this.news = [];
        this.komunitas = [];
        this.sliders = [];

    }

}

export default new Store();
