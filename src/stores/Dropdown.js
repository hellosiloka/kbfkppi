// @flow

import { observable, action, computed, } from 'mobx';
import axios from "axios";



import {DevApi, MainApi} from '../screens/fkppi/Api';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.delete['Content-Type'] = 'application/json';


class Store {

   @observable title1;
   @observable title2;
   @observable religions;
   @observable gender;
   @observable degree;
   @observable occupations;
   @observable maritalstatus;
   @observable militarystatus;
   @observable kesatuan;
   @observable kinship;
   @observable membership;
   @observable jabatan;

 //////////////////////////////////

     constructor() {
        this.religions = [];
        this.title1 = [];
        this.title2 = [];
        this.gender = [];
        this.degree = [];
        this.occupations=[];
        this.maritalstatus=[];
        this.militarystatus=[];
        this.kesatuan=[];
        this.kinship=[];
        this.membership=[];
        this.jabatan=[];
      }

///////////////////////////////// Fetch

  async fetchReligion(token) {
    let { data } = await axios.get(
      `${DevApi}list/religion`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setReligi(data) 
  }
///////////////////////////

   async fetchTitle1(token) {
    let { data } = await axios.get(
      `${DevApi}list/title/first`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setTitle1(data) 
  }

  //////////////////////////

   async fetchTitle2(token) {
    let { data } = await axios.get(
      `${DevApi}list/title/last`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setTitle2(data) 
  }

  async fetchGender(token) {
    let { data } = await axios.get(
      `${DevApi}list/gender`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setGender(data) 
  }

  async fetchDegree(token) {
    let { data } = await axios.get(
      `${DevApi}list/degree`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setDegree(data) 
  }

  async fetchOccupations(token) {
    let { data } = await axios.get(
      `${DevApi}list/occupation`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setOccupations(data) 
  }

  async fetchMartial(token) {
    let { data } = await axios.get(
      `${DevApi}list/maritalstatus`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    console.log(data);
    this.setMartial(data) 
  }

  async fetchMilitary(token) {
    let { data } = await axios.get(
      `${DevApi}list/militarystatus`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setMilitary(data) 
  }

  async fetchKesatuan(token) {
    let { data } = await axios.get(
      `${DevApi}list/militaryuo`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    console.log(data);
    this.setKesatuan(data) 
  }

  async fetchKinship(token) {
    let { data } = await axios.get(
      `${DevApi}list/kinship`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setKinship(data) 
  }

  async fetchMembership(token) {
    let { data } = await axios.get(
      `${DevApi}list/membershipstatus`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setMembership(data) 
  }


  async fetchJabatan(token) {
    let { data } = await axios.get(
      `${DevApi}list/position`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setJabatan(data) 
  }


///////////////////////////////// Actions 

  @action setReligi(data) {

    this.religions = data;
   
   }

   @action setTitle1(data) {

    this.title1 = data;
   
   }


    @action setTitle2(data) {

      this.title2 = data;
     
     }

     @action setGender(data) {

      this.gender = data;
     
     }

    @action setDegree(data) {

      this.degree = data;
     
     }

    @action setOccupations(data) {

      this.occupations = data;
     
     }

     @action setMartial(data) {

      this.maritalstatus = data;
     
     }

     @action setMilitary(data) {

      this.militarystatus = data;
     
     }

     @action setKesatuan(data) {

      this.kesatuan = data;
     
     }


     @action setKinship(data) {

      this.kinship = data;
     
     }

     @action setMembership(data) {

      this.membership = data;
     
     }

     @action setJabatan(data) {

      this.jabatan = data;
     
     }

  
   @action clearItems() {

        this.religions = [];
        this.title1 = [];
        this.title2 = [];
        this.gender = [];
        this.degree = [];
        this.occupations=[];
        this.maritalstatus=[];
        this.militarystatus=[];
        this.kesatuan=[];
        this.kinship=[];
        this.membership=[];
        this.jabatan=[];

    }

}

export default new Store();
