// @flow

import { create } from 'mobx-persist';
import { AsyncStorage } from 'react-native';

import App     from './App';
import Counter from './Counter';
import Religion from './Religion';
import Dropdown from './Dropdown';
import Provinsi from './Provinsi';
import Rayon from './Rayon';
import Main from './Main';


const hydrate = create({ storage: AsyncStorage });

const stores = {
  App,
  Counter,
  Religion,
  Dropdown,
  Provinsi,
  Rayon,
  Main
}

// you can hydrate stores here with mobx-persist
//hydrate('Account', stores.Account);

export default {
  ...stores
};
