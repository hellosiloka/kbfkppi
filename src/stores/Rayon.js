// @flow

import { observable, action, computed, } from 'mobx';
import axios from "axios";



import {DevApi, MainApi} from '../screens/fkppi/Api';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.delete['Content-Type'] = 'application/json';


class Store {

   @observable pd;
   @observable pc;
   @observable rayon;

 //////////////////////////////////

     constructor() {
        this.pd = [];
        this.pc = [];
        this.rayon = [];
      }

///////////////////////////////// Fetch

  async fetchPd(token) {
    let { data } = await axios.get(
      `${DevApi}list/pd`,
      {headers: {
        "Authorization" : token
      }
    }
    );
   console.log(data);
    this.setPd(data) 
  }
///////////////////////////
 

 async fetchPc(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/pc/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setPc(data) 
  }


  async fetchRayon(token, id) {
    let { data } = await axios.get(
      `${DevApi}list/rayon/${id}`,
      {headers: {
        "Authorization" : token
      }
    }
    );
    //console.log(data);
    this.setRayon(data) 
  }



///////////////////////////////// Actions 

  @action setPd(data) {

    this.pd = data;
   
   }

   @action setPc(data) {

    this.pc = data;
   
   }

   @action setRayon(data) {

    this.rayon = data;
   
   }

   

  
   @action clearItems() {

        this.pd = [];
        this.pc = [];
        this.rayon = [];
    }

}

export default new Store();
